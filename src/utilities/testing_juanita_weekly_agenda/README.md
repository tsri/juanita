# Mailgun test script

With index.js you can send test mails over mailgun. It's especially meant to test your templates on Mailgun.

# setup

`npm run install`

# send test mail

- add your `api key` from Mailgun and other variables in `index.js` file.
- run`node index.js` in your cli
