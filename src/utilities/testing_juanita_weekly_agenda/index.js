// INSERT YOUR API KEY HERE
const apiKey = ''

const mailgun = require('mailgun-js')
const DOMAIN = 'mg.tsrimails.ch'
const mg = mailgun({
  apiKey,
  domain: DOMAIN,
  host: 'api.eu.mailgun.net'
})
const variables = {
  events: [
    {
      category: 'Diverses',
      start_date: '8.3.2023',
      pretty_time: '08.03.2023 17:00',
      organizer: 'Tsüri',
      title: 'ZWZ: Flinta*-Hang-Out',
      image:
        'https://tsrich.s3.eu-central-1.amazonaws.com/images/2023/03/Bildschirmfoto_2023-03-06_um_10.55.16.png',
      description:
        "Am Flinta*-Hang-Out in der Zentralwäscherei kannst du bei Prosecco oder Sirup dein Transpi für die Demo am Samstag oder für den 14.Juni basteln. Ausserdem gibt es die Möglichkeit, feministische Stickers zu designen oder eine coole, empowernde Playlist zusammenzustellen. Wenn dir aber nicht nach basteln oder diggen ist, kannst du dich auch einfach gemütlich dazugesellen und den Austausch mit gleichgesinnten Frauen, non-binären, trans und agender Personen geniessen. Let's go!",
      url: 'https://zentralwaescherei.space/event/508'
    },
    {
      category: 'Diverses',
      start_date: '8.3.2023',
      pretty_time: '08.03.2023 17:00',
      organizer: 'Tsüri',
      title: 'Präsentation: Unbekannte Pionierinnen sichtbar machen',
      image:
        'https://tsrich.s3.eu-central-1.amazonaws.com/images/2023/03/Bildschirmfoto_2023-03-06_um_10.58.12.jpg',
      description:
        'Das Landesmuseum zeigt zurzeit eine Ausstellung, die als Hommage an einige Frauen gilt, die sich während Jahrzehnten für eine Verbesserung im Leben der Schweizerinnen eingesetzt haben. Die Historikerin Franziskar Rogger stellt heute Mittwoch Hörstücke vor, die sie in den letzten 30 Jahren mit verschiedenen Frauen aufgenommen hat. Frauen, die gegen sexuelle Belästigung, Arbeitsverbot oder keinen Lohn kämpften.',
      url: 'https://zentralwaescherei.space/event/508'
    }
  ]
}

const data = {
  from: 'Mailgun Sandbox <postmaster@mg.tsrimails.ch>',
  to: 'seraina.manser@tsri.ch',
  subject: 'Hello',
  template: 'juanita_weekly_agenda',
  'h:X-Mailgun-Variables': JSON.stringify(variables)
}
mg.messages().send(data, function(error, body) {
  console.log(error)
  console.log(body)
})
