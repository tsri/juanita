const colors = require('tailwindcss/colors')

module.exports = {
  content: ['./src/pages/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}'],
  theme: {
    fontFamily: {
      sans:
        '"HK Grotesk",ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"'
    },
    screens: {
      mobile: {max: '639px'},
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px'
    },
    flex: {
      '1': '1 1 0%',
      auto: '1 1 auto',
      initial: '0 1 auto',
      inherit: 'inherit',
      none: 'none',
      main: '1 0 auto'
    },
    colors: {
      tsri: '#36ADDF',
      bartsurigo: '#ffc7da',
      gray: colors.gray,
      white: '#fff',
      error: '#DC2626',
      errorLight: '#FFBBA6',
      primary: {
        DEFAULT: '#3AA1F0',
        light: '#3AA1F0',
        dark: '#3AA1F0'
      },
      blue: {
        DEFAULT: '#0E9FED',
        light: '#C0E9FF'
      },
      yellow: {
        DEFAULT: '#F5FF64',
        light: '#FBFFC0'
      },
      black: {
        DEFAULT: '#000000',
        light: '#D6D5D5'
      },
      red: {
        DEFAULT: '#FFBBA6',
        light: '#FFE8E0'
      },
      violet: {
        DEFAULT: '#B973FF',
        light: '#E7D7FF'
      },
      green: {
        DEFAULT: '#6BE59E',
        light: '#D0F6E0'
      }
    },
    minHeight: {
      '0': '0',
      '20': '20vh',
      '1/4': '25%',
      '1/2': '50%',
      '3/4': '75%',
      full: '100%',
      screen: '100vh',
      t450: '450px',
      t485: '486px'
    }
  },
  /*variants: {
    extend: {
      opacity: ['disabled'],
      cursor: ['disabled']
    }
  },*/
  plugins: []
}
