const path = require('path')
const dotenv = require('dotenv')
const {withSentryConfig} = require('@sentry/nextjs')
if (dotenv) dotenv.config()

const {i18n} = require('./next-i18next.config')

// Array of domains to whitelist for images
let imageDomain = ['localhost']
if (process.env.IMAGE_DOMAIN_LIST) {
  imageDomain = process.env.IMAGE_DOMAIN_LIST.split(',')
}

const moduleExports = {
  compiler: {
    // ssr and displayName are configured by default
    styledComponents: true
  },
  output: 'standalone',
  experimental: {
    scrollRestoration: true,
    esmExternals: false
  },
  i18n,
  images: {
    domains: imageDomain
  },
  trailingSlash: false,
  webpackDevMiddleware: config => {
    config.watchOptions = {
      poll: 1000,
      aggregateTimeout: 300
    }

    return config
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')]
  },
  async redirects() {
    return [
      {
        source: '/zh',
        destination: '/',
        permanent: true
      },
      {
        source: '/account',
        destination: '/account/profile',
        permanent: false
      }
    ]
  }
}

if (process.env.NODE_ENV === 'production') {
  moduleExports.sentry = {
    disableServerWebpackPlugin: true,
    disableClientWebpackPlugin: true
  }
}

const SentryWebpackPluginOptions = {
  // Additional config options for the Sentry Webpack plugin. Keep in mind that
  // the following options are set automatically, and overriding them is not
  // recommended:
  //   release, url, org, project, authToken, configFile, stripPrefix,
  //   urlPrefix, include, ignore

  silent: true // Suppresses all logs
  // For all available options, see:
  // https://github.com/getsentry/sentry-webpack-plugin#options.
}

// Make sure adding Sentry options is the last code to run before exporting, to
// ensure that your source maps include changes from all other Webpack plugins
if (process.env.NODE_ENV === 'production') {
  module.exports = withSentryConfig(moduleExports, SentryWebpackPluginOptions)
} else {
  module.exports = moduleExports
}
