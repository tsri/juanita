import cookie from 'cookie'
import {ReactNode, useEffect, useState} from 'react'
import {PaymentPeriodicity} from './@types/codegen/api'
import {toast} from 'react-toastify'
import * as Sentry from '@sentry/nextjs'

export function parseCookies(req) {
  return cookie.parse(req ? req.headers.cookie || '' : '')
}

export const TSRI_PAGE_TITLE = 'Tsüri.ch #MirSindTsüri'

// Hook
export function useScript(src: string, scriptAttributes: Record<string, any>) {
  // Keep track of script status ("idle", "loading", "ready", "error")
  const [status, setStatus] = useState(src ? 'loading' : 'idle')

  useEffect(
    () => {
      // Allow falsy src value if waiting on other data needed for
      // constructing the script URL passed to this hook.
      if (!src) {
        setStatus('idle')
        return
      }

      // Fetch existing script element by src
      // It may have been added by another intance of this hook
      let script = document.querySelector(`script[src="${src}"]`)

      if (!script) {
        // Create script
        script = document.createElement('script')
        script['src'] = src
        script['async'] = true
        script.setAttribute('data-status', 'loading')

        for (const [key, value] of Object.entries(scriptAttributes)) {
          script.setAttribute(key, value)
        }
        // Add script to document body
        document.body.appendChild(script)

        // Store status in attribute on script
        // This can be read by other instances of this hook
        const setAttributeFromEvent = event => {
          script.setAttribute('data-status', event.type === 'load' ? 'ready' : 'error')
        }

        script.addEventListener('load', setAttributeFromEvent)
        script.addEventListener('error', setAttributeFromEvent)
      } else {
        // Grab existing script status from attribute and set to state.
        setStatus(script.getAttribute('data-status'))
      }

      // Script event handler to update status in state
      // Note: Even if the script already exists we still need to add
      // event handlers to update the state for *this* hook instance.
      const setStateFromEvent = event => {
        setStatus(event.type === 'load' ? 'ready' : 'error')
      }

      // Add event listeners
      script.addEventListener('load', setStateFromEvent)
      script.addEventListener('error', setStateFromEvent)

      // Remove event listeners on cleanup
      return () => {
        if (script) {
          script.removeEventListener('load', setStateFromEvent)
          script.removeEventListener('error', setStateFromEvent)
          script.remove()
        }
      }
    },
    [src] // Only re-run effect if script src changes
  )

  return status
}

export function getMonthsinNumberFromPaymentPeriodicity(
  paymentPeriodicity: PaymentPeriodicity
): number {
  switch (paymentPeriodicity) {
    case PaymentPeriodicity.Monthly:
      return 1
    case PaymentPeriodicity.Quarterly:
      return 3
    case PaymentPeriodicity.Biannual:
      return 6
    case PaymentPeriodicity.Yearly:
      return 12
  }
}

export function sortCreatedAtDateByNewestFirst(a, b) {
  const aCreatedAtDate = new Date(a.createdAt)
  const bCreatedAtDate = new Date(b.createdAt)

  if (aCreatedAtDate.getTime() > bCreatedAtDate.getTime()) return -1
  if (aCreatedAtDate.getTime() < bCreatedAtDate.getTime()) return 1
  return 0
}

export function handleError({
  errorText,
  errorDisplay = errorText,
  autoClose
}: {
  errorText: string
  errorDisplay?: string | ReactNode
  autoClose?: false | number
}) {
  console.log(errorText)
  toast(errorDisplay, {type: 'error', autoClose: autoClose})
  Sentry.captureMessage(errorText)
}

export enum PaymentSlugs {
  STRIPE_PAYMENT = 'stripe',
  PAYREXX_PAYMENT = 'payrexx'
}
export const TSRI_SELLING_MEMBER_PLAN_TAG = 'selling'
export const validateEmailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
