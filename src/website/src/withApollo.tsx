import {ApolloClient, NormalizedCacheObject, InMemoryCache, createHttpLink} from '@apollo/client'
import {useMemo} from 'react'
import {onError} from '@apollo/client/link/error'
import * as Sentry from '@sentry/nextjs'
import {setContext} from '@apollo/client/link/context'
let apolloClient

export const getApolloClient = (ctx?: any, initialState?: NormalizedCacheObject) => {
  const uri =
    (typeof window === 'undefined' ? process.env.API_URL_SSR : process.env.NEXT_PUBLIC_API_URL) ||
    process.env.NEXT_PUBLIC_API_URL
  const httpLink = createHttpLink({
    uri,
    credentials: 'include'
  })
  const authLink = setContext((_, {headers}) => {
    // get the authentication token from local storage if it exists
    let token
    if (typeof window !== 'undefined') {
      token = localStorage.getItem('token')
    }
    // return the headers to the context so httpLink can read them
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : ''
      }
    }
  })
  const errorLink = onError(({graphQLErrors, networkError, operation, forward}) => {
    if (graphQLErrors) {
      graphQLErrors.forEach(graphQLError => {
        Sentry.captureException(graphQLError)
      })
      if (networkError) {
        Sentry.captureException(networkError)
      }
    }
    forward(operation)
  })

  return new ApolloClient({
    link: authLink.concat(errorLink).concat(httpLink),
    cache: new InMemoryCache().restore(initialState || {})
  })
}

export function initializeApollo(initialState = null) {
  const _apolloClient = apolloClient ?? getApolloClient()

  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // gets hydrated here
  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract()
    // Restore the cache using the data passed from getStaticProps/getServerSideProps
    // combined with the existing cached data
    _apolloClient.cache.restore({...existingCache, ...initialState})
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === 'undefined') return _apolloClient
  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient

  return _apolloClient
}
export function useApollo(initialState) {
  return useMemo(() => initializeApollo(initialState), [initialState])
}
