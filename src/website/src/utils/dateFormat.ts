import {format} from 'date-fns'
import {de} from 'date-fns/locale'

// const locales = {de}

// by providing a default string of 'PP' or any of its variants for `formatStr`
// it will format dates in whichever way is appropriate to the locale
export default function dateFormat(date: Date, formatStr = 'PP') {
  return format(date, formatStr, {
    locale: de //locales[window.__localeId__] // or global.__localeId__
  })
}
