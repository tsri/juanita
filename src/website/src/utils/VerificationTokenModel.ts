import {getModelForClass, prop} from '@typegoose/typegoose'

class VerificationToken {
  @prop()
  public identifier: string

  @prop()
  public expires: Date

  @prop()
  public token: string
}

const VerificationTokenModel = getModelForClass(VerificationToken)

export default VerificationTokenModel
