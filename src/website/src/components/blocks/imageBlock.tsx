import React from 'react'
import {useTranslation} from 'next-i18next'
import styled from 'styled-components'

export const ArticleImage = styled.img`
  max-height: calc(95vh - 108px);
  max-width: 100%;
  object-fit: cover;
`

export interface ImageBlockProps {
  readonly src: string
  readonly height: number
  readonly width: number
  readonly description?: string
  readonly caption?: string
  readonly author?: string
}

export function ImageBlock(props: ImageBlockProps) {
  const {t} = useTranslation('common')

  return (
    <div className="w-full mb-6">
      <ArticleImage src={props.src} alt={props.description} />
      {(props.author || props.caption) && (
        <p>
          {props.caption}{' '}
          {props.author ? <>{t('blocks.imageBlockCredits', {name: props.author})}</> : null}
        </p>
      )}
    </div>
  )
}
