import React, {useMemo} from 'react'
import Link from 'next/link'
import {HeaderType} from '../types'
import AuthorDetails from 'components/atoms/AuthorDetails'
import {TitleBlock} from './titleBlock'
import {formatRelative} from 'date-fns'
import {de} from 'date-fns/locale'
import dateFormat from '../../utils/dateFormat'
import {useTranslation} from 'next-i18next'
import {
  EmailIcon,
  EmailShareButton,
  FacebookIcon,
  FacebookShareButton,
  TwitterIcon,
  TwitterShareButton
} from 'react-share'
import {FullAuthorFragment} from '../../@types/codegen/graphql'
import {getIsSponsor, getNonAdAuthors} from '../ArticleComponent'

interface TsriTitleBlockDefaultProps {
  title: string
  lead?: string
  authors?: FullAuthorFragment[]
  publishedAt?: Date
  updatedAt?: Date
  showSocialMediaIcons?: boolean
  shareUrl: string
  isPeerArticle: boolean
  className?: string
  tags?: string[]
  isTicker: boolean
}

export function TsriTitleBlock({type, ...props}: TsriTitleBlockDefaultProps & {type: HeaderType}) {
  const {authors, publishedAt, updatedAt, title, lead, tags, shareUrl, isTicker} = props
  const {t} = useTranslation('common')
  const publishedDisplayDate = dateFormat(new Date(publishedAt), 'PPPp')

  const showUpdatedAt = publishedAt?.getTime() != updatedAt?.getTime()
  const updatedDisplayDate = formatRelative(new Date(updatedAt), new Date(), {
    locale: de,
    weekStartsOn: 1
  })

  const isPromo = useMemo(() => {
    return getIsSponsor(authors)
  }, [authors])

  const nonAdAuthors = useMemo(() => {
    return getNonAdAuthors(authors)
  }, [authors])

  return (
    <div className="grid grid-cols-12 grid-flow-row sm:mb-20">
      <div className="order-2	lg:order-1 col-span-12 lg:col-span-4 mt-8 lg:my-0">
        <div className={`bg-gray-400 mb-4 h-0.5 lg:w-10 ${isPromo ? 'mt-0' : 'mt-4'}`} />
        {nonAdAuthors.map((author, index) => (
          <div className="my-4" key={index}>
            <AuthorDetails author={author} />
            <div className="bg-gray-400	my-4 h-0.5 lg:w-10" />
          </div>
        ))}
        <p>{publishedDisplayDate}</p>
        <p>
          {showUpdatedAt && updatedAt && (
            <p>{t('blocks.titleBlockUpdatedAt', {date: updatedDisplayDate})}</p>
          )}
        </p>
        {isTicker && (
          <>
            <br />
            <em>{t('blocks.tickerInfo')}</em>
          </>
        )}
        {!!tags?.length && (
          <>
            <div className="bg-gray-400	my-4 h-0.5 lg:w-10" />
            <div className="leading-5 my-6">
              {tags?.map((tag, index) => (
                <Link
                  key={index}
                  href={`/zh/rubrik/${tag}`}
                  className="cursor-pointer inline-block rounded-full border-solid border-black border p-1 mx-0.5  my-1">
                  {`#${tag}`}
                </Link>
              ))}
            </div>
          </>
        )}
        {isPromo && (
          <>
            <div className="bg-gray-400	my-4 h-0.5 lg:w-10" />
            <div className="leading-5 my-6 italic">{t('blocks.promoArtikel')}</div>
          </>
        )}
      </div>
      <div className="order-1	lg:order-2 col-span-12 lg:col-span-8">
        <TitleBlock
          type={HeaderType.Default}
          shareUrl={'articleShareUrl' || ''}
          title={title}
          lead={lead}
          publishedAt={publishedAt}
          updatedAt={updatedAt}
          isPeerArticle={false}
          tags={tags}
          // isArticle={isArticle}
        />
        <div className="flex mt-10 w-full lg:w-1/2">
          <FacebookShareButton className="flex-1" url={shareUrl}>
            <FacebookIcon
              className="m-auto"
              size={64}
              round
              iconFillColor="black"
              bgStyle={{fill: 'white', stroke: 'black'}}
            />
          </FacebookShareButton>
          <TwitterShareButton className="flex-1" url={shareUrl}>
            <TwitterIcon
              className="m-auto"
              size={64}
              round
              iconFillColor="black"
              bgStyle={{fill: 'white', stroke: 'black'}}
            />
          </TwitterShareButton>
          <EmailShareButton className="flex-1" url={shareUrl}>
            <EmailIcon
              className="m-auto"
              size={64}
              round
              iconFillColor="black"
              bgStyle={{fill: 'white', stroke: 'black'}}
            />
          </EmailShareButton>
        </div>
      </div>
    </div>
  )
}

export default TsriTitleBlock
