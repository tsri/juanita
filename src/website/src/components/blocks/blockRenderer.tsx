import {Block, BlockType} from '../types'
import React, {ReactNode} from 'react'
import {EmbedBlock} from './embedBlock'
import {GalleryBlock} from './galleryBlock'
import {ImageBlock} from './imageBlock'
import {QuoteBlock} from './quoteBlock'
import {HTMLBlock} from './htmlBlock'
import {RichTextBlock} from './richTextBlock'
import {ListicalBLock} from './listicalBlock'
import {TitleBlock} from './titleBlock'
import {TitleImageBlock} from './titleBlockImage'
import CategoryGrid from './grid/categoryGrid'
import GridFlexBlockView from './grid/gridFlexBlockView'
import GridBlock from './grid/gridBlock'
import LinkPageBreakBlock from './linkPageBreakBlock'

export interface BlockRendererProps {
  blocks: Block[]
  publishedAt: Date
  updatedAt: Date
  children?(blockElement: JSX.Element | null): ReactNode
  articleShareUrl?: string
  isArticle?: boolean
  isPeerArticle?: boolean
  tags?: string[]
}

export function BlockRenderer({blocks, children, ...props}: BlockRendererProps) {
  return (
    <>
      {blocks.map(block =>
        block ? (
          <React.Fragment key={block.key}>
            {children ? children(renderBlock(block, props)) : renderBlock(block, props)}
          </React.Fragment>
        ) : null
      )}
    </>
  )
}

export interface RenderBlockOptions {
  publishedAt: Date
  updatedAt: Date
  articleShareUrl?: string
  isArticle?: boolean
  isPeerArticle?: boolean
  tags?: string[]
}

export function renderBlock(block: Block | null, opts: RenderBlockOptions) {
  const {publishedAt, updatedAt, articleShareUrl, isArticle, isPeerArticle = false, tags} = opts

  if (!block) return null

  switch (block.type) {
    case BlockType.RichText:
      return <RichTextBlock value={block.value} />

    case BlockType.Gallery:
      return <GalleryBlock media={block.value.media} />

    case BlockType.Embed:
      return <EmbedBlock data={block.value} />

    case BlockType.Grid:
      // special ui case Tsüri: create grid with articles autofill from certain category.
      const teasers = block?.value?.teasers
      if (
        block.value.numColumns === 1 &&
        teasers[0].preTitle &&
        teasers[0].preTitle.startsWith('category:')
      ) {
        return <CategoryGrid teaser={teasers[0]} tag={teasers[0].preTitle.split('category:')[1]} />
      }
      return <GridBlock teasers={teasers} />

    case BlockType.GridFlex:
      return <GridFlexBlockView flexTeasers={block.value.flexTeasers} />

    case BlockType.Quote:
      return <QuoteBlock author={block.value.author} text={block.value.text} />

    case BlockType.HTML:
      return <HTMLBlock html={block.value.html} />

    case BlockType.Image:
      return (
        <ImageBlock
          src={block.value.extension === '.gif' ? block.value.url : block.value.largeURL}
          width={1280}
          height={700}
          description={block.value.description}
          caption={block.value.caption}
          author={block.value.author}
        />
      )

    case BlockType.TitleImage:
      return <TitleImageBlock image={block.value} width={1280} height={680} />

    case BlockType.LinkPageBreak:
      return <LinkPageBreakBlock linkPageBreak={block.value} />

    case BlockType.Listicle:
      return <ListicalBLock listical={block.value} />

    case BlockType.Title:
      return (
        <TitleBlock
          shareUrl={articleShareUrl || ''}
          // preTitle={block.value.preTitle}
          type={block.value.type}
          title={block.value.title}
          lead={block.value.lead}
          publishedAt={publishedAt}
          updatedAt={updatedAt}
          showSocialMediaIcons={block.value.isHeader}
          isPeerArticle={isPeerArticle}
          tags={tags}
          //isArticle={isArticle}
        />
      )

    default:
      return null
  }
}
