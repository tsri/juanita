import React from 'react'

export interface HTMLBlockProps {
  readonly html: string
}

export function HTMLBlock({html}: HTMLBlockProps) {
  return (
    <div
      className="w-full lg:w-3/4"
      style={{maxWidth: '100% !important', overflowX: 'hidden'}}
      dangerouslySetInnerHTML={{__html: html}}></div>
  )
}
