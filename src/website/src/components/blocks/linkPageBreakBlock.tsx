import React, {useContext, useMemo} from 'react'
import {LinkPageBreakBlockDataFragment} from '../../@types/codegen/graphql'
import styled from 'styled-components'
import {RichText} from '../atoms/RichText'
import {ArticleImage} from './imageBlock'
import Link from 'next/link'
import {AuthContext} from '../auth/AuthContextProvider'

const PageBreakGridContainer = styled.div`
  width: 100%;
  background-color: ${props => props.theme.colors.yellow.DEFAULT};
  display: flex;
  flex-wrap: wrap;
  padding: 32px 32px 40px 22px;
  margin-bottom: 24px;
  align-items: center;

  @media (min-width: ${props => props.theme.screens.lg}) {
    padding: 77px 0 93px 75px;
  }
`
const PageBreakGridItem = styled.div`
  width: 100%;

  @media (min-width: ${props => props.theme.screens.lg}) {
    width: 50%;
  }
`

const PageBreakTitle = styled.div`
  width: 100%;
  font-style: italic;
  font-weight: 700;
  font-size: 40px;
  line-height: 48px;
  text-transform: uppercase;
  hyphens: auto;
  word-wrap: break-word;

  @media (min-width: ${props => props.theme.screens.lg}) {
    font-size: 55px;
    line-height: 70px;
    word-break: break-word;
  }
`
const PageBreakText = styled.div`
  width: 100%;
  padding-top: 10px;
  font-weight: 500;
  font-size: 16px;
  line-height: 135%;

  @media (min-width: ${props => props.theme.screens.lg}) {
    font-size: 22px;
    line-height: 130%;
    padding: 0 80px 0 80px;
  }
`
const PageBreakCTOContainer = styled.div`
  width: 100%;
  padding-top: 40px;

  @media (min-width: ${props => props.theme.screens.lg}) {
    padding: 40px 80px 0 80px;
  }
`
export const CTOButton = styled.button`
  width: 100%;
  height: 45px;
  background: black;
  color: white;
  padding: 12px;
  font-weight: 500;
  font-size: 16px;
  line-height: 19px;
  text-align: center;
  letter-spacing: 0.04em;

  @media (min-width: ${props => props.theme.screens.lg}) {
    width: unset;
    padding: 0 40px 0 40px;
  }
`

interface LinkPageBreakBlockProps {
  linkPageBreak: LinkPageBreakBlockDataFragment
}
export default function LinkPageBreakBlock({linkPageBreak}: LinkPageBreakBlockProps) {
  const authContext = useContext(AuthContext)
  const hideLinkPageBreakBlock = useMemo(() => {
    return (
      linkPageBreak.templateOption === 'membership' &&
      (authContext.hasActiveSubscription || authContext.loading)
    )
  }, [authContext])

  // if break block is of type membership and user is logged-in or is logging-in, do not show the page break block
  if (hideLinkPageBreakBlock) {
    return null
  }
  return (
    <>
      {linkPageBreak.image && (
        <ArticleImage src={linkPageBreak.image.largeURL} alt={linkPageBreak.image.description} />
      )}
      <PageBreakGridContainer>
        <PageBreakGridItem>
          {linkPageBreak.text && <PageBreakTitle>{linkPageBreak.text}</PageBreakTitle>}
        </PageBreakGridItem>
        <PageBreakGridItem>
          {linkPageBreak.richText && (
            <PageBreakText>
              <RichText value={linkPageBreak.richText} />
            </PageBreakText>
          )}
          {linkPageBreak.linkURL && (
            <PageBreakCTOContainer>
              <Link
                href={linkPageBreak.linkURL}
                passHref
                target={linkPageBreak.linkTarget}
                rel="noreferrer">
                <CTOButton>{linkPageBreak.linkText}</CTOButton>
              </Link>
            </PageBreakCTOContainer>
          )}
        </PageBreakGridItem>
      </PageBreakGridContainer>
    </>
  )
}
