import React, {useMemo} from 'react'
import styled, {css} from 'styled-components'
import {
  ArticleTeaser,
  ArticleTeaserFragment,
  TeaserFragment,
  TeaserStyle,
  useArticleListQuery
} from '../../../@types/codegen/graphql'
import TeaserView from '../../atoms/teaser/teaserView'
import {wordVariations} from '../../utility'
import {GRID_GAP} from '../../../../styles/helpers'
import Link from 'next/link'

const CategoryGridContainer = styled.div`
  margin-top: 80px;
  display: flex;
  flex-wrap: wrap;
  justify-content: end;

  ${props => css`
    @media (min-width: ${props.theme.screens.lg}) {
      justify-content: start;
      flex-wrap: nowrap;
    }
  `}
`

const ColorContainer = styled.div`
  height: 418px;
  width: calc(100% + 30px);
  margin: 0 -15px 0 -15px;

  ${props => css`
    @media (min-width: ${props.theme.screens.lg}) {
      height: 504px;
      width: 60%;
      margin: 0 0;
    }
  `}
`
const TeaserTitle = styled.div`
  font-weight: 500;
  font-size: 35px;
  line-height: 37px;
  letter-spacing: 0.01em;
  margin: 44px 40px 0 40px;
  width: 300px;
  cursor: pointer;

  ${props => css`
    @media (min-width: ${props.theme.screens.lg}) {
      margin: 100px 0 0 76px;
      font-size: 40px;
      line-height: 142.5%;
    }
  `}
`
const SingleArticleContainer = styled.div`
  width: 100%;
  margin: 0 26px;

  ${props => css`
    @media (min-width: ${props.theme.screens.sm}) {
      width: 75%;
    }
  `}

  ${props => css`
    @media (min-width: ${props.theme.screens.lg}) {
      width: 40%;
    }
  `}
`
const ArticleShift = styled.div`
  width: 100%;
  margin-top: -200px;

  ${props => css`
    @media (min-width: ${props.theme.screens.lg}) {
      margin-left: -50%;
      margin-top: 110px;
    }
  `}
`
const ArticleGrid = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -${GRID_GAP / 2}px;
`
const ArticleItem = styled.div`
  width: calc(100% - ${GRID_GAP}px);
  margin: ${GRID_GAP}px ${GRID_GAP / 2}px;

  ${props => css`
    @media (min-width: ${props.theme.screens.sm}) {
      width: calc(50% - ${GRID_GAP}px);
    }

    @media (min-width: ${props.theme.screens.lg}) {
      width: calc(25% - ${GRID_GAP}px);
    }
  `}
`

interface CategoryGridProps {
  teaser?: TeaserFragment
  tag: string
}
export default function CategoryGrid({teaser, tag}: CategoryGridProps) {
  const tagVariations = useMemo(() => {
    return wordVariations(tag)
  }, [tag])

  const {data} = useArticleListQuery({
    variables: {
      filter: {
        tags: tagVariations
      },
      take: 6
    }
  })

  const articles = useMemo(() => {
    return data?.articles?.nodes?.slice(1, data?.articles?.nodes?.length)
  }, [data])

  const firstArticle = useMemo(() => {
    if (!articles?.length) {
      return
    }
    return articles[0]
  }, [articles])

  const gridArticles = useMemo(() => {
    if (!articles?.length) {
      return
    }
    return articles.slice(1, articles.length)
  }, [articles])

  const teaserWithReplacedArticle = useMemo(() => {
    if (!firstArticle || !teaser) {
      return
    }
    if (teaser.__typename === 'ArticleTeaser') {
      const newTeaser = {...teaser}
      newTeaser.article = firstArticle
      return newTeaser
    }
    return undefined
  }, [firstArticle, teaser])

  return (
    <>
      <CategoryGridContainer>
        <ColorContainer className={'bg-blue-light'}>
          <Link href={`/zh/rubrik/${tag}`} legacyBehavior>
            <TeaserTitle>{teaser?.title}</TeaserTitle>
          </Link>
        </ColorContainer>
        <SingleArticleContainer>
          <ArticleShift>
            <TeaserView
              teaser={teaserWithReplacedArticle}
              overrideTitle={teaserWithReplacedArticle?.article?.title}
            />
          </ArticleShift>
        </SingleArticleContainer>
      </CategoryGridContainer>

      <ArticleGrid>
        {!!gridArticles?.length &&
          gridArticles.map((article, articleIndex) => {
            const articleTeaser: ArticleTeaserFragment = {style: TeaserStyle.Default}
            articleTeaser.__typename = 'ArticleTeaser'
            articleTeaser.article = article
            return (
              <ArticleItem key={articleIndex}>
                <TeaserView dynamicHeight teaser={articleTeaser} />
              </ArticleItem>
            )
          })}
      </ArticleGrid>
    </>
  )
}
