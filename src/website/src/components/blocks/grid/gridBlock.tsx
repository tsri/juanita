import React from 'react'
import {TeaserFragment} from '../../../@types/codegen/graphql'
import styled from 'styled-components'
import TeaserView from '../../atoms/teaser/teaserView'
import {BLOCK_GAP, GRID_GAP} from '../../../../styles/helpers'

const GridBlockContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -${GRID_GAP / 2}px ${BLOCK_GAP}px;
  @media (min-width: ${props => props.theme.screens.sm}) {
    margin: 0 0 ${BLOCK_GAP}px;
  }

  @media (min-width: ${props => props.theme.screens.lg}) {
    margin: 0 -${GRID_GAP / 2}px ${BLOCK_GAP}px;
  }
`
const GridBlockItem = styled.div`
  width: calc(100% - ${GRID_GAP}px);
  margin: 0 ${GRID_GAP / 2}px;

  @media (min-width: ${props => props.theme.screens.sm}) {
    width: calc(50% - ${GRID_GAP}px);
  }

  @media (min-width: ${props => props.theme.screens.lg}) {
    width: calc((100% / 3) - ${GRID_GAP}px);
  }
`

interface GridBlockProps {
  teasers: TeaserFragment[]
}
export default function GridBlock({teasers}: GridBlockProps) {
  return (
    <GridBlockContainer>
      {teasers.map((teaser, teaserIndex) => (
        <GridBlockItem key={`teaser-index-${teaserIndex}`}>
          <TeaserView teaser={teaser} dynamicHeight />
        </GridBlockItem>
      ))}
    </GridBlockContainer>
  )
}
