import React from 'react'
import styled from 'styled-components'
import {tabletMediaQuery} from '../../../styles/deprecated_helpers'
import {HeaderType} from '../types'
import {TitleBlockBreaking} from './titleBlockBreaking'
import {getHumanReadableTimePassed} from '../utility'
import {useTranslation} from 'next-i18next'
import {formatRelative} from 'date-fns'
import {de} from 'date-fns/locale'
import {pxToRem} from '../../../styles/helpers'

export const TitleBlock = styled(BaseTitleBlock)``

export const TitleInnerBlock = styled.div`
  max-width: ${pxToRem(1200)};
  margin: 0 auto;
  width: 100%;
`

export interface TitleBlockDefaultProps {
  title: string
  lead?: string
  publishedAt?: Date
  updatedAt?: Date
  showSocialMediaIcons?: boolean
  shareUrl: string
  isPeerArticle: boolean
  className?: string
  tags?: string[]
}

export function BaseTitleBlock({type, ...props}: TitleBlockDefaultProps & {type: HeaderType}) {
  switch (type) {
    default:
    case HeaderType.Default:
      return <TitleBlockDefault {...props} />

    case HeaderType.Breaking:
      return <TitleBlockBreaking {...props} />
  }
}

export function TitleBlockDefault({title, lead, className}: TitleBlockDefaultProps) {
  const ref = React.createRef<HTMLParagraphElement>()

  return (
    <div ref={ref} className={className}>
      <TitleInnerBlock>
        <h1 className="mobile:mb-6 mb-12 w-full md:text-5xl lg:text-6xl">{title}</h1>
        {lead && <p className="text-xl">{lead}</p>}
      </TitleInnerBlock>
    </div>
  )
}
