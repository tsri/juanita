import React, {useEffect} from 'react'

import {EmbedData, EmbedType} from '../types'

import {FacebookPostEmbed} from '../atoms/facebookEmbed'
import {InstagramPostEmbed} from '../atoms/instagramEmbed'
import {TwitterTweetEmbed} from '../atoms/twitterEmbed'
import {YouTubeVideoEmbed} from '../atoms/youTubeEmbed'
import {VimeoEmbed} from '../atoms/vimeoEmbed'
import {SoundCloudEmbed} from '../atoms/soundCloudEmbed'

// import {pxToRem, whenTablet, whenDesktop} from 'styles/helpers'
import {usePermanentVisibility} from '../utils/hooks'
import {transformCssStringToObject} from '../utility'
import {BildwurfAdEmbed} from '../atoms/bildwurfAdEmbed'
import {IframeEmbed} from '../atoms/iframeEmbed'
import {PayrexxEmbed} from '../atoms/payrexxEmbed'
import {WeMakeItEmbed} from '../atoms/weMakeItEmbed'
import {SpotifyEmbed} from '../atoms/spotifyEmbed'

export interface EmbedBlockProps {
  readonly data: EmbedData
}

// const EmbedBlockStyle = cssRule<{showBackground: boolean}>(({showBackground}) => ({
//   marginBottom: pxToRem(50),
//   padding: `0 ${pxToRem(25)}`,
//   opacity: showBackground ? 1 : 0,
//   transform: showBackground ? 'translate3d(0, 0, 0)' : 'translate3d(0, 100px, 0)',
//   transition: 'opacity 500ms ease, transform 700ms ease',

//   ...whenTablet({
//     width: '75%',
//     maxWidth: pxToRem(900),
//     margin: `0 auto ${pxToRem(70)}`,
//     display: 'flex',
//     justifyContent: 'center'
//   }),

//   ...whenDesktop({
//     width: '50%',
//     maxWidth: pxToRem(900),
//     margin: `0 auto ${pxToRem(70)}`,
//     display: 'flex',
//     justifyContent: 'center'
//   })
// }))

function embedForData(data: EmbedData) {
  switch (data.type) {
    /* case EmbedType.FacebookPost:
      return <FacebookPostEmbed {...data} /> */

    case EmbedType.InstagramPost:
      return <InstagramPostEmbed {...data} />

    case EmbedType.TwitterTweet:
      return <TwitterTweetEmbed {...data} />

    case EmbedType.YouTubeVideo:
      return <YouTubeVideoEmbed {...data} />

    case EmbedType.VimeoVideo:
      return <VimeoEmbed {...data} />

    case EmbedType.SoundCloudTrack:
      return <SoundCloudEmbed {...data} />

    case EmbedType.BildwurfAd:
      return <BildwurfAdEmbed {...data} />

    case EmbedType.IFrame:
      if (data.url?.includes('payrexx')) {
        return <PayrexxEmbed {...data} />
      } else if (data.url?.includes('wemakeit')) {
        return <WeMakeItEmbed {...data} />
      } else if (data.url?.includes('open.spotify.com')) {
        return <SpotifyEmbed {...data} />
      }
      return <IframeEmbed {...data} />
  }
}

export function EmbedBlock({data}: EmbedBlockProps) {
  return (
    <div
      className="mb-6"
      // className={css(EmbedBlockStyle)}
    >
      {embedForData(data)}
    </div>
  )
}
