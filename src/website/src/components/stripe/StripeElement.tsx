import {Elements} from '@stripe/react-stripe-js'
import React from 'react'
import {loadStripe} from '@stripe/stripe-js'

export interface StripeCardUpdateProps {
  children: React.ReactNode
  clientSecret?: string
}

const stripePromise = loadStripe(process.env.NEXT_PUBLIC_STRIPE_PUBLISHABLE_KEY)

export default function StripeElement({clientSecret, children}: StripeCardUpdateProps) {
  return (
    <Elements
      stripe={stripePromise}
      options={{
        clientSecret,
        locale: 'de'
      }}>
      {children}
    </Elements>
  )
}
