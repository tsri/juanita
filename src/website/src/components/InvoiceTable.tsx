import dateFormat from '../utils/dateFormat'
import React, {useMemo, useState} from 'react'
import {useTranslation} from 'next-i18next'
import {FullInvoiceFragment} from '../@types/codegen/api'
import {Button} from '@mui/material'

export interface InvoiceTableProps {
  invoices: FullInvoiceFragment[]
  onInvoicePayClick: (invoiceID: string) => Promise<void>
}

export function InvoiceTable({invoices, onInvoicePayClick}: InvoiceTableProps) {
  const {t} = useTranslation('common')

  const [payBtnDisabled, setPayBtnDisabled] = useState<boolean>(false)

  const sortedInvoices = useMemo(() => {
    return invoices?.sort(
      (a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
    )
  }, [invoices])

  function getPaymentStatus(invoice: FullInvoiceFragment) {
    if (invoice.canceledAt) {
      return (
        <div className={'mobile:mt-4 uppercase font-extrabold'}>
          {t('account.billingCanceledValue', {
            date: dateFormat(new Date(invoice.canceledAt), 'do MMMM yyyy')
          })}
        </div>
      )
    }
    if (invoice.paidAt) {
      return (
        <div className={'mobile:mt-4 uppercase font-extrabold'}>
          {t('account.billingPaidValue', {
            date: dateFormat(new Date(invoice.paidAt), 'do MMMM yyyy')
          })}
        </div>
      )
    }
    return (
      <>
        <Button
          onClick={async () => {
            setPayBtnDisabled(true)
            if (invoice.paidAt || invoice.canceledAt) {
              return
            }
            await onInvoicePayClick(invoice.id)
            setPayBtnDisabled(false)
          }}
          disabled={payBtnDisabled}>
          {t('account.billingPayNow')}
        </Button>
        <span className="flex h-3 w-3 absolute top-2 right-2">
          <span className="animate-ping absolute inline-flex h-full w-full rounded-full bg-error opacity-75"></span>
          <span className="relative inline-flex rounded-full h-3 w-3 bg-errorLight"></span>
        </span>
      </>
    )
  }

  return (
    <table className="billing border-collapse w-full">
      <thead>
        {sortedInvoices?.map(invoice => {
          return (
            <tr
              key={invoice.id}
              className="bg-tsri flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
              <th className="p-3 font-bold uppercase text-white border border-gray-300 hidden lg:table-cell">
                {t('account.billingDate')}
              </th>
              <th className="p-3 font-bold uppercase text-white border border-gray-300 hidden lg:table-cell">
                {t('account.billingReason')}
              </th>
              <th className="p-3 font-bold uppercase text-white border border-gray-300 hidden lg:table-cell">
                {t('account.billingAmount')}
              </th>
              <th className="p-3 font-bold uppercase text-white border border-gray-300 hidden lg:table-cell">
                {t('account.billingPaid')}
              </th>
            </tr>
          )
        })}
      </thead>
      <tbody>
        {sortedInvoices?.map(invoice => {
          let description = invoice.description
          if (invoice.items.length > 0 && invoice.items[0].description) {
            const itemDescriptionParts = invoice.items[0].description.split(' ')
            const subscriptionStartDate = new Date(itemDescriptionParts[1])
            const subscriptionEndDate = new Date(itemDescriptionParts[3])

            if (!isNaN(subscriptionStartDate.getTime()) && !isNaN(subscriptionEndDate.getTime())) {
              description = t('account.BillingDescriptionText', {
                fromDate: dateFormat(subscriptionStartDate, 'dd.MM.yyyy'),
                toDate: dateFormat(subscriptionEndDate, 'dd.MM.yyyy')
              })
            }
          }

          return (
            <tr
              key={invoice.id}
              className="bg-white lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0">
              <td className="w-full lg:w-auto p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
                <span className="lg:hidden absolute top-0 left-0 bg-tsri px-2 py-1 text-xs font-bold uppercase">
                  {t('account.billingDate')}
                </span>
                <div className="mobile:mt-4">
                  {t('account.billingDateValue', {
                    date: dateFormat(new Date(invoice.createdAt), 'do MMMM yyyy')
                  })}
                </div>
              </td>
              <td className="w-full lg:w-auto p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
                <span className="lg:hidden absolute top-0 left-0 bg-tsri px-2 py-1 text-xs font-bold uppercase">
                  {t('account.billingReason')}
                </span>
                <div className="mobile:mt-4">{description}</div>
              </td>

              <td className="w-full lg:w-auto p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
                <span className="lg:hidden absolute top-0 left-0 bg-tsri px-2 py-1 text-xs font-bold uppercase">
                  {t('account.billingAmount')}
                </span>
                <div className="mobile:mt-4">
                  {t('account.billingAmountValue', {amount: invoice.total / 100})}
                </div>
              </td>
              <td
                className={`w-full lg:w-auto p-3 text-center border border-b block lg:table-cell lg:relative relative lg:static`}>
                {getPaymentStatus(invoice)}
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}
