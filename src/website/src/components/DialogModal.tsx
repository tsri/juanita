import React, {useEffect, useRef, useState} from 'react'
import {useTranslation} from 'next-i18next'

export interface ModalProps {
  show: boolean
  children: React.ReactNode
  showTopClose: boolean
  allowBackgroundClose: boolean
  onClose(): void
}

export default function DialogModal({
  show,
  children,
  showTopClose,
  allowBackgroundClose,
  onClose
}: ModalProps) {
  const {t} = useTranslation('common')

  const [isModalVisible, setIsModalVisible] = useState<boolean>(false)

  useEffect(() => {
    setIsModalVisible(show)
  }, [show])

  const ref = useRef<HTMLDialogElement>(null)

  useEffect(() => {
    // @ts-ignore
    if (isModalVisible && !ref.current?.open) {
      // @ts-ignore
      ref.current?.showModal()
    } else {
      onClose()
      // @ts-ignore
      ref.current?.close()
    }
  }, [isModalVisible])

  const handlingClose = () => {
    setIsModalVisible(false)
    onClose()
  }

  const onDialogClick = () => {
    if (allowBackgroundClose) {
      setIsModalVisible(false)
      onClose()
    }
  }

  const preventAutoClose = (e: React.MouseEvent) => e.stopPropagation()

  return (
    <dialog className="backdrop-blur-md max-w-lg" ref={ref} onClick={onDialogClick}>
      <div onClick={preventAutoClose}>
        {showTopClose && (
          <div className="absolute top-0 right-4 mt-2">
            <button onClick={() => handlingClose()}>{t('loginModal.close')}</button>
          </div>
        )}
        <div className="mt-4">{children}</div>
      </div>
    </dialog>
  )
}
