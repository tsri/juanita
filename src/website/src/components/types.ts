import {Node} from 'slate'
import {
  FlexTeaserFragment,
  FullAuthorFragment,
  LinkPageBreakBlockDataFragment,
  PublicProperties,
  ReducedImageFragment,
  TeaserFragment
} from '../@types/codegen/graphql'

export enum VersionState {
  Draft = 'draft',
  DraftReview = 'draftReview',
  Published = 'published'
}

export interface ImageData {
  url: string
  width: number
  height: number
  description?: string
  caption?: string
  author?: string
  focusPoint?: {x: number; y: number}
  extension: string

  ogURL: string
  smallTeaserURL: string
  mediumTeaserURL: string
  largeURL: string
}

export interface Author {
  id: string
  url: string
  slug: string
  name: string
  jobTitle?: string
  image: ImageData
  bio?: RichTextBlock[]
  links?: any[]
}

/*export interface NavigationItem {
  title: string
  route?: Route
  url?: string
  isActive: boolean
}*/

export interface ArticleMeta {
  id: string
  url: string

  peer?: Peer
  authors: FullAuthorFragment[]
  tags: string[]

  publishedAt: Date
  updatedAt: Date

  preTitle?: string
  title: string
  lead: string
  image: any
  slug?: string
  isBreaking: boolean

  socialMediaTitle?: string
  socialMediaDescription?: string
  socialMediaAuthors: FullAuthorFragment[]
  socialMediaImage?: any

  canonicalUrl?: string

  teaserType?: TeaserType
  teaserStyle?: TeaserStyle
}

export type PublishedArticle = ArticleMeta & {
  blocks: Block[]
}

export interface PublicProperty {
  key: string
  value: string
}

export interface PageMeta {
  id: string
  updatedAt: Date
  publishedAt: Date
  slug: string
  url: string
  title: string
  description: string
  tags: string[]
  properties: PublicProperties
  image: ReducedImageFragment
  socialMediaTitle?: string
  socialMediaDescription?: string
  socialMediaImage: ReducedImageFragment
}

export type PublishedPage = PageMeta & {
  blocks: Block[]
}

export enum TeaserType {
  Article = 'article',
  PeerArticle = 'peerArticle',
  Page = 'page'
}

export enum TeaserStyle {
  Default = 'default',
  Light = 'light',
  Text = 'text',
  Breaking = 'breaking'
}

export interface Front {
  id: string
}

export interface Page {
  id: string
}

export interface Peer {
  id: string
  slug: string
  name: string
  logoURL: string
  websiteURL: string
  themeColor: string
  callToActionText: Node[]
  callToActionURL: string
}

export enum BlockType {
  Foo = 'foo',
  Bar = 'bar',

  // Content
  TitleImage = 'titleImage',
  Title = 'title',
  RichText = 'RichTextBlock',
  Gallery = 'gallery',
  Teaser = 'teaser',
  Embed = 'embed',
  Quote = 'quote',
  Image = 'image',
  Listicle = 'listicle',
  LinkPageBreak = 'linkPageBreak',
  HTML = 'html',

  // Layout
  Grid = 'grid',
  GridFlex = 'gridFlex'
}

export interface BaseBlock<T extends BlockType, V> {
  type: T
  key: string
  value: V
}

// Content Blocks
// --------------
export enum HeaderType {
  Default = 'default',
  Breaking = 'breaking'
}
export type TitleBlockValue = {
  type: HeaderType
  preTitle?: string
  date?: Date
  title: string
  lead: string
  isHeader?: boolean
}
export type TitleBlock = BaseBlock<BlockType.Title, TitleBlockValue>

export type QuoteBlock = BaseBlock<BlockType.Quote, {text: string; author: string}>

export type HTMLBlock = BaseBlock<BlockType.HTML, {html: string}>

export type ImageBlock = BaseBlock<BlockType.Image, ImageData>
export type TitleImageBlock = BaseBlock<BlockType.TitleImage, ImageData>

export type ListicleBlock = BaseBlock<
  BlockType.Listicle,
  {
    title: string
    image: ImageData
    text: Node[]
  }[]
>

export type PeerPageBreakBlock = BaseBlock<BlockType.LinkPageBreak, LinkPageBreakBlockDataFragment>

export type RichTextBlock = BaseBlock<BlockType.RichText, Node[]>

export type GalleryBlock = BaseBlock<
  BlockType.Gallery,
  {
    title: string
    media: any[]
  }
>

export type TeaserBlock = BaseBlock<BlockType.Teaser, PublishedArticle | PublishedPage>

export enum EmbedType {
  FacebookPost = 'facebookPost',
  InstagramPost = 'instagramPost',
  TwitterTweet = 'twitterTweet',
  VimeoVideo = 'vimeoVideo',
  YouTubeVideo = 'youTubeVideo',
  SoundCloudTrack = 'soundCloudTrack',
  IFrame = 'iframe',
  BildwurfAd = 'bildwurfAd'
}

export interface FacebookPostEmbedData {
  type: EmbedType.FacebookPost
  userID: string
  postID: string
}

export interface InstagramPostEmbedData {
  type: EmbedType.InstagramPost
  postID: string
}

export interface TwitterTweetEmbedData {
  type: EmbedType.TwitterTweet
  userID: string
  tweetID: string
}

export interface VimeoVideoEmbedData {
  type: EmbedType.VimeoVideo
  videoID: string
}

export interface YouTubeVideoEmbedData {
  type: EmbedType.YouTubeVideo
  videoID: string
}

export interface SoundCloudTrackEmbedData {
  type: EmbedType.SoundCloudTrack
  trackID: string
}

export interface BildwurfAdEmbedData {
  type: EmbedType.BildwurfAd
  zoneID: string
}

export interface IFrameEmbed {
  type: EmbedType.IFrame
  title?: string
  url?: string
  width?: number
  height?: number
  styleCustom?: string
}

export type EmbedData =
  | FacebookPostEmbedData
  | InstagramPostEmbedData
  | TwitterTweetEmbedData
  | VimeoVideoEmbedData
  | YouTubeVideoEmbedData
  | SoundCloudTrackEmbedData
  | BildwurfAdEmbedData
  | IFrameEmbed

export type EmbedBlock = BaseBlock<BlockType.Embed, EmbedData>

// Layout Blocks
// -------------

export type GridBlock = BaseBlock<
  BlockType.Grid,
  {
    numColumns: number
    teasers: TeaserFragment[]
  }
>

export type GridFlexBlock = BaseBlock<
  BlockType.GridFlex,
  {
    flexTeasers: FlexTeaserFragment[]
  }
>

// Block Unions
// ------------

export type Block =
  | RichTextBlock
  | GalleryBlock
  | EmbedBlock
  | ImageBlock
  | QuoteBlock
  | HTMLBlock
  | ListicleBlock
  | PeerPageBreakBlock
  | TitleBlock
  | TitleImageBlock
  | GridBlock
  | GridFlexBlock
  | TeaserBlock

// Image
export interface ImageRefData {
  id: string

  filename?: string
  extension: string
  title?: string
  description?: string

  width: number
  height: number

  url: string
  largeURL: string
  mediumURL: string
  thumbURL: string

  column1URL: string
  column6URL: string
  previewURL: string
  squareURL: string
}

// Article Reference
export interface ArticleReference {
  id: string

  createdAt: string
  modifiedAt: string

  draft?: {revision: number}
  pending?: {revision: number}
  published?: {revision: number}

  latest: {
    revision: number
    publishAt?: string
    publishedAt?: string
    updatedAt?: string
    title?: string
    lead?: string
    image?: ImageRefData
  }
}

// Page Info
export interface PageInfo {
  readonly startCursor?: string
  readonly endCursor?: string
  readonly hasNextPage: boolean
  readonly hasPreviousPage: boolean
}
