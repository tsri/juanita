import React from 'react'
import {useTranslation} from 'next-i18next'

export interface SectionButtonsProps {
  editButtonText?: string
  saveButtonText?: string
  cancelButtonText?: string
}

export interface SectionProps {
  buttons?: SectionButtonsProps
  title: string
  children: any
  onSave?: any
  isUpdating?: boolean
  toggleUpdating?: (isUpdating: boolean) => void
  disabled?: boolean
}

export default function Section({
  buttons = {},
  title,
  children,
  onSave,
  isUpdating,
  toggleUpdating,
  disabled
}: SectionProps) {
  const {t} = useTranslation('common')
  const {
    editButtonText = t('edit'),
    saveButtonText = t('save'),
    cancelButtonText = t('abort')
  } = buttons
  return (
    <section className="my-10 flex flex-row justify-between flex-wrap">
      <h2 className="w-full md:w-1/4">{title}</h2>
      <div className="w-full md:w-1/2 flex flex-wrap mt-2 md:p-2">{children}</div>
      <div className="w-full flex flex-col md:w-1/4">
        {isUpdating && (
          <>
            <button
              disabled={disabled}
              type="submit"
              onClick={onSave}
              className={`px-5 py-3 lg:px-10 lg:py-3 lg:text-lg lg:inline-block text-white bg-tsri`}>
              {saveButtonText}
            </button>
            <button
              onClick={() => toggleUpdating(false)}
              className={`mt-5 px-5 py-3 lg:px-10 lg:py-3 lg:text-lg lg:inline-block text-white bg-black`}>
              {cancelButtonText}
            </button>
          </>
        )}
        {!isUpdating && toggleUpdating && !disabled && (
          <button
            type="submit"
            onClick={() => toggleUpdating(true)}
            className={`px-5 py-3 lg:px-10 lg:py-3 lg:text-lg lg:inline-block text-white bg-black`}>
            {editButtonText}
          </button>
        )}
      </div>
    </section>
  )
}
