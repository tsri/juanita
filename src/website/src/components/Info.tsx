import React from 'react'

interface InfoProps {
  label: string
  value: any
  type?: string
  name?: string
  onChange?: any
  isUpdating?: boolean
}

const Info = ({label, value, type = 'text', name, onChange, isUpdating}: InfoProps) => (
  <div className="mb-10 w-full lg:w-1/2 lg:pr-2">
    <p className="uppercase">{label}</p>
    {isUpdating ? (
      <input
        className="w-full border-solid border-2 p-2"
        name={name}
        value={value}
        type={type}
        onChange={onChange}
      />
    ) : (
      <p className="font-semibold">{value ?? '--'}</p>
    )}
  </div>
)

export default Info
