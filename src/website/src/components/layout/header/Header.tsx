import React, {useMemo, useState} from 'react'
import {Burger, MegaMenu} from '../Menu'
import Link from 'next/link'
import {NavsProps} from '../../../pages/_app'
import {useRouter} from 'next/router'
import HeaderUserMenu from './HeaderUserMenu'
import {usePeerProfileQuery} from '../../../@types/codegen/graphql'

export type HeaderNavigations = Omit<NavsProps, 'footerNavigation' | 'ribbonNavigation'>

export interface HeaderProps {
  headerNavigations: Partial<HeaderNavigations>
  hideLogo?: boolean
}

export default function Header(props: HeaderProps) {
  const [isMegaMenuOpen, toggleMenu] = useState(false)
  const containerClassName = isMegaMenuOpen ? 'bg-blue  z-20' : 'bg-white'
  const {route, push} = useRouter()

  const {data: peerProfile} = usePeerProfileQuery()
  const logoUrl = useMemo<string | undefined>(() => peerProfile?.peerProfile?.logo?.mediumURL, [
    peerProfile
  ])

  async function handleProfileClick() {
    toggleMenu(false)
  }

  const hideMemberBtn: boolean = useMemo(() => {
    return route === '/mitmachen-rettet-zueri'
  }, [route])

  return (
    <header
      className={`top-0 z-30 w-screen ${isMegaMenuOpen ? 'fixed mobile:h-screen' : 'sticky'}`}>
      <div
        onClick={event => {
          event.stopPropagation()
        }}
        className={`${containerClassName} top-0 transition-all duration-200 ease-in-out flex flex-col py-4 md:py-3 lg:py-4 xl:py-7 z-20`}>
        <div className="mx-4 sm:mx-auto sm:container">
          <div className="w-full flex items-center">
            <div className="order-2 flex-1 lg:flex-none">
              {!props.hideLogo && !!logoUrl && (
                <Link href="/">
                  <img
                    className="w-20 md:w-24 lg:w-32 xl:w-40 cursor-pointer fade-in"
                    src={logoUrl}
                    alt="Tsri logo"
                  />
                </Link>
              )}
            </div>
            <div className={`flex order-1 md:flex-1 ${isMegaMenuOpen ? 'text-white' : ''}`}>
              <Burger isOpen={isMegaMenuOpen} onClick={() => toggleMenu(!isMegaMenuOpen)} />
              <nav className="font-semibold md:text-lg md:pt-1">
                <ul className="md:inline-block md:flex hidden">
                  <li className="px-4 uppercase">
                    <Link href="/">Home</Link>
                  </li>
                  <li className="px-4 uppercase">
                    <Link href="/agenda">Agenda</Link>
                  </li>
                </ul>
              </nav>
            </div>
            <div className="order-3 lg:flex-1 flex items-center justify-end">
              {/* profile menu */}
              <HeaderUserMenu />

              {/* link to store */}
              <img
                onClick={() => {
                  window.location.href = `${process.env.NEXT_PUBLIC_SHOP_URL}`
                }}
                className="w-7 h-7 ml-2 md:mx-4 xl:mx-6 cursor-pointer"
                src="/icons/cart.png"
                alt="search"
              />

              {/* link to /mitmachen */}
              {!hideMemberBtn && (
                <button
                  onClick={async e => {
                    await push('/mitmachen')
                  }}
                  className={`ml-5 px-5 py-3 lg:px-3 lg:py-3 lg:text-lg lg:inline-block hidden ${
                    isMegaMenuOpen ? 'bg-white' : 'bg-blue text-white'
                  }`}>
                  Werde Tsüri Member
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
      {isMegaMenuOpen && (
        <MegaMenu
          className="fixed mobile:overflow-y-scroll bg-blue transition-all duration-200 ease-in-out mobile:h-full py-4 md:py-3 lg:px-12 lg:py-4 xl:py-7"
          headerNavigations={props.headerNavigations}
        />
      )}
    </header>
  )
}
