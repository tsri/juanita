import React, {useContext} from 'react'
import {Menu, MenuItem} from '@mui/material'
import {AuthContext} from '../../auth/AuthContextProvider'
import {useRouter} from 'next/router'

export default function HeaderUserMenu() {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)
  const authContext = useContext(AuthContext)
  const router = useRouter()

  /**
   * FUNCTIONS
   */
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }
  function logout() {
    authContext.logout()
    handleClose()
  }
  async function goToProfile(): Promise<void> {
    handleClose()
    await router.push('/account/profile')
  }

  return (
    <>
      <span
        id="user-icon-btn"
        className="w-7 h-7 mx-2 md:mx-4 xl:mx-6 relative inline-block cursor-pointer"
        onClick={handleClick}>
        <img className="" src="/icons/account.png" alt="account icon" />
        {authContext.subscriptionNeedsAction && (
          <span className="absolute top-0 -right-2 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-white transform -translate-y-1/2 bg-red rounded-full">
            !
          </span>
        )}
      </span>

      <Menu
        id="menu-container"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}>
        {authContext.isAuthenticated && (
          <div>
            <MenuItem onClick={goToProfile}>Profile</MenuItem>
            <MenuItem onClick={logout}>Logout</MenuItem>
          </div>
        )}
        {!authContext.isAuthenticated && <MenuItem onClick={goToProfile}>Anmelden</MenuItem>}
      </Menu>
    </>
  )
}
