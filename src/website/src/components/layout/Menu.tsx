import styled from 'styled-components'
import {useState} from 'react'
import useWindowDimensions from 'components/utils/UseWindowDimensions'
import Link from 'next/link'
import {useTranslation} from 'next-i18next'
import {HeaderNavigations} from './header/Header'

const StyledBurger = styled.button`
  &.opened div {
    background: ${({theme}) => theme.colors.white};
  }

  &.opened div:nth-child(1) {
    -webkit-transform: rotate(-45deg) translate(-9px, 6px);
    transform: rotate(-45deg) translate(-9px, 6px);
  }

  &.opened div:nth-child(2) {
    opacity: 0;
  }

  &.opened div:nth-child(3) {
    -webkit-transform: rotate(45deg) translate(-9px, -6px);
    transform: rotate(45deg) translate(-9px, -6px);
  }
`

export const Burger = ({onClick, isOpen}) => (
  <StyledBurger
    className={`inline-block mr-4 lg:mr-7 focus:outline-none ${isOpen ? 'opened' : ''}`}
    onClick={onClick}>
    <div className="transition duration-500 ease-in-out w-9 h-0.5 my-2 bg-tsri" />
    <div className="transition duration-500 ease-in-out w-9 h-0.5 my-2 bg-tsri" />
    <div className="transition duration-500 ease-in-out w-9 h-0.5 my-2 bg-tsri" />
  </StyledBurger>
)

export interface MegaMenuProps {
  className?: string
  headerNavigations: Partial<HeaderNavigations>
}

const NavigationList = (props: any) => {
  const {width} = useWindowDimensions()
  const {navigation} = props

  // default value will equal either a passed prop from parent, or equal true if screen is less than md or false.
  const [isNavOpen, toggleNav] = useState(props.isNavOpen || width >= 768 ? true : false)

  if (!navigation) return null

  return (
    <div>
      <h4 className="text-2xl font-medium mb-5" onClick={() => toggleNav(!isNavOpen)}>
        {navigation?.name}
        <i
          className={`arrow ${isNavOpen ? 'down' : 'right'} ml-5 md:hidden transition duration-300`}
        />
      </h4>
      {isNavOpen && (
        <ul>
          {navigation?.links?.map(({article, page, url, label}, i) => {
            const href = article?.slug
              ? `/a/${article.id}/${article.slug}`
              : page?.slug
              ? `/${page?.slug}`
              : url
            return (
              <li
                className="border-b-solid border-b-2 text-xl uppercase md:border-b-0 md:my-4 mobile:mb-6"
                key={i}>
                <Link href={href || '/404'} legacyBehavior>
                  {label}
                </Link>
              </li>
            )
          })}
        </ul>
      )}
    </div>
  )
}

export const MegaMenu = ({className, headerNavigations}: MegaMenuProps) => {
  const {
    aboutUsNavigation,
    eventsNavigation,
    fokusMonatNavigation,
    categoriesNavigation
  } = headerNavigations
  const {t} = useTranslation('common')
  return (
    <nav className={`z-30 flex flex-row text-white min-w-min justify-center px-6 ${className}`}>
      <div className="grid lg:grid-cols-2 grid-cols-1 w-full">
        <nav className="grid md:grid-cols-3 grid-cols-1 gap-4 w-full">
          <div className="md:hidden">
            <Link href="/">
              <h4 className="text-2xl font-medium mb-5">Home</h4>
            </Link>
          </div>
          <div className="md:hidden">
            <Link href="/agenda" legacyBehavior>
              <div>
                <h4 className="text-2xl font-medium mb-5">{t('header.agenda')}</h4>
              </div>
            </Link>
          </div>
          <NavigationList navigation={categoriesNavigation} isNavOpen={true} />
          <div>
            <NavigationList navigation={eventsNavigation} />
            <NavigationList navigation={aboutUsNavigation} />
          </div>
          <NavigationList navigation={fokusMonatNavigation} />
        </nav>
        <div className="w-full">
          <Link href={`${process.env.NEXT_PUBLIC_WEBSITE_URL}/mitmachen`} legacyBehavior>
            <img src="/mega-menu.png" alt="" className="sm:m-auto pb-5" />
          </Link>
        </div>
      </div>
    </nav>
  )
}
