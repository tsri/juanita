import Button, {ButtonType} from 'components/atoms/Button'
import {AuthContext} from 'components/auth/AuthContextProvider'
import {differenceInHours} from 'date-fns'
import Link from 'next/link'
import {useContext, useEffect, useState} from 'react'

export function Paywall() {
  const authContext = useContext(AuthContext)
  const [display, setDisplay] = useState(false)
  const [offsetTop, setOffsetTop] = useState(0)

  // Hide paywall for logged in users, on the registration and login
  // page, and if it was hidden within the last 24 hours
  useEffect(() => {
    const path = window.location.pathname
    const isLoggedIn = Number(localStorage.getItem('userAuthenticated')) ?? 0
    const lastClosedTime = Number(localStorage.getItem('paywallLastClosed')) ?? 0
    const currentTime = new Date().getTime()

    if (
      !isLoggedIn &&
      path !== '/mitmachen' &&
      path !== '/account/profile' &&
      differenceInHours(currentTime, lastClosedTime) > 24
    ) {
      setDisplay(true)
    } else {
      setDisplay(false)
    }
  })

  // The isAuthenticated property changes with a delay of some seconds.
  // Cache it across pageloads, so the previous value will be used.
  useEffect(() => {
    localStorage.setItem('userAuthenticated', authContext.isAuthenticated ? '1' : '0')
  }, [authContext.isAuthenticated])

  useEffect(() => {
    const header = document.querySelector('header')
    const topHeight = header.offsetHeight
    setOffsetTop(topHeight)
  }, [])

  const handleClose = () => {
    setDisplay(false)
    localStorage.setItem('paywallLastClosed', new Date().getTime().toString())
  }

  if (!display) {
    return null
  }

  return (
    <section className="z-20 w-full p-5 bg-tsri text-white fixed" style={{top: offsetTop}}>
      <button
        className="text-6xl absolute top-5 right-5"
        style={{lineHeight: '0.5em'}}
        onClick={handleClose}>
        &times;
      </button>
      <div className="grid grid-cols-3 gap-4 lg:w-1/2 lg:mx-auto">
        <h2 className="col-span-2">Werde Teil von Tsüri.ch</h2>
        <p className="col-span-2">
          Wir haben weder einen reichen Onkel noch ein börsenkotiertes Unternehmen im Nacken.
          Tsüri.ch wird von 1500 Membern unterstützt, die Journalismus allen Menschen zugänglich
          machen wollen. Werde auch du Teil der Community!
        </p>
        <div
          className="flex flex-col mt-1 lg:col-start-3 lg:row-start-1 lg:row-span-2"
          style={{justifyContent: 'space-between'}}>
          <Link href={`${process.env.NEXT_PUBLIC_WEBSITE_URL}/mitmachen`} className="mb-5">
            <Button className={'px-2 lg:px-2 text-base'} type={ButtonType.Secondary}>
              Jetzt Member werden
            </Button>
          </Link>

          <Link href={`${process.env.NEXT_PUBLIC_WEBSITE_URL}/account/profile`}>
            <Button className={'px-2 lg:px-2 text-base'} type={ButtonType.Secondary}>
              Zum Login
            </Button>
          </Link>
        </div>
      </div>
    </section>
  )
}
