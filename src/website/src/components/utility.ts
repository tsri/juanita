import {useRef, useEffect, useState, useMemo, useCallback} from 'react'

export function useScript(src: string, checkIfLoaded: () => boolean, crossOrigin: boolean = false) {
  const scriptRef = useRef<HTMLScriptElement | null>(null)

  const [isLoading, setLoading] = useState(false)
  const [isLoaded, setLoaded] = useState(() => checkIfLoaded())

  useEffect(() => {
    if (isLoading && !isLoaded && !scriptRef.current) {
      const script = document.createElement('script')

      script.src = src
      script.async = true
      script.defer = true
      script.onload = () => setLoaded(true)
      script.crossOrigin = crossOrigin ? 'anonymous' : null

      document.head.appendChild(script)
      scriptRef.current = script
    }
  }, [isLoading])

  const load = useCallback(() => {
    setLoading(true)
  }, [])

  const memo = useMemo(
    () => ({
      isLoading,
      isLoaded,
      load
    }),
    [isLoading, isLoaded, load]
  )
  return typeof window != 'object' ? {isLoaded: false, isLoading: false, load: () => {}} : memo
}

export function wordVariations(word: string): string[] {
  const lower = word.toLowerCase()
  const upper = word.toUpperCase()
  const capitalized = word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()

  return [lower, upper, capitalized]
}

export function convertMSToDHMS(milliseconds: number) {
  let seconds = Math.floor(milliseconds / 1000)
  let minute = Math.floor(seconds / 60)

  seconds = seconds % 60

  let hour = Math.floor(minute / 60)

  minute = minute % 60

  let day = Math.floor(hour / 24)

  hour = hour % 24

  return {
    day: day,
    hour: hour,
    minute: minute,
    seconds: seconds
  }
}

// TODO: Move into Component which can auto update
export function getHumanReadableTimePassed(date: Date): string {
  const now = new Date()
  const diff = now.getTime() - date.getTime()
  const diffInDHMS = convertMSToDHMS(diff)

  if (diffInDHMS.day > 3) {
    const FormattedDate = Intl.DateTimeFormat('de-CH', {
      year: '2-digit',
      month: '2-digit',
      day: '2-digit'
    }).format(date)

    const FormattedTime = Intl.DateTimeFormat('de-CH', {
      hour: '2-digit',
      minute: '2-digit'
    }).format(date)
    return `${FormattedDate}, ${FormattedTime}`
  }

  if (diffInDHMS.day === 1) {
    return `vor ${diffInDHMS.day} Tag`
  }

  if (diffInDHMS.day > 1) {
    return `vor ${diffInDHMS.day} Tagen`
  }

  if (diffInDHMS.hour === 1) {
    return `vor ${diffInDHMS.hour} Stunde`
  }

  if (diffInDHMS.hour > 1) {
    return `vor ${diffInDHMS.hour} Stunden`
  }

  return `vor ${diffInDHMS.minute} Minuten`
}

export function transformCssStringToObject(styleCustom: string): object {
  const styleRules = styleCustom.split(';')
  if (styleRules.length === 0) return {}
  return styleRules.reduce((previousValue: object, currentValue: string) => {
    const [key, value] = currentValue.split(':')
    if (key && value) {
      return Object.assign(previousValue, {[key.trim()]: value.trim()})
    }
    return previousValue
  }, {})
}

export function useWindowSize() {
  // Initialize state with undefined width/height so server and client renders match
  // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined
  })

  useEffect(() => {
    // Handler to call on window resize
    function handleResize() {
      // Set window width/height to state
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight
      })
    }

    // Add event listener
    window.addEventListener('resize', handleResize)

    // Call handler right away so state gets updated with initial window size
    handleResize()

    // Remove event listener on cleanup
    return () => window.removeEventListener('resize', handleResize)
  }, []) // Empty array ensures that effect is only run on mount

  return windowSize
}
