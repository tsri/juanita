import React, {useContext, useEffect, useMemo, useState} from 'react'
import {useTranslation} from 'next-i18next'
import {
  FullMemberPlanFragment,
  FullSubscriptionFragment,
  PaymentPeriodicity,
  useCancelSubscriptionMutation,
  useCheckInvoiceStatusLazyQuery,
  useCreatePaymentFromInvoiceMutation,
  useUpdateSubscriptionMutation
} from '../../../@types/codegen/graphql'
import {handleError} from '../../../helpers'
import dateFormat from '../../../utils/dateFormat'
import {toast} from 'react-toastify'
import * as Sentry from '@sentry/nextjs'
import {ChooseRightModal} from '../../Modal'
import StripeElement from '../../stripe/StripeElement'
import StripePayment from '../../stripe/StripePayment'
import {InvoiceTable} from '../../InvoiceTable'
import Section from '../../Section'
import Info from '../../Info'
import Button, {ButtonType} from '../../atoms/Button'
import {AuthContext} from '../../auth/AuthContextProvider'
import Link from 'next/link'
import {
  FullInvoiceFragment,
  InvoiceListQuery,
  InvoiceListQueryVariables
} from '../../../@types/codegen/api'
import {ApolloQueryResult} from '@apollo/client'

interface IntentAndInvoice {
  intentSecret: string
  invoiceID: string
}

type SubscriptionItemProps = {
  subscription: FullSubscriptionFragment
  memberPlan: FullMemberPlanFragment
  isUpdating: boolean
  setIsUpdating: (isUpdating: boolean) => void
  invoices: FullInvoiceFragment[]
  reloadInvoices: (
    variables?: Partial<InvoiceListQueryVariables>
  ) => Promise<ApolloQueryResult<InvoiceListQuery>>
}

export default function SubscriptionItem({
  subscription,
  memberPlan,
  isUpdating,
  setIsUpdating,
  invoices,
  reloadInvoices
}: SubscriptionItemProps) {
  const {t} = useTranslation('common')
  const authContext = useContext(AuthContext)
  const [updateSubscriptionDetails] = useUpdateSubscriptionMutation()
  const [createPaymentFromInvoice] = useCreatePaymentFromInvoiceMutation()
  const [cancelUserSubscription] = useCancelSubscriptionMutation()

  const [checkInvoiceStatus, {loading: invoiceChecking}] = useCheckInvoiceStatusLazyQuery({
    fetchPolicy: 'no-cache',
    onError: error => handleError({errorText: error.message})
  })

  const [paymentMethod, setPaymentMethod] = useState(subscription?.paymentMethod)
  const [monthlyAmount, setMonthlyAmount] = useState<number | undefined>(
    Math.floor(subscription.monthlyAmount / 100)
  )
  const [intentSecret, setIntentSecret] = useState<IntentAndInvoice | null>(null)
  const [mutableSubscription, setMutableSubscription] = useState<
    FullSubscriptionFragment | undefined
  >(JSON.parse(JSON.stringify(subscription)))

  const subscriptionDeactivated = useMemo(() => {
    if (!mutableSubscription.deactivation) {
      return false
    }
    const deactivationDate = new Date(mutableSubscription.deactivation.date)
    return deactivationDate <= new Date()
  }, [mutableSubscription])

  const handleCancelSubscription = async () => {
    const message = t('account.confirmDeactivation', {
      date: dateFormat(new Date(mutableSubscription.paidUntil), 'do MMMM yyyy')
    })

    if (confirm(message)) {
      try {
        const response = await cancelUserSubscription({variables: {id: mutableSubscription.id}})
        const {cancelUserSubscription: updatedUserSubscription} = response.data
        if (!updatedUserSubscription) throw new Error('Server did not return data')
        authContext.fetchSubscriptions()
        setIsUpdating(false)
        toast('Dein Abo wurde gekündet.', {type: 'success'})
      } catch (error) {
        handleError({errorText: error})
      }
    }
  }

  async function createPaymentFromInvoiceAndExecuteAction(invoiceID) {
    try {
      const paymentResponse = await createPaymentFromInvoice({
        variables: {
          input: {
            invoiceID: invoiceID,
            paymentMethodID: paymentMethod.id,
            successURL: `${window.location.href}?paymentSuccess=true&invoiceID=${invoiceID}`,
            failureURL: `${window.location.href}?paymentSuccess=false`
          }
        }
      })

      const {createPaymentFromInvoice: payment} = paymentResponse.data
      if (payment.paymentMethod.paymentProviderID === 'stripe') {
        setIntentSecret({
          intentSecret: payment.intentSecret,
          invoiceID: invoiceID
        })
      } else if (payment.intentSecret.startsWith('http')) {
        window.location.href = payment.intentSecret
      } else {
        // used for tokenization
        await reloadInvoices()
        // to be sure, invoices are re-loaded
        window.location.reload()
      }
    } catch (error) {
      handleError({errorText: error.toString()})
      // to be sure, invoices are re-loaded. give user some time to read the error
      setTimeout(() => {
        window.location.reload()
      }, 5000)
    }
  }

  const handleSave = async (executePaymentAction: boolean = false) => {
    try {
      await updateSubscriptionDetails({
        variables: {
          id: mutableSubscription.id,
          input: {
            id: mutableSubscription.id,
            autoRenew: mutableSubscription.autoRenew,
            monthlyAmount: monthlyAmount * 100,
            memberPlanID: mutableSubscription.memberPlan?.id,
            paymentMethodID: mutableSubscription.paymentMethod?.id,
            paymentPeriodicity: mutableSubscription.paymentPeriodicity
          }
        }
      })
      // re-load subscriptions from server
      authContext.fetchSubscriptions()
      setIsUpdating(false)
    } catch (error) {
      toast(t('account.errorToast'))
      Sentry.captureException(error)
    }
  }

  /**
   * Check api, if invoices have been paid in the meantime.
   */
  async function checkOpenInvoices() {
    if (!invoices) {
      return
    }
    // skip, if no open invoice
    for (const invoice of invoices) {
      // invoice paid or cancelled. No need to check it.
      if (invoice.paidAt || invoice.canceledAt) {
        continue
      }
      await checkInvoiceStatus({variables: {id: invoice.id}})
    }
  }

  const nonAutoRenewDateHelper: string = useMemo(() => {
    const deactivationDate = mutableSubscription?.deactivation?.date
    const paidUntil = mutableSubscription.paidUntil
    const formatStr = 'do MMMM yyyy'
    if (paidUntil || deactivationDate) {
      return t('account.autoRenewFalse', {
        endDate: dateFormat(new Date(paidUntil || deactivationDate), formatStr)
      })
    }
    return 'Wird nicht automatisch erneuert'
  }, [mutableSubscription])

  /**
   * compute open invoice out of invoices
   */
  const openInvoice: FullInvoiceFragment | null = useMemo(() => {
    const openInvoice =
      invoices?.find(invoice => invoice.paidAt === null && invoice.canceledAt === null) || null
    return openInvoice
  }, [invoices])

  const hasUnpaidInvoice = useMemo<boolean>(() => {
    return !!openInvoice
  }, [invoices])

  useEffect(() => {
    if (mutableSubscription?.paymentMethod) {
      setPaymentMethod(mutableSubscription.paymentMethod)
    }
  }, [mutableSubscription])

  useEffect(() => {
    checkOpenInvoices()
  }, [])

  return (
    <>
      <ChooseRightModal
        show={intentSecret !== null}
        allowBackgroundClose={false}
        showTopClose={false}
        onClose={() => setIntentSecret(null)}>
        {intentSecret && (
          <StripeElement clientSecret={intentSecret.intentSecret}>
            <StripePayment
              amount={invoices.find(invoice => invoice.id === intentSecret.invoiceID)?.total / 100}
              onClose={async () => {
                setTimeout(() => {
                  // give stripe some time => todo: absolutely find a better solution => will be removed when migrating to website builder of we.publish
                  location.reload()
                }, 1500)
              }}
            />
          </StripeElement>
        )}
      </ChooseRightModal>

      <h1 className="w-full mt-10">{memberPlan?.name}</h1>

      {openInvoice && !invoiceChecking && (
        <section className="my-10 flex flex-row justify-between flex-wrap">
          <h2 className="w-full md:w-1/4 text-error mb-4">{t('account.billingOpen')}</h2>
          <InvoiceTable
            invoices={[openInvoice]}
            onInvoicePayClick={async invoiceID => {
              await createPaymentFromInvoiceAndExecuteAction(invoiceID)
            }}
          />
        </section>
      )}

      <Section title={`${t('account.subscription')} ${subscriptionDeactivated ? '(inaktiv)' : ''}`}>
        <Info
          label={t('account.subscription')}
          name="startsAt"
          value={t('account.subscriptionValue', {
            memberSince: dateFormat(new Date(mutableSubscription.startsAt), 'do MMMM yyyy')
          })}
        />

        <Info
          label={t('account.paidUntil')}
          name="paidUntil"
          value={
            mutableSubscription.paidUntil
              ? t('account.paidUntilValue', {
                  paidUntil: dateFormat(new Date(mutableSubscription.paidUntil), 'do MMMM yyyy')
                })
              : t('account.notYetPaid')
          }
        />

        <Info
          label={t('account.yearlyAmount')}
          name="monthlyAmount"
          value={t('account.yearlyAmountValue', {
            amount: (mutableSubscription.monthlyAmount * 12) / 100
          })}
        />

        {(subscriptionDeactivated || hasUnpaidInvoice) && (
          <div className="mb-10 w-full">
            {subscriptionDeactivated && (
              <>
                <p className="font-semibold">{t('account.subscriptionNeedsRenewalDeactivated')}</p>
                <Link href={`/mitmachen`}>
                  <Button className={'px-2 text-base mt-2'} type={ButtonType.Primary}>
                    {t('account.subscriptionNeedsRenewalDeactivatedLink')}
                  </Button>
                </Link>
              </>
            )}

            {hasUnpaidInvoice && (
              <>
                <p className="uppercase text-error">{t('account.subscriptionNeedsRenewalTitle')}</p>
                <p className="font-semibold">{t('account.subscriptionNeedsRenewalPaidUntil')}</p>
              </>
            )}
          </div>
        )}
      </Section>

      <div className="bg-gray-400 my-4 mr-2 h-0.5 lg:w-full lg:mt-5" />

      <Section
        disabled={!!mutableSubscription.deactivation}
        onSave={handleSave}
        isUpdating={isUpdating}
        toggleUpdating={setIsUpdating}
        title={t('account.subscriptionSettings')}>
        {isUpdating && (
          <div className="mb-10 w-full">
            <p className="uppercase">{t('account.subscriptionChangeWillApplyTitle')}</p>
            <p className="font-semibold">{t('account.subscriptionChangeWillApply')}</p>
          </div>
        )}
        <div className="mb-10 w-full lg:w-1/2 lg:pr-2">
          <p className="uppercase">
            {t('account.monthlyAmount', {
              minAmount: mutableSubscription.memberPlan.amountPerMonthMin / 100
            })}
          </p>

          {isUpdating ? (
            <div className="pt-2 pl-2 inline-flex space-x-4">
              <input
                type="range"
                min={mutableSubscription.memberPlan.amountPerMonthMin / 100}
                max="50"
                value={monthlyAmount}
                onChange={e => {
                  setMonthlyAmount(parseInt(e.target.value))
                }}
              />
              <p>{t('account.monthlyAmountValue', {amount: monthlyAmount})}</p>
            </div>
          ) : (
            <p className="font-semibold">
              {t('account.monthlyAmountValue', {amount: monthlyAmount})}
            </p>
          )}
        </div>

        <div className="mb-10 w-full lg:w-1/2">
          <p className="uppercase">{t('account.paymentInterval')}</p>
          {isUpdating ? (
            <select
              className="w-full border-solid border-2 p-2"
              name="paymentPeriodicity"
              value={mutableSubscription.paymentPeriodicity}
              onChange={e => {
                const paymentPeriodicity = memberPlan.availablePaymentMethods[0].paymentPeriodicities.find(
                  apm => apm === e.target.value
                )
                const autoRenew =
                  paymentPeriodicity !== PaymentPeriodicity.Yearly
                    ? true
                    : mutableSubscription.autoRenew
                setMutableSubscription({
                  ...mutableSubscription,
                  paymentPeriodicity,
                  autoRenew
                })
              }}>
              {memberPlan.availablePaymentMethods[0].paymentPeriodicities.map((pp, index) => {
                return (
                  <option value={pp} key={index}>
                    {t(`account.${pp}`)}
                  </option>
                )
              })}
            </select>
          ) : (
            <p className="font-semibold">
              {t(`account.${mutableSubscription.paymentPeriodicity}`)}
            </p>
          )}
        </div>

        <div className="mb-10 w-full lg:w-1/2 lg:pr-2">
          <p className="uppercase">{t('account.autoRenew')}</p>
          {isUpdating ? (
            <>
              <input
                checked={mutableSubscription.autoRenew}
                disabled={mutableSubscription.paymentPeriodicity !== PaymentPeriodicity.Yearly}
                type="checkbox"
                className="w-1/6 border-solid border-2 p-2"
                name="autoRenew"
                id="autoRenew"
                onChange={e => {
                  const obj = {...mutableSubscription, autoRenew: e.target.checked}
                  setMutableSubscription(obj)
                }}
              />
              <label htmlFor="autoRenew">{t('account.autoRenewTrue')}</label>
            </>
          ) : (
            <p className="font-semibold">
              {mutableSubscription.autoRenew && !mutableSubscription.deactivation
                ? t('account.autoRenewTrue')
                : nonAutoRenewDateHelper}
            </p>
          )}
        </div>

        <div className="mb-10 w-full lg:w-1/2">
          <p className="uppercase">{t('account.paymentMethod')}</p>
          <p className="font-semibold">
            {mutableSubscription.paymentMethod.description ||
              mutableSubscription.paymentMethod.name}
          </p>
        </div>

        {isUpdating && (
          <div className="mb-10 w-full">
            {!mutableSubscription.deactivation &&
              new Date(mutableSubscription.paidUntil) > new Date() && (
                <Button type={ButtonType.Danger} onClick={() => handleCancelSubscription()}>
                  {t('account.deactivateSubscription')}
                </Button>
              )}
          </div>
        )}
      </Section>

      <div className="bg-gray-400 my-4 mr-2 h-0.5 lg:w-full lg:mt-5" />

      <section className="my-10 flex flex-row justify-between flex-wrap">
        <h2 className="w-full md:w-1/4 mb-4">{t('account.billingHistory')}</h2>
        <InvoiceTable
          invoices={invoices?.filter(
            invoice => invoice.paidAt !== null || invoice.canceledAt !== null
          )}
          onInvoicePayClick={async invoiceID => {
            await createPaymentFromInvoiceAndExecuteAction(invoiceID)
          }}
        />
      </section>

      <div className="bg-gray-400 my-4 mr-2 h-0.5 lg:w-full lg:mt-5" />
    </>
  )
}
