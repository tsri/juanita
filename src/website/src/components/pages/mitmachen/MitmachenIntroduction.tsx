import React from 'react'
export default function MitmachenIntroduction() {
  return (
    <>
      {/* introduction text */}
      <div className="grid grid-cols-6 xl:grid-cols-12 mt-16">
        <div className="col-span-6 md:col-span-4 xl:col-span-6 md:col-start-2 xl:col-start-4">
          <p className="text-3xl lg:text-4xl text-tsri font-bold md:text-center">
            Werde jetzt Tsüri-Member
          </p>
          <p className="mt-4 md:text-center lg:text-xl">
            Wir sind dein Zürcher Stadtmagazin. Gemeinsam mit dir bewegen wir die Stadt. Wir
            entfachen Debatten, organisieren Events und berichten über das, was Zürich bewegt. Werde
            jetzt Teil der Tsüri-Community!
          </p>
        </div>
      </div>
    </>
  )
}
