import React from 'react'
import Image from 'next/image'
import mitmachen from '../../../../public/imgs/mitmachen/mitmachen_wir_retten_zueri_header.jpg'

export default function MitmachenRetteZueriIntroduction({memberCount}: {memberCount?: number}) {
  return (
    <>
      {/* image with overlay */}
      <div className="relative w-full h-52 md:h-80 xl:h-96 -mt-10">
        <Image alt="Wir Retten Züri" src={mitmachen} layout="fill" objectFit="contain" />
      </div>

      {/* introduction text */}
      <div className="grid grid-cols-6 xl:grid-cols-12 mt-16">
        <div className="col-span-6 md:col-span-4 xl:col-span-6 md:col-start-2 xl:col-start-4">
          <p className="text-3xl lg:text-4xl text-tsri font-bold md:text-center">
            Für eine Stadt, in der alle eine Zukunft haben.
          </p>

          <p className="mt-4 md:text-center lg:text-xl">
            Die Mieten steigen. Immer mehr von uns werden verdrängt. Wir befinden uns in einer
            Wohnungskrise.
          </p>

          <p className="mt-4 md:text-center lg:text-xl">
            Wir dürfen nicht wegschauen. Wir brauchen investigativen Journalismus, der hinschaut und
            Probleme benennt, der Ungerechtigkeit anprangert und über echte Lösungen spricht. Wir
            brauchen eine Berichterstattung, die die Geschichten der Betroffenen erzählt. Wir
            brauchen Journalismus, der die Aktivist:innen begleitet, die für Veränderung kämpfen.
          </p>

          <p className="mt-4 md:text-center lg:text-xl">
            Wir haben uns viel vorgenommen. Das schaffen wir nur, wenn wir von der ganzen Community
            unterstützt werden.
          </p>
        </div>
      </div>
    </>
  )
}
