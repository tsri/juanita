import {IconDefinition, faArrowUp} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import SocialMediaShareBtns from '../../atoms/SocialMediaShareBtns'

type MitmachenAppendixProps = {
  becomeMemberText: string
  sharePath: string
}

export default function MitmachenShareAppendix({
  becomeMemberText,
  sharePath
}: MitmachenAppendixProps) {
  const baseUrl: string = `${process.env.NEXT_PUBLIC_WEBSITE_URL}/${sharePath}`
  const shareIconSize: number = 40

  function scrollToBecomeMemberForm() {
    if (typeof document === 'undefined') {
      return
    }
    document.getElementById('mitmachen-form').scrollIntoView({behavior: 'smooth'})
  }

  return (
    <>
      <p className="text-3xl lg:text-4xl text-tsri font-bold pt-16 text-center hyphens-auto md:text-center">
        Teile diese Seite mit deinen Freund:innen
      </p>

      <div className="flex justify-center gap-4 pt-4">
        <SocialMediaShareBtns url={baseUrl} size={shareIconSize} />
      </div>

      <div
        className="flex justify-center gap-4 text-tsri cursor-pointer pb-16 pt-4"
        onClick={() => {
          scrollToBecomeMemberForm()
        }}>
        <FontAwesomeIcon icon={faArrowUp} className="h-8" />
        <p className="text-3xl lg:text-4xl font-bold text-center">{becomeMemberText}</p>
        <FontAwesomeIcon icon={faArrowUp} className="h-8" />
      </div>
    </>
  )
}
