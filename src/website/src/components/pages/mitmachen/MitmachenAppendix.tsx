import React from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCheck, IconDefinition} from '@fortawesome/free-solid-svg-icons'
import LinkPageBreakBlock from '../../blocks/linkPageBreakBlock'
import {LinkPageBreakBlockDataFragment} from '../../../@types/codegen/api'
import Grid2 from '@mui/material/Unstable_Grid2'

interface TsriChance {
  icon: IconDefinition
  title: string
  text?: string
}

interface MemberAdvantage {
  icon: IconDefinition
  title: string
}

type MitmachenAppendixProps = {
  becomeMemberText: string
  sharePath: string
}

export default function MitmachenAppendix({becomeMemberText, sharePath}: MitmachenAppendixProps) {
  const tsriChances: TsriChance[] = [
    {
      icon: faCheck,
      title: 'Kostenloser Zugang zu allen Artikeln auf Tsüri.ch'
    },
    {
      icon: faCheck,
      title: 'Morgendliches Züri-Briefing mit den wichtigsten News der Stadt'
    },
    {
      icon: faCheck,
      title: 'Einladungen zu zahlreichen Tsüri-Events'
    },
    {
      icon: faCheck,
      title: 'Mailing mit den günstigsten Wohnungen Zürichs'
    },
    {
      icon: faCheck,
      title: 'Zürcher Fashion aus dem Tsüri-Store'
    },
    {
      icon: faCheck,
      title: 'Zugang zur Tsüri-Community'
    }
  ]

  const linkPageBlockContent: LinkPageBreakBlockDataFragment = {
    __typename: 'LinkPageBreakBlock',
    text: 'Ohne deine Unterstützung geht es nicht',
    richText: [
      {
        type: 'paragraph',
        children: [
          {
            text:
              'Unsere Community ermöglicht den freien Zugang für alle. Dies unterscheidet uns von anderen Medien. Wir begreifen Journalismus nicht nur als Produkt, sondern auch als öffentliches Gut. Unsere Artikel sollen möglichst vielen Menschen zugutekommen. Mit unserer Berichterstattung versuchen wir das zu tun, was wir können: guten, engagierten Journalismus. Alle Schwerpunkte, Berichte und Hintergründe stellen wir dabei frei zur Verfügung, ohne Paywall. Gerade jetzt müssen Einordnungen und Informationen allen zugänglich sein. Was uns noch unterscheidet: unsere Leser:innen. Sie müssen nichts bezahlen, wissen aber, dass guter Journalismus nicht aus dem Nichts entsteht. Dafür sind wir sehr dankbar. Mittlerweile sind 1500 Menschen dabei und ermöglichen damit den Tsüri-Blick aufs Geschehen in unserer Stadt. Damit wir auch morgen noch unseren Journalismus machen können, brauchen wir mehr Unterstützung. Unser nächstes Ziel: 2000 – und mit deiner Beteiligung können wir es schaffen. Es wäre ein schönes Zeichen für Tsüri.ch und für die Zukunft unseres Journalismus. Mit nur 8 Franken bist du dabei!'
          }
        ]
      }
    ],
    linkURL: '#',
    linkText: 'Jetzt Member werden',
    hideButton: false,
    linkTarget: null,
    styleOption: 'default',
    layoutOption: 'default',
    templateOption: 'subscription',
    image: null
  }

  return (
    <>
      <div className="w-full pt-16">
        <p className="text-3xl lg:text-4xl text-tsri font-bold md:text-center">Deine Vorteile</p>

        {/* tsri chances */}
        <div className="grid grid-cols-12 gap-x-6 justify-items-center">
          {tsriChances.map(tsriChance => (
            <div className="col-span-10 md:col-span-4 pt-6" key={tsriChance.title}>
              <div className="grid grid-flow-col auto-cols-auto gap-8 pt-4">
                <div>
                  <FontAwesomeIcon icon={tsriChance.icon} className="text-tsri h-12 text-center" />
                </div>
                <div>
                  <p className="text-xl lg:text-2xl font-bold">{tsriChance.title}</p>
                  {tsriChance.text && <p className="hyphens-auto lg:text-xl">{tsriChance.text}</p>}
                </div>
              </div>
            </div>
          ))}
        </div>

        <div className="pt-16">
          <LinkPageBreakBlock linkPageBreak={linkPageBlockContent} />
        </div>

        {/* image collage */}
        <Grid2 container spacing={2} className="pt-16 pb-16">
          <Grid2 xs={6} lg={4}>
            <img
              src="/imgs/mitmachen/collage_1.webp"
              alt="collage_1"
              style={{objectFit: 'cover'}}
            />
          </Grid2>
          <Grid2 xs={6} lg={4}>
            <img
              src="/imgs/mitmachen/collage_2.webp"
              alt="collage_2"
              style={{objectFit: 'cover'}}
            />
          </Grid2>
          <Grid2 xs={6} lg={4}>
            <img
              src="/imgs/mitmachen/collage_3.webp"
              alt="collage_3"
              style={{objectFit: 'cover'}}
            />
          </Grid2>
          <Grid2 xs={6} lg={4}>
            <img
              src="/imgs/mitmachen/collage_4.webp"
              alt="collage_4"
              style={{objectFit: 'cover'}}
            />
          </Grid2>
          <Grid2 xs={6} lg={4}>
            <img
              src="/imgs/mitmachen/collage_5.webp"
              alt="collage_5"
              style={{objectFit: 'cover'}}
            />
          </Grid2>
          <Grid2 xs={6} lg={4}>
            <img
              src="/imgs/mitmachen/collage_6.webp"
              alt="collage_6"
              style={{objectFit: 'cover'}}
            />
          </Grid2>
        </Grid2>
      </div>
    </>
  )
}
