import React from 'react'
import {PaymentPeriodicity} from '../../../@types/codegen/api'
import {useTranslation} from 'next-i18next'

interface SelectPaymentPeriodicityParams {
  paymentPeriodicity: PaymentPeriodicity
  onPaymentPeriodicityChange: (paymentPeriodicity: PaymentPeriodicity) => void
}

export default function SelectPaymentInterval({paymentPeriodicity, onPaymentPeriodicityChange}) {
  const {t} = useTranslation()

  return (
    <>
      <p className="lg:text-xl">{t('account.paymentInterval')}</p>
      <select
        value={paymentPeriodicity}
        onChange={event => {
          onPaymentPeriodicityChange(event.target.value as PaymentPeriodicity)
        }}
        className="w-full border-solid border-2 p-2 bg-white">
        <option value={PaymentPeriodicity.Monthly}>
          {t(`account.${PaymentPeriodicity.Monthly}`)}
        </option>
        <option value={PaymentPeriodicity.Quarterly}>
          {t(`account.${PaymentPeriodicity.Quarterly}`)}
        </option>
        <option value={PaymentPeriodicity.Yearly}>
          {t(`account.${PaymentPeriodicity.Yearly}`)}
        </option>
      </select>
    </>
  )
}
