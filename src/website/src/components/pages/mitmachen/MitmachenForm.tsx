import React, {useContext, useMemo, useState} from 'react'
import {useForm} from 'react-hook-form'
import Button, {ButtonType} from '../../atoms/Button'
import {ChallengeInput, PaymentPeriodicity} from '../../../@types/codegen/api'
import {
  getMonthsinNumberFromPaymentPeriodicity,
  handleError,
  PaymentSlugs,
  TSRI_SELLING_MEMBER_PLAN_TAG,
  validateEmailRegex
} from '../../../helpers'
import ChallengeModal from './ChallengeModal'
import {
  MemberPlanRefFragment,
  useChallengeQuery,
  useCreateSubscriptionMutation,
  useMemberPlanListQuery,
  useRegisterMemberAndReceivePaymentMutation
} from '../../../@types/codegen/graphql'

import {InfoBox} from '../../atoms/InfoBox'
import Link from 'next/link'
import SelectPaymentInterval from './SelectPaymentInterval'
import {AuthContext} from '../../auth/AuthContextProvider'
import {useRouter} from 'next/router'

const noMemberPlanText = `Es ist kein gültiger Memberplan konfiguriert. Ein Memberplan sollte das Tag ${TSRI_SELLING_MEMBER_PLAN_TAG} enthalten.`

export function checkMemberPlan(
  memberPlan: MemberPlanRefFragment | undefined,
  hideError: boolean = false
): boolean {
  const memberPlanSlug = memberPlan?.slug
  if (memberPlanSlug) return true
  if (!hideError) {
    handleError({
      errorText: noMemberPlanText
    })
  }
  return false
}

interface MitmachenFormProps {
  title?: string
  overviewTitle?: string
  isLoading?(state: boolean): void
  memberPlanSlug: string
  paymentPeriodicityDefault?: PaymentPeriodicity
}

export default function MitmachenForm({
  title,
  overviewTitle,
  isLoading,
  memberPlanSlug,
  paymentPeriodicityDefault = PaymentPeriodicity.Yearly
}: MitmachenFormProps) {
  const paymentProvider = PaymentSlugs.PAYREXX_PAYMENT

  const [name, setName] = useState<string | undefined>(undefined)
  const [firstName, setFirstName] = useState<string | undefined>(undefined)
  const [email, setEmail] = useState<string | undefined>(undefined)
  const [monthlyAmount, setMonthlyAmount] = useState<number>(15)
  const [paymentPeriodicity, setPaymentPeriodicity] = useState<PaymentPeriodicity>(
    paymentPeriodicityDefault
  )
  const [showChallenge, setShowChallenge] = useState<boolean>(false)
  const baseUrl = process.env.NEXT_PUBLIC_WEBSITE_URL
  const successUrl = `${baseUrl}/account/subscriptions?paymentSuccess=true`
  const failureUrl = `${baseUrl}/account/subscriptions?paymentSuccess=false`
  const router = useRouter()
  const authContext = useContext(AuthContext)

  const [registerMemberAndReceivePaymentMutation] = useRegisterMemberAndReceivePaymentMutation({
    fetchPolicy: 'no-cache',
    onError: error => {
      error.graphQLErrors.forEach(async graphQLError => {
        if (graphQLError.extensions.code == 'EMAIL_ALREADY_IN_USE') {
          await router.push('/account/subscriptions?message=EMAIL_ALREADY_IN_USE')
        } else {
          const errorText = graphQLError.message
          const errorDisplay = undefined
          handleError({errorText, errorDisplay, autoClose: false})
        }
      })
    }
  })
  const [createSubscriptionMutation] = useCreateSubscriptionMutation({
    fetchPolicy: 'no-cache',
    onError: error => handleError({errorText: error.message})
  })
  const {
    handleSubmit,
    register,
    formState: {errors}
  } = useForm()

  // load member plans
  const {data: memberPlanList, loading: loadingMemberPlanList} = useMemberPlanListQuery({
    variables: {
      take: 10
    }
  })

  // load challenge
  const {data: challengeData, refetch: refetchChallenge} = useChallengeQuery()

  const monthCount = useMemo(() => {
    return getMonthsinNumberFromPaymentPeriodicity(paymentPeriodicity)
  }, [paymentPeriodicity])
  const totalAmount = useMemo(() => {
    return getMonthsinNumberFromPaymentPeriodicity(paymentPeriodicity) * monthlyAmount
  }, [paymentPeriodicity, monthlyAmount])

  const memberPlan: MemberPlanRefFragment | undefined = useMemo(() => {
    if (!memberPlanList?.memberPlans?.nodes) {
      return
    }

    return memberPlanList.memberPlans.nodes.find(memberPlan => memberPlan.slug === memberPlanSlug)
  }, [memberPlanList?.memberPlans?.nodes, memberPlanSlug])

  const hasSubscription: boolean = useMemo(() => {
    return !!authContext.subscriptions?.find(
      subscription =>
        subscription.memberPlan.id === memberPlan?.id && subscription.deactivation === null
    )
  }, [authContext.subscriptions, memberPlan?.id])

  /**
   * Handle loading logic for this component
   */
  const [subscriptionLoading, setSubscriptionLoading] = useState<boolean>(false)
  const [registerMemberLoading, setRegisterMemberLoading] = useState<boolean>(false)

  // loading memo
  const componentLoading = useMemo(() => {
    const loading =
      loadingMemberPlanList || subscriptionLoading || registerMemberLoading || authContext.loading
    isLoading(loading)
    return loading
  }, [
    loadingMemberPlanList,
    subscriptionLoading,
    registerMemberLoading,
    isLoading,
    authContext.loading
  ])

  /**
   * Force auto-renew to be set, if payment periodicity is one month only
   * @param paymentPeriodicity
   */
  function handlePaymentPeriodicity(paymentPeriodicity: PaymentPeriodicity) {
    setPaymentPeriodicity(paymentPeriodicity)
  }

  async function submitForm() {
    if (authContext.isAuthenticated) {
      await createSubscription()
    } else {
      // show challenge first
      setShowChallenge(true)
    }
  }

  async function createSubscription() {
    if (!checkMemberPlan(memberPlan)) {
      return
    }

    setSubscriptionLoading(true)
    try {
      const {data} = await createSubscriptionMutation({
        variables: {
          autoRenew: true,
          monthlyAmount: monthlyAmount * 100,
          memberPlanSlug: memberPlan.slug,
          paymentPeriodicity,
          paymentMethodSlug: paymentProvider,
          failureUrl,
          successUrl
        }
      })
      const returnedPaymentMethodSlug = data.createSubscription.paymentMethod.slug as PaymentSlugs
      const intentSecret = data.createSubscription.intentSecret

      redirectToPayment(returnedPaymentMethodSlug, intentSecret)
      setSubscriptionLoading(false)
    } catch (e) {
      setSubscriptionLoading(false)
      handleError({errorText: e.toString()})
    }
  }

  /**
   * Simply create a new subscription without register any user.
   */

  /**
   * New user is registered and subscription is created.
   * @param challengeSolution
   */
  async function registerMemberAndReceivePayment(challengeAnswer: ChallengeInput) {
    if (!checkMemberPlan(memberPlan)) {
      return
    }

    setRegisterMemberLoading(true)
    // close challenge modal
    await refetchChallenge()
    setShowChallenge(false)
    const memberPlanSlug = memberPlan.slug

    try {
      const {data} = await registerMemberAndReceivePaymentMutation({
        variables: {
          autoRenew: true,
          monthlyAmount: monthlyAmount * 100,
          name,
          firstName,
          email,
          paymentMethodSlug: paymentProvider,
          challengeAnswer,
          paymentPeriodicity,
          memberPlanSlug,
          successURL: successUrl,
          failureURL: failureUrl
        }
      })
      setRegisterMemberLoading(false)
      const registerResponse = data.registerMemberAndReceivePayment
      const sessionToken = registerResponse.session.token
      const intentSecret = registerResponse.payment.intentSecret
      const returnedPaymentMethodSlug = registerResponse.payment.paymentMethod.slug as PaymentSlugs

      if (!sessionToken) {
        handleError({errorText: 'Kein Session-Token von der API erhalten.'})
        return
      }
      // login-in
      await authContext.loginWithSessionToken(sessionToken)

      // payment
      redirectToPayment(returnedPaymentMethodSlug, intentSecret)
    } catch (e) {
      setRegisterMemberLoading(false)
      return
    }
  }

  function redirectToPayment(paymentSlug: PaymentSlugs, intentSecret: string) {
    // some error checks
    if (!intentSecret) {
      handleError({errorText: 'Kein Intentsecret von der API erhalten.'})
      return
    }
    if (paymentSlug !== paymentProvider) {
      handleError({
        errorText:
          'Unerwarteter Fehler: Payment-Methode von der API stimmt nicht mit der Payment-Methode Deines Clients überein.'
      })
      return
    }

    if (paymentSlug === PaymentSlugs.PAYREXX_PAYMENT) {
      // go to Payrexx payment
      window.location.href = intentSecret
    } else if (paymentSlug === PaymentSlugs.STRIPE_PAYMENT) {
      // go to stripe payment form and login through site reload
      window.location.href = `${baseUrl}/mitmachen/payment?intentSecret=${intentSecret}&amount=${totalAmount}`
    } else {
      handleError({errorText: `PaymentSlug wird nicht unterstützt: ${paymentSlug}`})
    }
  }

  // component is loading. show spinner.
  if (componentLoading) {
    return null
  }

  // user already has a subscription. avoid multiple subscriptions per user
  if (hasSubscription) {
    return (
      <div className="pt-8">
        <InfoBox>
          <div>
            Du hast bereits ein Abo.{' '}
            <Link href="/account/subscriptions">
              Klicke hier, um in dein Profil zu gelangen und das Abo zu verwalten.
            </Link>
          </div>
        </InfoBox>
      </div>
    )
  }

  // no member plan available. show error box.
  if (!checkMemberPlan(memberPlan, true)) {
    return (
      <div className="pt-8">
        <InfoBox>{noMemberPlanText}</InfoBox>
      </div>
    )
  }

  return (
    <>
      <div className="grid grid-cols-1 lg:grid-cols-6">
        <div
          id="mitmachen-form"
          className="mt-16 lg:col-start-2 lg:col-span-4 p-4"
          style={{backgroundColor: '#deeaf3'}}>
          <form onSubmit={handleSubmit(submitForm)}>
            <div
              className={`grid grid-cols-1 md:gap-4 lg:gap-6 ${
                authContext.isAuthenticated ? 'justify-items-center' : 'md:grid-cols-2'
              }`}>
              {!authContext.isAuthenticated && (
                <div>
                  {/* firstName */}
                  <div className="">
                    <p className="lg:text-xl">Vorname *</p>
                    <input
                      className="w-full border-solid border-2 p-2"
                      value={firstName || ''}
                      {...register('firstName', {required: true})}
                      type="text"
                      onChange={event => setFirstName(event.target.value)}
                    />
                    {errors.firstName && <span className="text-error">Bitte Vorname eingeben</span>}
                  </div>

                  {/* name */}
                  <div className="mt-4">
                    <p className="lg:text-xl">Nachname *</p>
                    <input
                      className="w-full border-solid border-2 p-2"
                      value={name || ''}
                      {...register('name', {required: true})}
                      type="text"
                      onChange={event => setName(event.target.value)}
                    />
                    {errors.name && <span className="text-error">Bitte Namen eingeben</span>}
                  </div>

                  {/* email */}
                  <div className="mt-4">
                    <p className="lg:text-xl">E-Mail *</p>
                    <input
                      className="w-full border-solid border-2 p-2"
                      value={email || ''}
                      type="text"
                      {...register('email', {
                        required: true,
                        pattern: {value: validateEmailRegex, message: 'Keine Gültige E-Mail'}
                      })}
                      onChange={event => setEmail(event.target.value)}
                    />
                    {errors.email && (
                      <span className="text-error">Bitte gültige E-Mail eingeben</span>
                    )}
                  </div>

                  {/* payment intervall */}
                  <div className="mt-4">
                    <SelectPaymentInterval
                      paymentPeriodicity={paymentPeriodicity}
                      onPaymentPeriodicityChange={paymentPeriodicity =>
                        handlePaymentPeriodicity(paymentPeriodicity)
                      }
                    />
                  </div>
                </div>
              )}

              <div
                className={`${
                  authContext.isAuthenticated ? 'md:w-2/3 lg:w-1/2' : ''
                } pt-4 md:pt-0`}>
                {/* slider */}
                <div className="">
                  <label className="lg:text-xl">
                    Ich unterstütze Tsüri.ch mit {monthlyAmount} CHF pro Monat.
                  </label>
                  <input
                    type="range"
                    min={memberPlan.amountPerMonthMin / 100}
                    max="50"
                    step="1"
                    value={monthlyAmount}
                    onChange={event => {
                      setMonthlyAmount(parseInt(event.target.value))
                    }}
                    className="appearance-none w-full h-2 bg-tsri mt-4"
                    id="monthlyAmountSlider"
                  />
                </div>

                {authContext.isAuthenticated && (
                  <>
                    <div className="mt-4 md:w-1/2">
                      <SelectPaymentInterval
                        paymentPeriodicity={paymentPeriodicity}
                        onPaymentPeriodicityChange={paymentPeriodicity =>
                          handlePaymentPeriodicity(paymentPeriodicity)
                        }
                      />
                    </div>
                  </>
                )}

                {/* abo overview */}
                <div className="mt-8 text-center">
                  <p className="lg:text-xl w-full border-b border-black border-solid">
                    {monthCount} {monthCount === 1 ? 'Monat' : 'Monate'} für {totalAmount} CHF
                  </p>
                  {overviewTitle && <p className="font-bold">{overviewTitle}</p>}
                </div>

                {/* select payment provider & submit */}
                <div className="mt-8 text-center w-full">
                  <div className="flex flex-wrap mt-6 justify-center">
                    {['twint', 'master', 'visa'].map(name => {
                      return (
                        <img
                          style={{minWidth: '45px'}}
                          key={name}
                          className="h-8 rounded border border-current mb-1 me-1"
                          src={`/cards/${name}.svg`}></img>
                      )
                    })}
                  </div>

                  <div className="flex justify-center mt-4">
                    <div className="w-full">
                      <Button type={ButtonType.Primary} buttonType="submit" className="w-full">
                        Ja, ich werde Member
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>

          {/* challenge */}
          <ChallengeModal
            show={showChallenge}
            challenge={challengeData?.challenge}
            onCancel={async () => {
              setShowChallenge(false)
              await refetchChallenge()
            }}
            onSetChallengeAnswer={async challengeAnswer => {
              await registerMemberAndReceivePayment(challengeAnswer)
            }}
          />
        </div>
      </div>
    </>
  )
}
