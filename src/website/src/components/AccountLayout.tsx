import {AccountIcon, SubscriptionIcon} from './atoms/icon'
import App from './App'
import React from 'react'
import Link from 'next/link'
import {useRouter} from 'next/router'
import {useTranslation} from 'next-i18next'
import {FullUserFragment} from '../@types/codegen/graphql'

export interface AccountLayoutProps {
  navs: any
  user?: FullUserFragment
  showNav: boolean
  children: React.ReactNode
}

export default function AccountLayout({children, navs, user, showNav}: AccountLayoutProps) {
  const router = useRouter()
  const {t} = useTranslation('common')

  return (
    <App navs={navs || {}}>
      <div className="lg:container mx-4 lg:mx-auto mb-8">
        {user && (
          <h1 className="lg:text-5xl pt-4 mb-8 uppercase">
            {t('account.welcome', {userName: `${user.firstName} ${user.name}`})}
          </h1>
        )}
        {showNav && (
          <div>
            <div className="bg-gray-400 my-4 mr-2 h-0.5 lg:w-full lg:mt-5" />
            <div className="flex flex-row items-center">
              <Link href="/account/profile" legacyBehavior>
                <div className="flex flex-row cursor-pointer items-center mr-10 lg:mr-24">
                  <div className="w-10 h-10 mr-2">
                    <AccountIcon
                      bgColor={router.pathname === '/account/profile' ? '#0e9fed' : 'black'}
                    />
                  </div>
                  <h6>{t('account.profile')}</h6>
                </div>
              </Link>

              <Link href="/account/subscriptions" legacyBehavior>
                <div className="flex flex-row cursor-pointer mr-10 lg:mr-24 items-center">
                  <div className="w-10 h-10 mr-2">
                    <SubscriptionIcon
                      bgColor={router.pathname === '/account/subscriptions' ? '#0e9fed' : 'black'}
                    />
                  </div>
                  <h6>{t('account.subscription')}</h6>
                </div>
              </Link>
            </div>
            <div className="bg-gray-400 my-4 mr-2 h-0.5 lg:w-full lg:mt-5" />
          </div>
        )}
        {children}
      </div>
    </App>
  )
}
