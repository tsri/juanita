import React, {useEffect, useRef, useState} from 'react'
import {useTranslation} from 'next-i18next'
import DialogModal from './DialogModal'

export interface ModalProps {
  show: boolean
  children: React.ReactNode
  showTopClose: boolean
  allowBackgroundClose: boolean
  onClose(): void
}

export function ChooseRightModal({
  show,
  children,
  showTopClose,
  allowBackgroundClose,
  onClose
}: ModalProps) {
  // @ts-ignore
  // TODO: check compatibility of <dialog /> with stripe
  if (typeof HTMLDialogElement === 'function' && false) {
    return (
      <DialogModal
        show={show}
        showTopClose={showTopClose}
        allowBackgroundClose={allowBackgroundClose}
        onClose={onClose}>
        {children}
      </DialogModal>
    )
  } else {
    return (
      <Modal
        show={show}
        showTopClose={showTopClose}
        allowBackgroundClose={allowBackgroundClose}
        onClose={onClose}>
        {children}
      </Modal>
    )
  }
}

export default function Modal({
  show,
  children,
  showTopClose,
  allowBackgroundClose,
  onClose
}: ModalProps) {
  const {t} = useTranslation('common')

  const closeButtonRef = useRef(null)
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false)

  useEffect(() => {
    setIsModalVisible(show)
  }, [show])

  function handlingClose() {
    setIsModalVisible(false)
    onClose()
  }

  useEffect(() => {
    if (isModalVisible) {
      document.body.classList.add('modal-open')
    } else {
      document.body.classList.remove('modal-open')
    }

    return () => {
      document.body.classList.remove('modal-open')
    }
  }, [isModalVisible])

  return (
    <div
      className={`fixed grid place-items-center align-middle z-30 inset-0 overflow-y-auto ${
        isModalVisible ? 'block' : 'hidden'
      }`}>
      <div
        onClick={() => allowBackgroundClose && handlingClose()}
        className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"
      />
      <div className="flex self-center items-start justify-center text-center sm:block sm:p-0">
        {/* This element is to trick the browser into centering the modal contents. */}
        <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
          &#8203;
        </span>
        <div className="inline-block align-bottom bg-white text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg w-full">
          {showTopClose && (
            <div className="absolute right-4 mt-3">
              <button ref={closeButtonRef} onClick={() => handlingClose()}>
                {t('loginModal.close')}
              </button>
            </div>
          )}
          <div className="bg-white px-4 pt-5 pb-4 min-h-20">
            <div className="sm:flex sm:items-start">
              <div className="mt-3 text-center flex-1">{children}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
