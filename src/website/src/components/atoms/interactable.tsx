import React, {ButtonHTMLAttributes} from 'react'

/**
 * @deprecated Use `BaseButton` instead.
 */
export function Interactable(props: ButtonHTMLAttributes<HTMLButtonElement>) {
  // const css = useStyle()
  return (
    <button
      // className={css(ButtonResetStyle)}
      {...props}
    />
  )
}
