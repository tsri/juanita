import React, {MouseEventHandler, ReactNode} from 'react'

export enum ButtonType {
  Primary,
  Secondary,
  Danger,
  White
}

export interface ButtonProps {
  readonly type: ButtonType
  readonly buttonType?: 'submit' | 'reset' | 'button'
  readonly className?: string
  readonly href?: string
  readonly children?: ReactNode
  readonly disabled?: boolean
  readonly onClick?: MouseEventHandler<HTMLAnchorElement> & MouseEventHandler<HTMLButtonElement>
}

export default function Button({
  type,
  buttonType,
  className,
  href,
  children,
  disabled,
  onClick
}: ButtonProps) {
  const Element = href ? 'a' : 'button'
  let colorScheme = ''
  switch (type) {
    case ButtonType.Primary:
      colorScheme = 'text-white bg-tsri'
      break
    case ButtonType.Secondary:
      colorScheme = 'text-white bg-black'
      break
    case ButtonType.Danger:
      colorScheme = 'text-white bg-error'
      break
    case ButtonType.White:
      colorScheme = 'text-black bg-white'
      break
  }

  return (
    <Element
      type={buttonType ?? 'button'}
      disabled={disabled}
      onClick={onClick}
      className={`px-5 py-3 lg:px-10 lg:py-3 lg:text-lg lg:inline-block ${colorScheme} ${className}`}>
      {children}
    </Element>
  )
}
