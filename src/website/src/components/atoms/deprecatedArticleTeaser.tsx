import styled from 'styled-components'
import Link from 'next/link'
import {useTranslation} from 'next-i18next'
import {format} from 'date-fns'
import {useMemo} from 'react'
import {getIsSponsor, getNonAdAuthors} from '../ArticleComponent'

const Authors = styled.p`
  span {
    ::after {
      content: ', ';
    }
    :last-child::after {
      content: '';
    }
  }
`

export interface deprecatedArticleTeaserProps {
  date: Date
  title: string
  image: any
  href: string
  authors: any
  tags: string[]
  className?: string
}

function DeprecatedArticleTeaser({
  date,
  title,
  image,
  href,
  authors,
  tags,
  ...rest
}: deprecatedArticleTeaserProps) {
  const {t} = useTranslation('common')
  const displayDate = format(date, 'dd.MM.yyyy')
  const isPromo = useMemo(() => {
    return getIsSponsor(authors)
  }, [authors])
  const nonAdAuthors = useMemo(() => {
    return getNonAdAuthors(authors)
  }, [authors])

  return (
    <Link href={href} className={`${rest.className} block hover:text-tsri cursor-pointer`}>
      {image?.url && (
        <img
          className={'sm:min-h-t485 lg:min-h-t450'}
          style={{
            width: '100%',
            height: '100%',
            display: 'block',
            objectFit: 'cover'
          }}
          src={image?.extension === '.gif' ? image.url : image.betterTeaserUrl}
          alt={image?.description ?? 'Article Teaser Bild'}
        />
      )}
      <p className="my-2">
        {displayDate} {isPromo && <span className="float-right italic">Promotion</span>}
      </p>
      <h2>{title}</h2>
      <Authors>
        {t('atoms.articleTeaserAuthors')}
        {nonAdAuthors.length
          ? nonAdAuthors.map(({name}, i) => <span key={i}>{name}</span>)
          : 'Tsüri.ch'}
      </Authors>
    </Link>
  )
}

const StyledArticleTeaser = styled(DeprecatedArticleTeaser)`
  overflow: hidden;
  word-break: break-all;
`

export default StyledArticleTeaser
