import React, {useRef} from 'react'
import {useWindowSize} from '../utility'

export interface SpotifyEmbedProps {
  title?: string
  url?: string
  width?: number
  height?: number
  styleCustom?: string
}

export function SpotifyEmbed(data: SpotifyEmbedProps) {
  const refIframe = useRef<HTMLIFrameElement>(null)

  const size = useWindowSize()

  return (
    <div className="mb-6" style={{width: '100%'}}>
      <iframe
        ref={refIframe}
        style={{
          position: 'relative',
          top: 0,
          left: 0,
          border: 'none'
        }}
        frameBorder={0}
        scrolling="no"
        allowFullScreen
        title={data.title}
        src={data.url}
        width={'100%'}
        height={size.width < 650 ? 380 : 400}
      />
    </div>
  )
}
