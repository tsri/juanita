import React, {useMemo} from 'react'
import {ArticleTeaser, FullAuthorFragment, TeaserFragment} from '../../../@types/codegen/graphql'
import styled, {css} from 'styled-components'
import dateFormat from '../../../utils/dateFormat'
import Link from 'next/link'
import {GRID_ROW_GAP} from '../../../../styles/helpers'
import {getNonAdAuthors, getNonPromotionAuthors} from '../../ArticleComponent'

interface DynamicHeightProp {
  dynamicHeight?: boolean
}
const TeaserImg = styled.img`
  width: 100%;
  flex: 1 1 auto;
  max-height: ${(props: DynamicHeightProp) => (props.dynamicHeight ? '50vh' : '100%')};
  display: block;
  object-fit: cover;
  min-height: 0;
`

interface TeaserStyleProps {
  isMainTeaser: boolean
}

const TeaserContainer = styled.div`
  height: ${(props: DynamicHeightProp) => (props.dynamicHeight ? 'unset' : '100%')};
  cursor: pointer;
  display: flex;
  flex-flow: column;
  padding-bottom: ${GRID_ROW_GAP}px;
`

const TeaserDate = styled.div`
  flex: 0 1 auto;

  font-weight: 400;
  text-transform: uppercase;
  font-size: ${(props: TeaserStyleProps) => (props.isMainTeaser ? '18px' : '16px')};
  line-height: ${(props: TeaserStyleProps) => (props.isMainTeaser ? '22px' : '19px')};
  letter-spacing: 0.02em;

  ${props => css`
    @media (min-width: ${props.theme.screens.sm}) {
      font-size: ${(props: TeaserStyleProps) => (props.isMainTeaser ? '22px' : '16px')};
      line-height: ${(props: TeaserStyleProps) => (props.isMainTeaser ? '26px' : '19px')};
    }
  `}
`

const TeaserTitle = styled.div`
  flex: 0 1 auto;

  font-weight: 600;
  font-size: ${(props: TeaserStyleProps) => (props.isMainTeaser ? '35px' : '25px')};
  line-height: ${(props: TeaserStyleProps) => (props.isMainTeaser ? '107.1%' : '30px')};
  letter-spacing: ${(props: TeaserStyleProps) => (props.isMainTeaser ? '0.01em' : '0.02em')};

  ${props => css`
    @media (min-width: ${props.theme.screens.sm}) {
      font-size: ${(props: TeaserStyleProps) => (props.isMainTeaser ? '55px' : '28px')};
      line-height: ${(props: TeaserStyleProps) => (props.isMainTeaser ? '66px' : '34px')};
      letter-spacing: ${(props: TeaserStyleProps) => (props.isMainTeaser ? '0.02em' : '0.03em')};
    }
  `}
`

const TeaserLead = styled.div`
  flex: 0 1 auto;
  font-weight: 500;
  line-height: 135%;
  font-size: 15px;

  ${props => css`
    @media (min-width: ${props.theme.screens.sm}) {
      font-size: 22px;
    }
  `}
`

const TeaserAuthor = styled.div`
  flex: 0 1 auto;
  font-weight: 500;
  font-size: 15px;
  line-height: 135%;
  padding-top: 30px;

  ${props => css`
    @media (min-width: ${props.theme.screens.sm}) {
      font-size: 16px;
      line-height: 19px;
      padding-top: ${(props: TeaserStyleProps) => (props.isMainTeaser ? '60px' : '30px')};
    }
  `}
`

export interface TeaserViewProps {
  teaser?: TeaserFragment
  overrideTitle?: string
  dynamicHeight?: boolean
}
export default function TeaserView({teaser, overrideTitle, dynamicHeight}: TeaserViewProps) {
  const isMainTeaser = teaser?.style === 'LIGHT'
  const article = teaser?.__typename === 'ArticleTeaser' ? teaser?.article : undefined
  const image = teaser?.image || article?.image
  const title = overrideTitle || teaser?.title || article?.title
  const lead = teaser?.lead || article?.lead
  const href = `/a/${article?.id}/${article?.slug}`
  const authors: FullAuthorFragment[] | undefined = useMemo(() => {
    const authors = article?.authors
    if (!authors) {
      return
    }
    return getNonPromotionAuthors(authors)
  }, [article])

  if (!teaser) {
    return null
  }

  return (
    <Link href={href} legacyBehavior>
      <TeaserContainer dynamicHeight={dynamicHeight}>
        {image?.url && (
          <TeaserImg
            dynamicHeight={dynamicHeight}
            src={image.largeURL}
            alt={image?.description ?? 'Kein Article-Teaser-Bild gefunden.'}
          />
        )}
        {/* date */}
        {article?.publishedAt && (
          <TeaserDate
            isMainTeaser={isMainTeaser}
            className={`${isMainTeaser ? 'pt-5 sm:pt-6' : 'pt-4'}`}>
            {dateFormat(new Date(article.publishedAt), 'dd. MMMM yyyy')}
          </TeaserDate>
        )}
        {/* title */}
        {title && (
          <TeaserTitle isMainTeaser={isMainTeaser} className={isMainTeaser ? 'pt-3' : 'pt-2'}>
            {title}
          </TeaserTitle>
        )}
        {/* lead */}
        {isMainTeaser && lead && <TeaserLead className={'pt-3'}>{lead}</TeaserLead>}
        {/* author name */}
        {authors && !!authors.length && (
          <TeaserAuthor isMainTeaser={isMainTeaser}>
            {authors.map((author, authorIndex) => (
              <span key={`author-key-${authorIndex}`}>
                <span>
                  {author.name}
                  {authorIndex + 1 < authors.length && ' / '}
                </span>
              </span>
            ))}
          </TeaserAuthor>
        )}
      </TeaserContainer>
    </Link>
  )
}
