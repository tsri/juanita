import React, {useContext} from 'react'
import styled, {css} from 'styled-components'
import {TeaserViewProps} from '../teaserView'
import {MailChimpCampaign, MailChimpContext} from '../../../../pages'

const DailyBriefingContainer = styled.div`
  height: 100%;
  padding: 20px;
`

const DailyTitle = styled.div`
  font-weight: 400;
  font-size: 26px;
  line-height: 28px;
  text-transform: uppercase;

  ${props => css`
    @media (min-width: ${props.theme.screens.lg}) {
      font-size: 22px;
      line-height: 26px;
    }
  `}
`
interface DailyEntryProps {
  hideBorder: boolean
}
const DailyEntry = styled.div`
  border-bottom: ${(props: DailyEntryProps) => (props.hideBorder ? 'none' : '1px solid black')};
  padding-bottom: 20px;
  padding-top: 20px;
  font-weight: 600;
  font-size: 20px;
  line-height: 24px;
  letter-spacing: 0.02em;
`

export default function DailyBriefing({teaser}: TeaserViewProps) {
  const mcCampaigns = useContext<MailChimpCampaign[]>(MailChimpContext)

  return (
    <DailyBriefingContainer className={'bg-yellow'}>
      <DailyTitle>{teaser.title || 'Daily  Briefing'}</DailyTitle>
      <div className={'pt-5'}>
        {mcCampaigns &&
          mcCampaigns.map((mcCampaign, mcIndex) => (
            <DailyEntry key={mcCampaign.id} hideBorder={mcIndex === mcCampaigns.length - 1}>
              <a href={mcCampaign.long_archive_url} target="_blank" rel="noreferrer">
                {mcCampaign.settings.subject_line}
              </a>
            </DailyEntry>
          ))}
      </div>
    </DailyBriefingContainer>
  )
}
