import React, {useEffect, useMemo, useState} from 'react'
import {
  ArticleListQuery,
  ArticleRefFragment,
  useArticleListQuery
} from '../../../@types/codegen/graphql'
import styled from 'styled-components'
import RelatedArticle from './relatedArticle'
import {GRID_GAP} from '../../../../styles/helpers'
import {PublishedArticle} from '../../types'

const RelatedArticlesGrid = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin: 0 -${GRID_GAP / 2}px;
`
const RelatedArticleGridItem = styled.div`
  cursor: pointer;
  width: calc(100% - ${GRID_GAP}px);
  max-width: 250px;
  margin: ${GRID_GAP / 2}px ${GRID_GAP / 2}px;
  @media (min-width: ${props => props.theme.screens.lg}) {
    width: calc(100% / 3 - ${GRID_GAP}px);
    max-width: 350px;
  }
`
const RelatedArticlesTitle = styled.div`
  font-weight: 400;
  font-size: 16px;
  line-height: 19px;
  text-align: center;
  letter-spacing: 0.02em;
  text-transform: uppercase;

  @media (min-width: ${props => props.theme.screens.sm}) {
    font-size: 22px;
    line-height: 26px;
  }
`

interface RelatedArticlesProps {
  article: PublishedArticle
  amount: number
}
export default function RelatedArticles({article, amount}: RelatedArticlesProps) {
  const [fetchAttempts, setFetchAttempts] = useState<number>(0)
  const [articles, setArticles] = useState<ArticleRefFragment[]>([])

  // get new related articles
  useEffect(() => {
    if (articles.length) {
      setArticles([])
      refetch()
    }
  }, [article.id])

  const {refetch} = useArticleListQuery({
    variables: {
      filter: {
        tags: article?.tags || []
      },
      take: amount + 1
    },
    fetchPolicy: 'network-only',
    onCompleted: data => mergeArticles(data),
    onError: () => mergeArticles()
  })

  async function mergeArticles(newArticles?: ArticleListQuery) {
    setFetchAttempts(fetchAttempts + 1)
    const articleNodes = newArticles?.articles?.nodes
    let filteredArticles = []
    if (articleNodes) {
      filteredArticles = articleNodes.filter(articleNode => {
        return (
          articleNode.id !== article.id &&
          !articles.map(tmpArt => tmpArt.id).includes(articleNode.id)
        )
      })
      // do not add the original article nor already added articles in the list
      setArticles([...articles, ...filteredArticles])
    }
    // prevent endless re-fetching
    if (fetchAttempts >= 2) {
      return
    } else if (filteredArticles.length < amount) {
      // load more articles in case we did not reach the amount passed as prop
      const refetchedArticles = await refetch({
        take: amount * 2,
        filter: {
          tags: undefined
        }
      })
      await mergeArticles(refetchedArticles.data)
    }
  }

  // only display the requested amounts of articles passed as prop
  const articlesToShow = useMemo(() => articles.slice(0, amount), [articles])

  return (
    <>
      <RelatedArticlesTitle>Das könnte dich auch interessieren</RelatedArticlesTitle>

      <RelatedArticlesGrid className={'pt-8'}>
        {articlesToShow.map((relatedArticle, articleIndex) => (
          <RelatedArticleGridItem key={articleIndex}>
            <RelatedArticle article={relatedArticle} />
          </RelatedArticleGridItem>
        ))}
      </RelatedArticlesGrid>
    </>
  )
}
