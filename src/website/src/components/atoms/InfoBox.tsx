import React from 'react'

interface InfoBoxProps {
  children?: React.ReactNode
  text?: string
  className?: string
}

export function InfoBox({text, className, children}: InfoBoxProps) {
  return (
    <>
      <div
        className={
          className ? className : `border p-2 border-error text-error text-center font-bold bg-red`
        }>
        {text ?? children}
      </div>
    </>
  )
}
