import React, {useEffect, createContext, ReactNode, useContext} from 'react'
import {InstagramPostEmbedData} from '../types'
import {useScript} from '../utility'

// Define some globals set by Instagram SDK.
declare global {
  interface Window {
    instgrm: any
  }
}

export interface InstagramContextState {
  readonly isLoaded: boolean
  readonly isLoading: boolean

  load(): void
}

export const InstagramContext = createContext(null as InstagramContextState | null)

export interface InstagramProviderProps {
  children?: ReactNode
}

export function InstagramProvider({children}: InstagramProviderProps) {
  const contextValue = useScript('//www.instagram.com/embed.js', () =>
    typeof window !== 'undefined' ? !!window.instgrm : false
  )
  return <InstagramContext.Provider value={contextValue}>{children}</InstagramContext.Provider>
}

export function InstagramPostEmbed({postID}: InstagramPostEmbedData) {
  const context = useContext(InstagramContext)

  if (!context) {
    throw new Error(
      `Coulnd't find InstagramContext, did you include InstagramProvider in the component tree?`
    )
  }

  const {isLoaded, isLoading, load} = context

  useEffect(() => {
    if (isLoaded) {
      window.instgrm.Embeds.process()
    } else if (!isLoading) {
      load()
    }
  }, [isLoaded, isLoading])

  return (
    <div className={`w-full lg:w-3/4`}>
      <blockquote
        className="instagram-media"
        data-width="100%"
        data-instgrm-captioned
        data-instgrm-permalink={`https://www.instagram.com/p/${encodeURIComponent(postID)}/`}
        data-instgrm-version="12"
      />
    </div>
  )
}
