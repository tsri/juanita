import React, {ReactNode} from 'react'

export interface FullscreenOverlayWrapperProps {
  isFullscreen: boolean
  children?: ReactNode
}

export function FullscreenOverlayWrapper({children, isFullscreen}: FullscreenOverlayWrapperProps) {
  // const css = useStyle()

  return isFullscreen ? (
    <div
    // className={css(FullscreenOverlayWrapperStyle)}
    >
      {children}
    </div>
  ) : (
    <>{children}</>
  )
}
