import React from 'react'
import {
  EmailIcon,
  EmailShareButton,
  FacebookIcon,
  FacebookShareButton,
  TelegramIcon,
  TelegramShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton
} from 'react-share'

export default function SocialMediaShareBtns({url, size}: {url: string; size: number}) {
  return (
    <>
      <FacebookShareButton url={url}>
        <FacebookIcon round size={size} />
      </FacebookShareButton>
      <TwitterShareButton url={url}>
        <TwitterIcon round size={size} />
      </TwitterShareButton>
      <WhatsappShareButton url={url}>
        <WhatsappIcon round size={size} />
      </WhatsappShareButton>
      <TelegramShareButton url={url}>
        <TelegramIcon round size={size} />
      </TelegramShareButton>
      <EmailShareButton url={url}>
        <EmailIcon round size={size} />
      </EmailShareButton>
    </>
  )
}
