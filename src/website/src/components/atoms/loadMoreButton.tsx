import React from 'react'
import {BaseButton} from './baseButton'
import {PrimaryButton} from './primaryButton'

export interface LoadMoreButtonProps {
  readonly onLoadMore: () => void
}

export function LoadMoreButton({onLoadMore}: LoadMoreButtonProps) {
  return (
    <BaseButton onClick={onLoadMore}>
      <PrimaryButton text="Load more" />
    </BaseButton>
  )
}
