import React, {useEffect} from 'react'
import {useScript} from '../utility'

declare global {
  interface Window {
    _ASO: {
      loadAd(param1: string, param2: string): void
    }
  }
}

export interface BildwurfAdEmbedProps {
  zoneID: string
}

export function BildwurfAdEmbed({zoneID}: BildwurfAdEmbedProps) {
  const {load} = useScript(
    `https://media.aso1.net/js/code.min.js`,
    () => {
      return typeof window !== 'undefined' ? !!window._ASO : false
    },
    false
  )

  useEffect(() => {
    load()
    try {
      window._ASO.loadAd('bildwurf-injection-wrapper', zoneID)
    } catch (error) {
      console.warn('could not call _ASO.loadAd()')
    }
  }, [])

  return (
    <div className="w-full">
      <div id="bildwurf-injection-wrapper">
        <ins className="aso-zone" data-zone={zoneID} data-lazy="true"></ins>
      </div>
    </div>
  )
}
