import Link from 'next/link'
import {RoundImage} from './roundImage'
import SocialMediaButtons from './socialMediaButtons'
import {useTranslation} from 'next-i18next'
import {FullAuthorFragment} from '../../@types/codegen/graphql'

export interface AuthorDetailsProps {
  author: FullAuthorFragment
}

export function AuthorDetails({author}: AuthorDetailsProps) {
  const {links, name, slug, image, jobTitle} = author
  const {t} = useTranslation('common')
  return (
    <div className="w-full flex">
      <div className="mobile:w-1/4 md:w-40 lg:w-1/4">
        <RoundImage width={42} height={42} src={image?.squareURL} />
      </div>
      <div className="mobile:w-3/4 ml-4">
        <div className="w-full sm:mr-4">
          <b>
            {t('atoms.authorDetailText')}
            <Link href={`/redaktion/${slug}`} passHref className="text-tsri underline">
              {name}
            </Link>
          </b>
          <p className="uppercase break-all">{jobTitle}</p>
        </div>
        <div className="w-full">
          <SocialMediaButtons links={links} />
        </div>
      </div>
    </div>
  )
}

export default AuthorDetails
