import React, {ReactNode, Fragment} from 'react'
import {Node} from 'slate'
import styled from 'styled-components'
import {PopupButton} from '@typeform/embed-react'
import {Color} from '../../../styles/deprecated_colors'

export const BaseTable = styled.table`
  width: 100%;
  border-collapse: collapse;
  table-layout: fixed;
`

export const BaseTableCell = styled.td`
  border: 1px solid;
  padding: 8px;
  // remove extra whitespace after paragraph inside of table-cell
  > p {
    margin-block-end: 0;
  }
`

export interface RichTextProps {
  readonly value: Node[]
}

export function RichText(props: RichTextProps) {
  return <div>{renderNodes(props.value)}</div>
}

export function renderNodes(nodes: Node[]): ReactNode {
  if (!nodes) return ''
  return nodes.map((node: any, index) => {
    if (node.children) {
      switch (node.type) {
        case 'heading-one':
          return <h1 key={index}>{renderNodes(node.children)}</h1>

        case 'heading-two':
          return <h2 key={index}>{renderNodes(node.children)}</h2>

        case 'heading-three':
          return <h3 key={index}>{renderNodes(node.children)}</h3>

        case 'bulleted-list':
          return <ul key={index}>{renderNodes(node.children)}</ul>

        case 'unordered-list':
          if (node.children.length > 0) {
            // Check if unordered list should be a ticker
            const listItems = [...node.children]
            const last = listItems[listItems.length - 1]
            if (
              last.type === 'list-item' &&
              last.children.length === 1 &&
              last.children[0].text === '__ticker'
            ) {
              listItems.pop() // remove last element because it's just ticker indicator
              return (
                <div key={index} className="mb-6">
                  {listItems.map((listItem, index) => {
                    const children = [...listItem.children]
                    const hasSummary = children.length > 0 && children[0].bold === true
                    const summary = hasSummary ? children[0].text : 'Eintrag'
                    return (
                      <details
                        key={index}
                        className="bg-gray-100 open:bg-tsri duration-300"
                        open={index === 0}>
                        <summary className="bg-inherit px-5 py-3 text-lg cursor-pointer">
                          {summary}
                        </summary>
                        <div className="bg-white px-5 py-3 border border-gray-300">
                          {renderNodes(hasSummary ? children.splice(1) : children)}
                        </div>
                      </details>
                    )
                  })}
                </div>
              )
            }
          }
          return (
            <ol
              style={{listStyleType: 'square', marginLeft: '2rem', marginBottom: '1.5rem'}}
              key={index}>
              {renderNodes(node.children)}
            </ol>
          )

        case 'ordered-list':
          return (
            <ul
              style={{listStyleType: 'num', marginLeft: '2rem', marginBottom: '1.5rem'}}
              key={index}>
              {renderNodes(node.children)}
            </ul>
          )

        case 'list-item':
          return <li key={index}>{renderNodes(node.children)}</li>

        case 'table':
          return (
            <BaseTable key={index}>
              <tbody>{renderNodes(node.children)}</tbody>
            </BaseTable>
          )

        case 'table-row':
          return <tr key={index}>{renderNodes(node.children)}</tr>

        case 'table-cell':
          return (
            <BaseTableCell key={index} style={{borderColor: node.borderColor}}>
              {renderNodes(node.children)}
            </BaseTableCell>
          )

        case 'link':
          if (node.url.includes('typeform.com/to')) {
            const typeformURL = new URL(node.url)
            const formID = typeformURL.pathname.substr(4)
            const autoOpen = typeformURL.search.includes('auto')
            return (
              <a key={index}>
                <PopupButton open={autoOpen ? 'load' : undefined} id={formID}>
                  {node.title}
                </PopupButton>
              </a>
            )
          } else {
            return (
              <a
                key={index}
                target="_blank"
                rel="noreferrer"
                href={node.url}
                title={node.title}
                className="rich-text-link">
                {renderNodes(node.children)}
              </a>
            )
          }

        default:
          return <p key={index}>{renderNodes(node.children)}</p>
      }
    } else {
      const splitText: string[] = node.text.split('\n')

      let element = (
        <>
          {splitText.length > 1
            ? splitText.map((text, index) => (
                <Fragment key={index}>
                  {text}
                  {index !== splitText.length - 1 ? <br /> : null}
                </Fragment>
              ))
            : splitText}
        </>
      )

      if (node.bold) {
        element = <strong>{element}</strong>
      }

      if (node.italic) {
        element = <em>{element}</em>
      }

      if (node.underline) {
        element = <u>{element}</u>
      }

      if (node.strikethrough) {
        element = <del>{element}</del>
      }

      if (node.subscript) {
        element = <sub>{element}</sub>
      }

      if (node.superscript) {
        element = <sup>{element}</sup>
      }

      return <React.Fragment key={index}>{element}</React.Fragment>
    }
  })
}
