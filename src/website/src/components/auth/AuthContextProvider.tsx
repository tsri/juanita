import React, {createContext, useEffect, useMemo, useState} from 'react'
import {
  FullSubscriptionFragment,
  FullUserFragment,
  MeWithSubscriptionsQuery,
  MeWithSubscriptionsQueryVariables,
  MySubscriptionsQueryVariables,
  useCreateSessionMutation,
  useCreateSessionWithJwtMutation,
  useMeWithSubscriptionsQuery,
  useMySubscriptionsLazyQuery,
  User,
  useSendWebsiteLoginMutation
} from '../../@types/codegen/graphql'
import {handleError} from '../../helpers'
import {toast} from 'react-toastify'
import {useRouter} from 'next/router'
import {ApolloQueryResult, QueryLazyOptions} from '@apollo/client'

export const AuthContext = createContext<{
  loginWithCredentials: (email: string, password: string) => Promise<void>
  loginWithSessionToken: (token: string) => Promise<void>
  sendWebsiteLoginLink: (email: string) => Promise<void>
  logout: () => void
  fetchUserAndSubscriptions: (
    variables?: Partial<MeWithSubscriptionsQueryVariables>
  ) => Promise<ApolloQueryResult<MeWithSubscriptionsQuery>>
  fetchSubscriptions: (options?: QueryLazyOptions<MySubscriptionsQueryVariables>) => void
  isAuthenticated: boolean
  user: FullUserFragment
  setUser: (value: ((prevState: User) => User) | User) => void
  subscriptions: FullSubscriptionFragment[]
  subscriptionNeedsAction: boolean
  hasActiveSubscription: boolean
  loading: boolean
}>(undefined)

export function AuthContextProvider({children}) {
  const [user, setUser] = useState<User | undefined>(undefined)
  const [subscriptions, setSubscriptions] = useState<FullSubscriptionFragment[] | undefined>(
    undefined
  )
  const router = useRouter()

  /**
   * INITIAL LOADING USER DATA
   * Important note: The fetchPolicy has to be network-only. Otherwise, the app could load the users object after logout from apollo cache.
   */
  const {
    loading: loadingMeWithSubscriptions,
    refetch: fetchUserAndSubscriptions
  } = useMeWithSubscriptionsQuery({
    onCompleted: data => {
      const {me, subscriptions} = data
      setUser(me as User)
      setSubscriptions(subscriptions)
    },
    fetchPolicy: 'network-only'
  })
  /**
   * API CALLS
   */
  const [sendWebsiteLogin, {loading: loadingSendWebsiteLoading}] = useSendWebsiteLoginMutation({
    onCompleted: data => {
      toast(
        'Ein Login-Link wurde dir per Mail zugestellt. Dies kann einen Moment dauern. Bitte prüfe auch deinen Spam-Ordner.',
        {type: 'success'}
      )
    },
    onError: error => {
      handleError({
        errorText: error.message
      })
    }
  })
  const [createSession, {loading: loadingCreateSession}] = useCreateSessionMutation({
    onCompleted: async data => {
      const {user, token} = data.createSession
      await treatSessionData(user, token)
      await redirectAfterLogin()
    },
    onError: error => {
      handleError({
        errorText: error.message
      })
    }
  })
  const [
    createSessionWithJwt,
    {loading: loadingCreateSessionWithJwt}
  ] = useCreateSessionWithJwtMutation({
    onCompleted: async data => {
      const {user, token} = data.createSessionWithJWT
      await treatSessionData(user, token)
      await redirectAfterLogin()
    }
  })
  const [fetchSubscriptions, {loading: loadingMySubscriptions}] = useMySubscriptionsLazyQuery({
    onCompleted: data => {
      setSubscriptions(data.subscriptions)
    },
    onError: error => handleError({errorText: error.message}),
    fetchPolicy: 'network-only'
  })

  /**
   * MEMORY
   */
  const loading = useMemo(() => {
    return (
      loadingMeWithSubscriptions ||
      loadingSendWebsiteLoading ||
      loadingCreateSession ||
      loadingCreateSessionWithJwt ||
      loadingMySubscriptions
    )
  }, [
    loadingMeWithSubscriptions,
    loadingSendWebsiteLoading,
    loadingCreateSession,
    loadingCreateSessionWithJwt,
    loadingMySubscriptions
  ])

  const isAuthenticated: boolean = useMemo(() => {
    return !!user
  }, [user])
  const subscriptionNeedsAction: boolean = useMemo(() => {
    if (!subscriptions) {
      return false
    }
    return subscriptions.some(subscription => {
      const paidUntilDate = new Date(subscription.paidUntil)
      const expired = paidUntilDate < new Date()
      const cancelled = !!subscription.deactivation?.date
      return expired && !cancelled
    })
  }, [subscriptions])
  const hasActiveSubscription: boolean = useMemo(() => {
    return !!subscriptions?.find(subscription => new Date(subscription.paidUntil) > new Date())
  }, [subscriptions])

  /**
   * WATCHER
   */
  // looking for jwt key to automatically log-in
  useEffect(() => {
    let {jwt} = router.query
    if (typeof jwt === 'string') {
      // workaround & hotfix for wrong jwt key
      jwt = jwt.replace(/^"|"$/g, '')
      createSessionWithJwt({
        variables: {
          jwt
        }
      })
    }
  }, [router.query])

  /**
   * FUNCTIONS
   */
  async function treatSessionData(user: FullUserFragment, token: string) {
    setUser(user as User)
    localStorage.setItem('token', token)
    await fetchSubscriptions()
  }
  async function loginWithCredentials(email: string, password: string): Promise<void> {
    await createSession({
      variables: {
        email,
        password
      }
    })
  }

  async function redirectAfterLogin(): Promise<void> {
    const currentPathName = window.location.pathname
    const redirectRoute =
      currentPathName === '/' || currentPathName === '/login' ? '/account/profile' : currentPathName
    await router.push(redirectRoute)
  }

  async function loginWithSessionToken(token: string) {
    localStorage.setItem('token', token)
    await fetchUserAndSubscriptions()
    await redirectAfterLogin()
  }
  async function sendWebsiteLoginLink(email: string): Promise<void> {
    await sendWebsiteLogin({
      variables: {
        email
      }
    })
  }
  function logout(): void {
    // todo: implement revoke session with api mutation, as soon as working again: https://wepublish.atlassian.net/browse/WPC-1217
    localStorage.removeItem('token')
    setSubscriptions(undefined)
    setUser(undefined)
    toast('Erfolgreich ausgeloggt')
  }

  return (
    <AuthContext.Provider
      value={{
        loginWithCredentials,
        loginWithSessionToken,
        sendWebsiteLoginLink,
        logout,
        fetchUserAndSubscriptions,
        fetchSubscriptions,
        isAuthenticated,
        user,
        setUser,
        subscriptions,
        subscriptionNeedsAction,
        hasActiveSubscription,
        loading
      }}>
      {children}
    </AuthContext.Provider>
  )
}
