import React, {useContext, useState} from 'react'
import Grid2 from '@mui/material/Unstable_Grid2'
import {ToggleButton, ToggleButtonGroup, TextField, Button} from '@mui/material'
import {useForm, Controller} from 'react-hook-form'
import {validateEmailRegex} from '../../helpers'
import {AuthContext} from './AuthContextProvider'

type loginModeType = 'mail' | 'password'
export default function LoginForm() {
  const [loginMode, setLoginMode] = useState<loginModeType>('mail')
  const emailRules = {
    required: 'Bitte gib Deine E-Mail ein.',
    pattern: {value: validateEmailRegex, message: 'Das ist keine gültige E-Mail.'}
  }
  const {
    handleSubmit,
    control,
    formState: {errors}
  } = useForm()
  const authContext = useContext(AuthContext)

  async function submitForm(data) {
    if (loginMode === 'password') {
      await authContext.loginWithCredentials(data.email, data.password)
    } else if (loginMode === 'mail') {
      await authContext.sendWebsiteLoginLink(data.email)
    }
  }

  return (
    <>
      <form onSubmit={handleSubmit(submitForm)}>
        <Grid2 container spacing={2}>
          {/* toggle between  */}
          <Grid2 xs={12}>
            <ToggleButtonGroup
              exclusive
              size={'small'}
              color={'primary'}
              value={loginMode}
              onChange={(event, newLoginMode) => setLoginMode(newLoginMode)}>
              <ToggleButton value="mail">Mail-Login</ToggleButton>
              <ToggleButton value="password">Login mit Passwort</ToggleButton>
            </ToggleButtonGroup>
          </Grid2>

          {/* login by mail */}
          <Grid2 xs={12}>
            <Controller
              name="email"
              control={control}
              rules={emailRules}
              render={({field}) => (
                <TextField
                  key="key-email"
                  sx={{
                    width: '100%'
                  }}
                  label="E-Mail"
                  type="email"
                  {...field}
                  error={!!errors?.email}
                  helperText={`${errors?.email?.message || ''}`}
                  disabled={authContext.loading}
                />
              )}
            />
          </Grid2>

          {/* login by password */}
          {loginMode === 'password' && (
            <>
              <Grid2 xs={12}>
                <Controller
                  name="password"
                  control={control}
                  rules={{
                    required: 'Bitte gib dein Passwort ein.'
                  }}
                  render={({field}) => (
                    <TextField
                      label="Passwort"
                      sx={{
                        width: '100%'
                      }}
                      type="password"
                      {...field}
                      error={!!errors?.password}
                      helperText={`${errors?.password?.message || ''}`}
                      disabled={authContext.loading}
                    />
                  )}
                />
              </Grid2>
            </>
          )}

          <Grid2 xs={12} textAlign={'end'}>
            <Button
              variant={'outlined'}
              size={'large'}
              type={'submit'}
              disabled={authContext.loading}>
              {authContext.loading
                ? 'Einen Moment bitte...'
                : loginMode === 'mail'
                ? 'Sende Login-Link'
                : 'Login'}
            </Button>
          </Grid2>
        </Grid2>
      </form>
    </>
  )
}
