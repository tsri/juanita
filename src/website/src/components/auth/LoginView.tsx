import React from 'react'
import Grid2 from '@mui/material/Unstable_Grid2'
import {Typography} from '@mui/material'
import LoginForm from './LoginForm'
import App from '../App'
import {NavsProps} from '../../pages/_app'

/**
 * This component is a wrapper around the login form. Optimized and delivered with App vue
 */

export interface LoginViewProps {
  navs: Partial<NavsProps>
  title?: string
  subtitle?: string
}

export default function LoginView({navs, title, subtitle}: LoginViewProps) {
  return (
    <>
      <App navs={navs}>
        <Grid2 container>
          <Grid2
            smOffset={1}
            lgOffset={3}
            xs={12}
            sm={10}
            lg={6}
            sx={{
              pt: {
                xs: 1,
                sm: 6
              }
            }}>
            <Typography variant={'h4'} style={{textTransform: 'uppercase'}}>
              {title || 'Willkommen zurück'}
            </Typography>
            <Typography variant={'subtitle1'}>
              {subtitle || 'Melde dich in deinem Tsüri-Account an.'}
            </Typography>
          </Grid2>

          {/* login form */}
          <Grid2
            smOffset={1}
            lgOffset={3}
            xs={12}
            sm={10}
            lg={6}
            sx={{
              pt: 4
            }}>
            <LoginForm />
          </Grid2>
        </Grid2>
      </App>
    </>
  )
}
