import React, {ReactNode, useContext, useMemo} from 'react'
import {AuthContext} from './AuthContextProvider'
import LoginView, {LoginViewProps} from './LoginView'

/**
 * This is a wrapper for views, which need an authenticated user-session.
 */

interface AuthenticationRequiredProps {
  children: JSX.Element
  loginViewProps: LoginViewProps
}

export default function AuthenticationRequired({
  children,
  loginViewProps
}: AuthenticationRequiredProps) {
  const authContext = useContext(AuthContext)

  if (authContext.isAuthenticated) {
    return children
  }
  return <LoginView {...loginViewProps} />
}
