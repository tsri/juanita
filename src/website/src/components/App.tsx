import Header from 'components/layout/header/Header'
import Footer from 'components/layout/Footer'
import Head from 'next/head'
import {ToastContainer} from 'react-toastify'
import {TSRI_PAGE_TITLE} from '../helpers'
import {Ribbon} from './atoms/ribbon'
import FACEBOOK_PIXEL_1 from './facebook/pixel-1'
import React, {PropsWithChildren} from 'react'
import {NavsProps} from 'pages/_app'
import {Paywall} from './layout/Paywall'

type AppProps = {
  navs: Partial<NavsProps>
  hideLogo?: boolean
  showPopup?: boolean
}

export default function App({
  children,
  navs,
  showPopup,
  hideLogo = false
}: PropsWithChildren<AppProps>) {
  const {footerNavigation, ribbonNavigation, ...headerNavigations} = navs
  return (
    <>
      <Head>
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v1=new" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v1=new" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v1=new" />
        <link rel="manifest" href="/site.webmanifest?v1=new" />
        <link rel="mask-icon" href="/safari-pinned-tab.svg?v1=supernew" color="#5bbad5" />
        <link rel="shortcut icon" href="/favicon.ico?v1=new" />
        <meta name="msapplication-TileColor" content="#da532c" />
        <meta name="theme-color" content="#36ADDF" />
        <meta name="apple-mobile-web-app-status-bar" content="#90cdf4" />
        <FACEBOOK_PIXEL_1 />
        <title>{TSRI_PAGE_TITLE}</title>
      </Head>
      <div className="min-h-screen relative flex flex-col">
        <ToastContainer hideProgressBar={true} draggable={false} />
        {ribbonNavigation && <Ribbon navigation={ribbonNavigation} />}
        <Header hideLogo={hideLogo} headerNavigations={headerNavigations} />
        {showPopup && <Paywall />}
        <main className="flex-main pb-4 mx-4 md:mx-auto md:container md:pt-6">{children}</main>
        <Footer hideLogo={hideLogo} navigation={footerNavigation} />
      </div>
    </>
  )
}
