import {EventRefFragment, EventsListQuery, TagRefFragment} from '../../@types/codegen/api'
import {ssrEventsList} from '../../@types/codegen/page'
import App from '../../components/App'
import {LiteratureIcon, MovieIcon, MusicIcon} from '../../components/atoms/icon'
import {RichText} from '../../components/atoms/RichText'
import {EventSort, SortOrder} from '../../@types/codegen/graphql'

export async function getStaticProps() {
  const eventsResult = await ssrEventsList.getServerPage({
    variables: {
      order: SortOrder.Ascending,
      sort: EventSort.StartsAt,
      filter: {
        upcomingOnly: true
      }
    }
  })
  const events = eventsResult?.props?.data

  if (!events) {
    return {
      notFound: true,
      revalidate: 10
    }
  }

  return {
    props: {
      events
    },
    revalidate: 60
  }
}

interface EventCardProps {
  event: EventRefFragment
  isNewDay: boolean
}

export const EventCategoryIcon = (tag: TagRefFragment) => {
  switch (tag.tag) {
    case 'Film':
      return <MovieIcon />
    case 'Musik':
    case 'Disko':
      return <MusicIcon />
    case 'Literatur':
      return <LiteratureIcon />
    default:
      return <LiteratureIcon />
  }
}

function EventCard({event, isNewDay}: EventCardProps) {
  const {name, startsAt, endsAt, description, image, tags} = event

  const start = startsAt ? new Date(startsAt) : null
  const end = endsAt ? new Date(endsAt) : null

  const startTime = start?.toLocaleTimeString('de', {timeStyle: 'short'})
  const endTime = end?.toLocaleTimeString('de', {timeStyle: 'short'})

  const day = start?.toLocaleDateString('de', {day: '2-digit'})
  const month = start?.toLocaleDateString('de', {month: '2-digit'})

  const icons = (
    <div className="flex flex-wrap">
      {tags.map((tag, index) => {
        return (
          <div key={index} className="basis-1/2">
            <div className="flex">
              <div className="w-11">{EventCategoryIcon(tag)}</div>
              <div className="pr-3">{tag.tag}</div>
            </div>
          </div>
        )
      })}
    </div>
  )

  return (
    <div className="grid grid-flow-row grid-cols-4 lg:grid-flow-row lg:grid-cols-12 gap-x-3">
      <div className="col-span-4 lg:col-span-1 text-4xl font-bold flex flex-row lg:flex-col">
        {isNewDay && (
          <>
            <span className="lg:mr-0">{day}</span>
            <span className="visible lg:hidden">.</span>
            <span>{month}</span>
          </>
        )}
      </div>
      <div className="col-span-4 lg:col-span-2">
        <img
          src={image?.url}
          style={{
            width: '100%',
            minHeight: '250px',
            height: '100%',
            display: 'block',
            objectFit: 'cover'
          }}
          alt={'missing image'}
        />
      </div>
      <div className="col-span-4 lg:col-span-2 flex flex-col justify-between">
        <div>
          <p>
            {startTime && startTime !== '00:00' ? startTime : ''}{' '}
            {!startTime || !endTime ? '' : ' - '} {endTime ?? ''}
          </p>
          <h2 className="text-3xl">{name}</h2>
        </div>
        <div className="w-100">{icons}</div>
      </div>
      <div className="col-span-4 lg:col-span-5 lg:col-start-8">
        <RichText value={description} />
      </div>
    </div>
  )
}

export default function Agenda({events, navs}: {events: EventsListQuery; navs: any}) {
  return (
    <App navs={navs}>
      <div className="lg:container mx-4 lg:mx-auto">
        <h1 className="lg:text-5xl pt-10 mb-14 uppercase">Agenda</h1>
        <div>
          <div className="bg-gray-400	my-4 mr-2 h-0.5	lg:w-full lg:mt-5" />
          <div className="text-center uppercase text-3xl">
            {new Date().toLocaleDateString('de-CH', {
              month: 'long'
            })}
          </div>
        </div>
        {events.events.nodes.map((event, index, array) => {
          let isNewDay = true
          if (index > 0) {
            const previousDay = new Date(array[index - 1].startsAt).toLocaleDateString()
            const currentDay = new Date(event.startsAt).toLocaleDateString()
            isNewDay = previousDay !== currentDay
          }
          return (
            <div key={index}>
              {isNewDay && <div className="bg-gray-400	my-4 mr-2 h-0.5	lg:w-full lg:mt-5" />}
              <div className="my-10">
                <EventCard event={event} isNewDay={isNewDay} />
              </div>
            </div>
          )
        })}
      </div>
    </App>
  )
}
