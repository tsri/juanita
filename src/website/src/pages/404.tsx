import Image from 'next/image'
import Link from 'next/link'

export default function Custom404() {
  return (
    <>
      <div className="flex flex-col gap-y-12">
        <h1 className="text-center mt-10">Seite nicht gefunden!</h1>
        <div className="justify-center flex-grow sm:hidden">
          <Link href="/">
            <Image
              src="/404_mobile.png"
              alt="404 page image"
              width={700}
              height={933}
              layout="responsive"
            />
          </Link>
        </div>
      </div>
      <div
        className="mobile:hidden flex"
        style={{
          justifyContent: 'center',
          paddingTop: '6vh'
        }}>
        <Image
          src="/404.png"
          alt="me"
          width="1024"
          height="724"
          layout="intrinsic"
          useMap={'#workmap'}
        />

        <map name="workmap">
          <area
            alt="Mitmachen"
            title="Mitmachen"
            href={`${process.env.NEXT_PUBLIC_WEBSITE_URL}/mitmachen`}
            coords="86,426 152,393 413,503 420,666 148,517 "
            shape="polygon"
          />
          <area
            alt="Home"
            title="Home"
            href="/"
            coords="291,329 666,303 781,370 664,442 299,454 "
            shape="polygon"
          />
          <area
            alt="Agenda"
            title="Agenda"
            href="/agenda"
            coords="326,157 586,49 693,59 608,167 330,316 "
            shape="polygon"
          />
        </map>
      </div>
    </>
  )
}
