import '../../styles/globals.scss'
import {ApolloProvider} from '@apollo/client'
import {CookiesProvider} from 'react-cookie'
import {theme as deprecatedTheme} from '../../tailwind.config'
import {useApollo} from '../withApollo'
import {AppContext, AppProps} from 'next/app'
import App from 'next/app'
import {useRouter} from 'next/router'
import {useEffect} from 'react'
import {appWithTranslation} from 'next-i18next'

import 'react-toastify/dist/ReactToastify.css'
import {FullNavigationFragment} from '../@types/codegen/graphql'
import {ssrNavigationList} from '../@types/codegen/page'
import {TwitterProvider} from '../components/atoms/twitterEmbed'
import {InstagramProvider} from '../components/atoms/instagramEmbed'

// tailwind is deprecated
import {ThemeProvider as DeprecatedThemeProvider} from 'styled-components'
import {ThemeProvider, createTheme, responsiveFontSizes} from '@mui/material/styles'
import {AuthContextProvider} from '../components/auth/AuthContextProvider'

// to be extended
let theme = createTheme({
  palette: {
    primary: {
      main: '#3AA1F0'
    }
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 640,
      md: 768,
      lg: 1024,
      xl: 1280
    }
  }
})
theme = responsiveFontSizes(theme)

function MyApp({Component, pageProps: {session, ...pageProps}}: AppProps) {
  const router = useRouter()
  const apolloClient = useApollo(pageProps.initialApolloState)

  useEffect(() => {
    const handleRouteChange = (url: URL) => {
      // @ts-ignore
      if (window && window.gtag) {
        // @ts-ignore
        window.gtag('config', process.env.NEXT_PUBLIC_GA_TRACKING_ID, {
          page_path: url
        })
      }
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])

  return (
    <ApolloProvider client={apolloClient}>
      <AuthContextProvider>
        <DeprecatedThemeProvider theme={deprecatedTheme}>
          <ThemeProvider theme={theme}>
            <CookiesProvider>
              <TwitterProvider>
                <InstagramProvider>
                  <Component {...pageProps} />
                </InstagramProvider>
              </TwitterProvider>
            </CookiesProvider>
          </ThemeProvider>
        </DeprecatedThemeProvider>
      </AuthContextProvider>
    </ApolloProvider>
  )
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//

export interface NavsProps {
  aboutUsNavigation: FullNavigationFragment
  categoriesNavigation: FullNavigationFragment
  eventsNavigation: FullNavigationFragment
  fokusMonatNavigation: FullNavigationFragment
  footerNavigation: FullNavigationFragment
  ribbonNavigation: FullNavigationFragment
}

MyApp.getInitialProps = async (appContext: AppContext) => {
  // calls page's `getInitialProps` and fills `appProps.pageProps`

  const appProps = await App.getInitialProps(appContext)
  const navigationResponse = await ssrNavigationList.getServerPage({}, appContext.ctx.req)
  const {
    aboutUsNavigation,
    categoriesNavigation,
    eventsNavigation,
    fokusMonatNavigation,
    footerNavigation,
    ribbonNavigation
  } = navigationResponse.props.data
  const navs: NavsProps = {
    aboutUsNavigation,
    categoriesNavigation,
    eventsNavigation,
    fokusMonatNavigation,
    footerNavigation,
    ribbonNavigation
  }
  return Object.assign({}, appProps, {pageProps: {navs}})
}

export default appWithTranslation(MyApp)
