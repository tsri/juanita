import {
  FullInvoiceFragment,
  FullMemberPlanFragment,
  FullSubscriptionFragment,
  useInvoiceListQuery,
  useMemberPlanListQuery,
  useUpdatePaymentProviderCustomersMutation
} from '../../@types/codegen/graphql'
import React, {useEffect, useState, useContext, useMemo} from 'react'
import {useTranslation} from 'next-i18next'
import {toast} from 'react-toastify'
import StripeCardUpdate from '../../components/stripe/StripeCardUpdate'
import Button, {ButtonType} from '../../components/atoms/Button'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import AccountLayout from '../../components/AccountLayout'
import {useRouter} from 'next/router'
import StripeElement from '../../components/stripe/StripeElement'
import {Loader} from '../../components/atoms/loader'
import Link from 'next/link'
import {AuthContext} from '../../components/auth/AuthContextProvider'
import SubscriptionItem from '../../components/pages/subscription/SubscriptionItem'
import {NavsProps} from '../_app'
import {StripeCustomer} from '../api/getStripeCustomer'
import AuthenticationRequired from '../../components/auth/AuthenticationRequired'
import {GetServerSidePropsContext} from 'next/types'
import {Alert, AlertTitle, Button as MuiButton} from '@mui/material'
import {faEye, faEyeSlash} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

export async function getServerSideProps(context: GetServerSidePropsContext) {
  return {
    props: {
      ...(await serverSideTranslations(context.locale, ['common'])),
      message: context.query.message || null
    }
  }
}

export default function Subscriptions({
  navs,
  message
}: {
  navs: Partial<NavsProps>
  message: string
}) {
  const [isUpdating, setIsUpdating] = useState(false)
  const [isUpdatingCard, setIsUpdatingCard] = useState(false)
  const [subscriptions, setSubscriptions] = useState<FullSubscriptionFragment[] | undefined>(
    undefined
  )
  const [showCancelled, setShowCancelled] = useState<boolean>(false)
  const [memberPlans, setMemberPlans] = useState<FullMemberPlanFragment[] | undefined>(undefined)
  const [invoices, setInvoices] = useState<FullInvoiceFragment[] | undefined>(undefined)
  const [stripeCustomer, setStripeCustomer] = useState<StripeCustomer | undefined>(undefined)
  const {t} = useTranslation('common')
  const router = useRouter()
  const [updatePaymentProviderCustomers] = useUpdatePaymentProviderCustomersMutation()
  const authContext = useContext(AuthContext)

  useEffect(() => {
    if (message === 'EMAIL_ALREADY_IN_USE') {
      toast('Du bist bereits registriert.', {type: 'error', autoClose: 5000})
    }
  }, [message])

  /**
   * API REQUESTS
   */
  const {loading: loadingMemberPlanList, refetch: refetchMemberPlanList} = useMemberPlanListQuery({
    variables: {
      take: 100
    },
    onCompleted: data => setMemberPlans(data.memberPlans.nodes)
  })
  const {loading: loadingInvoiceList, refetch: refetchInvoices} = useInvoiceListQuery({
    onCompleted: data => setInvoices(data.invoices)
  })

  /**
   * MEMOS
   */
  const loading = useMemo(() => {
    return loadingMemberPlanList || loadingInvoiceList
  }, [loadingMemberPlanList, loadingInvoiceList])

  const currentSubscriptions = useMemo(() => {
    const now = new Date()
    return (
      subscriptions?.filter(subscription => {
        const deactivationTime = subscription?.deactivation?.date
          ? new Date(subscription.deactivation.date)
          : null
        if (!deactivationTime) return true
        return deactivationTime > now
      }) || []
    )
  }, [subscriptions])

  const deactivatedSubscriptions = useMemo(() => {
    return (
      subscriptions?.filter(subscription => {
        return !currentSubscriptions.find(current => current.id === subscription.id)
      }) || []
    )
  }, [subscriptions])

  /**
   * EFFECTS
   */
  useEffect(() => {
    // todo: remove if possible!!
    checkUpdatedCreditCard()
  }, [])

  // in case user was not logged in, initially load invoices and member plans
  useEffect(() => {
    if (!invoices?.length) {
      refetchInvoices()
    }
    if (!memberPlans?.length) {
      refetchMemberPlanList()
    }
  }, [subscriptions])

  // sort subscriptions
  useEffect(() => {
    setSubscriptions(
      authContext.subscriptions?.sort((a, b) => {
        // show subscriptions with open invoice first
        if (
          invoices?.find(
            invoice =>
              invoice.subscriptionID === a.id &&
              invoice.paidAt === null &&
              invoice.canceledAt === null
          )
        ) {
          return -1
        }
        // show active subscriptions first
        if (!a.deactivation && !!b.deactivation) {
          return -1
        }
        return 0
      })
    )
  }, [authContext.subscriptions, invoices])

  useEffect(() => {
    fetchStripeCustomer()
  }, [authContext.user])

  async function fetchStripeCustomer() {
    if (!authContext.user) {
      return
    }
    const customerID = authContext.user.paymentProviderCustomers.find(
      ppc => ppc.paymentProviderID === 'stripe'
    )?.customerID
    if (!customerID) {
      return
    }
    const res = await fetch(`/api/getStripeCustomer?customerID=${customerID}`)
    if (res.status === 200) {
      const data = await res.json()
      setStripeCustomer(data.stripeCustomer)
    }
  }

  // todo: check if can be done without page re-load! attention: message is crucial.
  async function checkUpdatedCreditCard() {
    const creditCardUpdated = router.query?.creditCard
    if (!creditCardUpdated) return
    toast(
      'Danke für die Aktualisierung deiner Kreditkarte. Falls du eine offene Rechnung hast, wird diese in ' +
        'den kommenden Tagen automatisch belastet. Du musst nichts weiter unternehmen.',
      {
        type: 'success',
        autoClose: false,
        position: 'top-left',
        style: {width: '100%', backgroundColor: 'black', color: 'white'}
      }
    )
    await router.replace({query: undefined})
  }

  const handleRemoveCreditCard = async () => {
    if (confirm(t('account.confirmCardDelete'))) {
      const updatedPaymentProviderCustomers = authContext.user.paymentProviderCustomers.filter(
        ppc => ppc.paymentProviderID !== 'stripe'
      )
      await updatePaymentProviderCustomers({
        variables: {
          input: updatedPaymentProviderCustomers
        }
      })
      router.reload()
    }
  }

  return (
    <>
      <AuthenticationRequired loginViewProps={{navs}}>
        <>
          <Loader isLoading={loading} />
          <AccountLayout navs={navs} user={authContext.user} showNav={true}>
            {/* no subscriptions available */}
            {!currentSubscriptions?.length && (
              <div className="grid grid-cols-1 md:grid-cols-3 items-center pt-8">
                <div className="text-3xl text-center md:text-left md:col-span-2">
                  Du bist noch kein:e Tsüri-Member:in. Werde es jetzt.
                </div>

                <div className="pt-4 md:pt-0 text-center md:text-right">
                  <Link href="/mitmachen" legacyBehavior>
                    <Button type={ButtonType.Primary}>Tsüri-Member werden</Button>
                  </Link>
                </div>
              </div>
            )}

            {/* iterate all subscriptions */}
            {!!memberPlans?.length &&
              currentSubscriptions?.map(subscription => (
                <SubscriptionItem
                  key={subscription.id}
                  subscription={subscription}
                  memberPlan={memberPlans?.find(
                    memberPlan => memberPlan.id === subscription.memberPlan.id
                  )}
                  invoices={invoices?.filter(invoice => invoice.subscriptionID === subscription.id)}
                  isUpdating={isUpdating}
                  setIsUpdating={setIsUpdating}
                  reloadInvoices={refetchInvoices}
                />
              ))}

            {/* update credit card information */}
            {!!currentSubscriptions?.length && (
              <section className="my-10 flex flex-row justify-between flex-wrap">
                <h2 className="w-full md:w-1/4">{t('account.creditCard')}</h2>

                <div className="w-full md:w-3/4 flex flex-wrap mt-2 md:p-2 mb-10">
                  {!isUpdatingCard ? (
                    <p className="uppercase">{t('account.creditCardNumber')}</p>
                  ) : null}
                  <div className="relative mb-10 w-full">
                    <div className="absolute w-full">
                      {isUpdatingCard ? (
                        <StripeElement>
                          <StripeCardUpdate
                            stripeCustomer={stripeCustomer}
                            paymentProviderCustomers={authContext.user.paymentProviderCustomers}
                            onClose={async reload => {
                              setIsUpdatingCard(false)
                              await router.replace({query: {creditCard: 'updated'}})
                              if (reload) router.reload()
                            }}
                          />
                        </StripeElement>
                      ) : (
                        <>
                          <p className="font-semibold">
                            {t(
                              stripeCustomer && stripeCustomer.card
                                ? 'account.activeCreditCard'
                                : 'account.noCreditCard',
                              {
                                cardType: stripeCustomer?.card?.brand,
                                last4: stripeCustomer?.card?.last4,
                                validTill: `${stripeCustomer?.card?.exp_month}/${stripeCustomer?.card?.exp_year}`
                              }
                            )}
                          </p>
                          <div className="mt-2">
                            <Button
                              onClick={() => setIsUpdatingCard(true)}
                              type={ButtonType.Secondary}>
                              {t(
                                stripeCustomer && stripeCustomer.card
                                  ? 'account.replaceCard'
                                  : 'account.addCard'
                              )}
                            </Button>
                            {stripeCustomer && stripeCustomer.card && (
                              <Button
                                className="ml-4"
                                type={ButtonType.Danger}
                                onClick={() => handleRemoveCreditCard()}>
                                {t('account.removeCard')}
                              </Button>
                            )}
                          </div>
                        </>
                      )}
                    </div>
                  </div>
                </div>
              </section>
            )}

            {/* subscription archive */}
            {!!deactivatedSubscriptions?.length && (
              <>
                <MuiButton
                  className={'mt-10'}
                  size="large"
                  variant="text"
                  endIcon={<FontAwesomeIcon icon={showCancelled ? faEyeSlash : faEye} />}
                  onClick={() => setShowCancelled(!showCancelled)}>
                  Inaktive Mitgliedschaften {showCancelled ? 'ausblenden' : 'anzeigen'}
                </MuiButton>

                {showCancelled && (
                  <>
                    <Alert severity="error" className={'mt-5'}>
                      <AlertTitle>Inaktive Mitgliedschaften</AlertTitle>
                      Die folgenden Mitgliedschaften sind deaktiviert und werden nicht mehr
                      belastet.
                    </Alert>
                    {deactivatedSubscriptions.map(deactivatedSubscription => (
                      <SubscriptionItem
                        key={deactivatedSubscription.id}
                        subscription={deactivatedSubscription}
                        memberPlan={memberPlans?.find(
                          memberPlan => memberPlan.id === deactivatedSubscription?.memberPlan?.id
                        )}
                        isUpdating={isUpdating}
                        setIsUpdating={setIsUpdating}
                        invoices={invoices?.filter(
                          invoice => invoice.subscriptionID === deactivatedSubscription.id
                        )}
                        reloadInvoices={refetchInvoices}
                      />
                    ))}
                  </>
                )}
              </>
            )}
          </AccountLayout>
        </>
      </AuthenticationRequired>
    </>
  )
}
