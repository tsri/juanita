import AccountLayout from '../../components/AccountLayout'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import {Trans, useTranslation} from 'next-i18next'

export async function getServerSideProps({locale}) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common']))
    }
  }
}

export default function Signin({navs}) {
  const {t} = useTranslation('common')
  return (
    <AccountLayout navs={navs} showNav={true}>
      <h1>{t('account.errorPageTitle')}</h1>
      <p className="mt-2">
        <Trans i18nKey="account.errorPageMessage">
          {'fake text'}
          <a className="underline" href="mailto:seraina.manser@tsri.ch">
            Name
          </a>
          {'more  fake text'}
        </Trans>
      </p>
      <div className="mt-10 w-full">
        <img
          className="block m-auto"
          src="https://media3.giphy.com/media/f9k1tV7HyORcngKF8v/giphy.gif"
        />
      </div>
    </AccountLayout>
  )
}
