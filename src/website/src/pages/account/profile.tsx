import * as Sentry from '@sentry/nextjs'
import React, {useContext, useEffect, useState} from 'react'
import {FullUserFragment, useUpdateUserMutation} from '../../@types/codegen/graphql'
import {Trans, useTranslation} from 'next-i18next'
import {Loader} from '../../components/atoms/loader'
import Section from '../../components/Section'
import Info from '../../components/Info'
import AccountLayout from '../../components/AccountLayout'
import {useRouter} from 'next/router'
import {handleError} from '../../helpers'
import {NavsProps} from '../_app'
import AuthenticationRequired from '../../components/auth/AuthenticationRequired'
import {AuthContext} from '../../components/auth/AuthContextProvider'
import {GetServerSideProps} from 'next'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import {User} from '../../@types/codegen/api'

export interface ProfileProps {
  navs: Partial<NavsProps>
}

export const getServerSideProps: GetServerSideProps = async ({params, query, locale}) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common']))
    }
  }
}

export default function Profile({navs}: ProfileProps) {
  /**
   * VARIABLES
   */
  const router = useRouter()
  const {t} = useTranslation('common')
  const [user, setUser] = useState<FullUserFragment | undefined>(undefined)
  const [isUpdating, setIsUpdating] = useState(false)
  const authContext = useContext(AuthContext)

  useEffect(() => {
    const contextUser = authContext.user
    if (!contextUser) {
      return
    }
    setUser(JSON.parse(JSON.stringify(contextUser)))
  }, [authContext.user])

  /**
   * API METHODS
   */
  const [updateUserMutation, {loading: isLoadingUpdateUser, error}] = useUpdateUserMutation()

  /**
   * FUNCTIONS
   */
  async function handleSave() {
    if (!user) {
      handleError({
        errorText: 'Unerwarteter Fehler. Kein User-Objekt vorhanden.'
      })
      return false
    }
    try {
      const response = await updateUserMutation({
        variables: {
          input: {
            email: user.email,
            name: user.name,
            firstName: user.firstName,
            address: {
              streetAddress: user.address?.streetAddress || '',
              streetAddress2: user.address?.streetAddress2 || '',
              zipCode: user.address?.zipCode || '',
              city: user.address?.city || '',
              country: user.address?.country || ''
            }
          }
        }
      })
      const {updateUser} = response.data
      authContext.setUser(updateUser as User)
      setIsUpdating(false)
      return true
    } catch (error) {
      handleError({
        errorText: 'Änderungen konnten nicht gespeichert werden.'
      })
      Sentry.captureException(error)
      return false
    }
  }

  function toggleUpdating(val): void {
    setIsUpdating(val)
    // refresh the original user from auth context
    if (!val) {
      setUser(authContext.user)
    }
  }

  return (
    <>
      <AuthenticationRequired
        loginViewProps={{
          navs: navs,
          title: 'Willkommen in deinem Profil',
          subtitle: 'Du musst dich einloggen.'
        }}>
        <AccountLayout navs={navs} user={user} showNav={true}>
          {authContext.subscriptionNeedsAction && (
            <div
              onClick={() => {
                router.push('/account/subscriptions')
              }}
              className="p-4 mt-4 w-full text-sm bg-red rounded-lg cursor-pointer"
              role="alert">
              <span className="font-medium">{t('loginModal.attention')}</span>{' '}
              {t('loginModal.subscriptionNeedsYourAttention')}
            </div>
          )}
          <Loader isLoading={isLoadingUpdateUser} />
          <Section
            title={t('account.address')}
            onSave={handleSave}
            isUpdating={isUpdating}
            toggleUpdating={toggleUpdating}>
            <Info
              isUpdating={isUpdating}
              label="Vorname"
              name="firstName"
              value={user?.firstName}
              onChange={e => setUser({...user, firstName: e.target.value})}
            />
            <Info
              isUpdating={isUpdating}
              label={t('account.name')}
              name="Nachname"
              value={user?.name}
              onChange={e => setUser({...user, name: e.target.value})}
            />
            <Info
              isUpdating={isUpdating}
              label={t('account.streetAddress')}
              name="streetAddress"
              value={user?.address?.streetAddress}
              onChange={e =>
                setUser({
                  ...user,
                  address: {
                    ...user.address,
                    streetAddress: e.target.value
                  }
                })
              }
            />
            <Info
              isUpdating={isUpdating}
              label={t('account.zip')}
              name="zipCode"
              value={user?.address?.zipCode}
              onChange={e =>
                setUser({
                  ...user,
                  address: {
                    ...user.address,
                    zipCode: e.target.value
                  }
                })
              }
            />
            <Info
              isUpdating={isUpdating}
              label={t('account.city')}
              name="city"
              value={user?.address?.city}
              onChange={e =>
                setUser({
                  ...user,
                  address: {
                    ...user.address,
                    city: e.target.value
                  }
                })
              }
            />
            <Info
              isUpdating={isUpdating}
              label={t('account.country')}
              name="country"
              value={user?.address?.country}
              onChange={e =>
                setUser({
                  ...user,
                  address: {
                    ...user.address,
                    country: e.target.value
                  }
                })
              }
            />
          </Section>
          <div className="bg-gray-400 my-4 mr-2 h-0.5 lg:w-full lg:mt-5" />
          {/* TODO: readonly as API won't accept changing it  */}
          <Section title={t('account.email')}>
            <Info
              type="email"
              isUpdating={false}
              name="email"
              label={t('account.email')}
              value={user?.email}
              onChange={e => setUser({...user, email: e.target.value})}
            />
            <div className="mb-10 w-full">
              <Trans i18nKey="account.emailChangeInfoText">
                {'fake text'}
                <a
                  className="underline"
                  href=" mailto:info@tsri.ch?subject=E-Mail-Adresse%20%C3%A4ndern">
                  Name
                </a>
                {'more  fake text'}
              </Trans>
            </div>
          </Section>
        </AccountLayout>
      </AuthenticationRequired>
    </>
  )
}
