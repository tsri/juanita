import React from 'react'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import {useState} from 'react'
import App from '../../components/App'
import {Loader} from '../../components/atoms/loader'
import MitmachenAppendix from '../../components/pages/mitmachen/MitmachenAppendix'
import MitmachenForm from '../../components/pages/mitmachen/MitmachenForm'
import MitmachenIntroduction from '../../components/pages/mitmachen/MitmachenIntroduction'
import {NavsProps} from '../_app'

export async function getStaticProps({locale}) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common']))
    }
  }
}

export default function Mitmachen({navs}: {navs: Partial<NavsProps>}) {
  const [isLoading, setIsLoading] = useState<boolean>(false)

  return (
    <App navs={navs}>
      <Loader isLoading={isLoading} />
      {/* introduction */}
      <MitmachenIntroduction />
      {/* form */}
      <MitmachenForm isLoading={setIsLoading} memberPlanSlug={'tsri-member-ab-8-chf'} />
      <MitmachenAppendix becomeMemberText="Jetzt Tsüri-Member werden" sharePath="mitmachen" />
    </App>
  )
}
