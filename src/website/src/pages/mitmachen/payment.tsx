import React from 'react'
import StripeElement from '../../components/stripe/StripeElement'
import StripePayment from '../../components/stripe/StripePayment'
import {useRouter} from 'next/router'
import App from '../../components/App'
import {toast} from 'react-toastify'
import * as Sentry from '@sentry/nextjs'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'

export async function getServerSideProps({locale}) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common']))
    }
  }
}

export default function Payment() {
  const router = useRouter()
  const {intentSecret, amount} = router.query

  function errorView() {
    const errorText = 'Unerwarteter Fehler: Ein Parameter (intentSecret oder amount) fehlt.'
    toast(errorText, {type: 'error'})
    Sentry.captureMessage(errorText)
    return (
      <App navs={{}}>
        <div />
      </App>
    )
  }

  function paymentView(intentSecret: string, amount: string) {
    return (
      <App navs={{}}>
        <div className="grid grid-cols-1 justify-items-center">
          <div className="md:w-4/6">
            <StripeElement clientSecret={intentSecret}>
              <StripePayment
                amount={parseInt(amount)}
                onClose={async success => {
                  if (success) {
                    await router.push('/account/subscriptions?paymentSuccess=true')
                  } else {
                    await router.push('/account/subscriptions?paymentSuccess=false')
                  }
                }}
              />
            </StripeElement>
          </div>
        </div>
      </App>
    )
  }

  if (typeof intentSecret !== 'string' || typeof amount !== 'string') {
    return errorView()
  }
  return paymentView(intentSecret, amount)
}
