import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import {useState} from 'react'
import {PaymentPeriodicity} from '../../@types/codegen/graphql'
import App from '../../components/App'
import {Loader} from '../../components/atoms/loader'
import MitmachenShareAppendix from '../../components/pages/mitmachen/MitmachenShareAppendix'
import MitmachenForm from '../../components/pages/mitmachen/MitmachenForm'
import MitmachenRetteZueriIntroduction from '../../components/pages/mitmachen/MitmachenRetteZueriIntroduction'

export async function getStaticProps({locale}) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common']))
    },
    revalidate: 10
  }
}

export default function MitmachenRettenZueri({navs}: {navs: any}) {
  const [isLoading, setIsLoading] = useState<boolean>(false)

  return (
    <App navs={navs} hideLogo>
      <style>
        {`
          .border-tsri {
            border-color: #fd6101;
          }

          .text-tsri {
            color: #fd6101;
          }

          .bg-blue,
          .bg-tsri {
              background-color: #fd6101;
          }

          .accent-tsri {
            accent-color: #fd6101;
          }
        `}
      </style>

      <Loader isLoading={isLoading} />
      {/* introduction */}
      <MitmachenRetteZueriIntroduction />

      {/* form */}
      <MitmachenForm
        title={'Werde hier Mitglied in der «Wir retten Züri»-Community!'}
        overviewTitle={'Ja, ich will Mitglied werden'}
        isLoading={setIsLoading}
        memberPlanSlug={'wir-retten-zueri-member'}
        paymentPeriodicityDefault={PaymentPeriodicity.Monthly}
      />

      <div className="w-full pt-16">
        <MitmachenShareAppendix
          becomeMemberText="Jetzt Rette-Züri Member werden"
          sharePath="mitmachen-rettet-zueri"
        />
      </div>
    </App>
  )
}
