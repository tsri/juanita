import '/node_modules/react-grid-layout/css/styles.css'
import '/node_modules/react-resizable/css/styles.css'

import {ssr} from '../@types/codegen/page'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import {GetServerSideProps} from 'next'
import Head from 'next/head'
import {TSRI_PAGE_TITLE} from '../helpers'
import React, {createContext, useMemo} from 'react'
import App from '../components/App'
import {BlockRenderer} from '../components/blocks/blockRenderer'
import {Page} from '../@types/codegen/api'
import {getBlocks} from '../components/route/blockAdapters'
const mailchimp = require('@mailchimp/mailchimp_marketing')

export interface MailchimpConfig {
  apiKey: string
  server: string
}
export interface MailChimpCampaignResponse {
  campaigns: MailChimpCampaign[]
}
export interface MailChimpCampaign {
  id: string
  long_archive_url: string
  settings: MailChimpCampaignSettings
}

export interface MailChimpCampaignSettings {
  subject_line: string
}

export const MailChimpContext = createContext<MailChimpCampaign[]>([])

export interface HomeIndexProps {
  page: Page | null
  navs: any
  mcCampaigns: MailChimpCampaign[]
}

async function fetchMailChimpCampaigns(
  apiKey: string,
  server: string
): Promise<MailChimpCampaign[]> {
  if (!apiKey) {
    console.warn('No Mailchimp API key provided!')
    return []
  }
  if (!server) {
    console.warn('No Mailchimp server prefix provided!')
    return []
  }
  try {
    mailchimp.setConfig({
      apiKey,
      server
    } as MailchimpConfig)
    const {campaigns} = (await mailchimp.campaigns.list({
      count: 4,
      sort_field: 'send_time',
      status: 'sent',
      sort_dir: 'DESC',
      folder_id: '90c02813e1',
      fields: ['campaigns.id', 'campaigns.long_archive_url', 'campaigns.settings.subject_line']
    })) as MailChimpCampaignResponse
    return campaigns
  } catch (e) {
    console.warn(e)
    return []
  }
}

export const getServerSideProps: GetServerSideProps = async ({query, locale}) => {
  // fetch daily briefing
  const apiKey = process.env.MAILCHIMP_API_KEY
  const server = process.env.MAILCHIMP_SERVER_PREFIX
  let mcCampaigns = await fetchMailChimpCampaigns(apiKey, server)

  const pageResponse = await ssr.getServerPage({
    variables: {
      slug: 'front'
    },
    fetchPolicy: 'network-only',
    errorPolicy: 'ignore'
  })

  const {data: pageData} = pageResponse.props

  return {
    props: {
      page: pageData?.page,
      query,
      ...(await serverSideTranslations(locale, ['common'])),
      mcCampaigns
    }
  }
}

export default function Index({page, navs, mcCampaigns}: HomeIndexProps) {
  const blocks = useMemo(() => {
    if (!page) {
      return []
    }
    return getBlocks(page.blocks)
  }, [page])

  return (
    <App navs={navs || {}}>
      <Head>
        <title>{TSRI_PAGE_TITLE}</title>
        <meta
          name="description"
          content="Dein Stadtmagazin. Spannende Geschichten aus der schönsten Stadt der Schweiz - diese erwarten dich auf tsüri.ch. Wir versorgen dich regelmässig mit kritischem, engagiertem und unabhängigem Journalismus."
        />
      </Head>

      <MailChimpContext.Provider value={mcCampaigns}>
        <BlockRenderer
          blocks={blocks}
          publishedAt={new Date(page?.publishedAt)}
          updatedAt={new Date(page?.updatedAt)}
        />
      </MailChimpContext.Provider>
    </App>
  )
}
