import {ssrArticleList} from '../@types/codegen/page'
import {Feed as RssFeed} from 'feed'

export async function getServerSideProps({req, res}) {
  const articleListResponse = await ssrArticleList.getServerPage(
    {
      variables: {
        take: 30
      }
    },
    req
  )
  const {data} = articleListResponse.props

  const feed = new RssFeed({
    title: 'Tsüri.ch #MirSindTsüri',
    description:
      'Wir versorgen dich regelmässig mit kritischem, engagiertem und unabhängigem Journalismus. Ein Muss für alle jene, die Zürich mit kritischen Augen betrachten und gute Texte, Bilder und Videos ebenso lieben wie stilvolle Unterhaltung. #MirSindTsüri',
    id: process.env.NEXT_PUBLIC_WEBSITE_URL,
    link: process.env.NEXT_PUBLIC_WEBSITE_URL,
    favicon: '',
    copyright: ' Creative Commons Namensnennung - Keine Bearbeitungen 4.0 International Lizenz.',
    updated: new Date()
  })
  data.articles?.nodes.map(article => {
    feed.addItem({
      title: article.title,
      id: article.id,
      link: article.url,
      description: article.lead,
      author: article.authors.map(author => ({name: author.name})),
      date: new Date(article.publishedAt),
      image: article.image?.teaserUrl
    })
  })

  res.setHeader('Content-Type', 'text/xml')
  res.write(feed.rss2()) //TODO: find out which rss type
  res.end()

  return {
    props: {}
  }
}

export default function Feed() {
  return null
}
