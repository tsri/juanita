import {NextApiRequest, NextApiResponse} from 'next'
import Stripe from 'stripe'

export interface Card {
  brand: string
  last4: string
  exp_month: number
  exp_year: number
}

export interface StripeCustomer {
  customer: Stripe.Customer
  card: Card | null
}
const stripe = new Stripe(process.env.STRIPE_SECRET_KEY, {apiVersion: '2022-11-15'})
async function getStripeCustomer(customerID): Promise<StripeCustomer> {
  let customer = null
  let card: Card
  try {
    customer = await stripe.customers.retrieve(customerID)
    if (customer?.invoice_settings?.default_payment_method) {
      const pm = await stripe.paymentMethods.retrieve(
        customer?.invoice_settings?.default_payment_method
      )
      card = {
        brand: pm.card.brand,
        last4: pm.card.last4,
        exp_month: pm.card.exp_month,
        exp_year: pm.card.exp_year
      }
    } else if (customer?.default_source) {
      const source = (await stripe.customers.retrieveSource(
        customer.id,
        customer.default_source
      )) as Stripe.Source.Card
      card = {
        brand: source.brand,
        last4: source.last4,
        exp_month: source.exp_month,
        exp_year: source.exp_year
      }
    }
  } catch (error) {
    console.warn('Stripe error', error)
  }
  return {card, customer}
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const {customerID} = req.query
  if (!customerID) {
    return res.status(400).json({
      error: 'Bad input. customerID is required in query.'
    })
  }

  const stripeCustomer = await getStripeCustomer(customerID)
  return res.status(200).json({
    stripeCustomer
  })
}
