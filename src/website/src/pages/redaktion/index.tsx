import App from 'components/App'
import {ssrAuthorList} from '../../@types/codegen/page'
import {RoundImage} from 'components/atoms/roundImage'
import Link from 'next/link'
import {ImageRefData} from 'components/types'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import Head from 'next/head'

export async function getServerSideProps({locale}) {
  const SHOW_ON_WEBSITE_TAG = 'show-on-website'
  const authorsListResponse = await ssrAuthorList.getServerPage({
    variables: {
      take: 100
    }
  })
  const {data: authorsData, error: authorsError} = authorsListResponse.props

  const team = authorsData.authors.nodes
    .filter(author => author.tags.find(tag => tag.tag === SHOW_ON_WEBSITE_TAG))
    .sort((a, b) => {
      const nameA = a.name.toUpperCase() // ignore upper and lowercase
      const nameB = b.name.toUpperCase() // ignore upper and lowercase
      if (nameA < nameB) {
        return -1
      }
      if (nameA > nameB) {
        return 1
      }
      return 0
    })

  return {
    props: {
      team,
      ...(await serverSideTranslations(locale, ['common']))
    }
  }
}

interface TeamMemberCardProps {
  image: ImageRefData
  name: string
  jobTitle?: string
  id: string
  slug: string
}

const TeamMemberCard = ({image, name, jobTitle, slug}: TeamMemberCardProps) => (
  <Link href={`/redaktion/${slug}`} legacyBehavior>
    <div className="flex flex-col items-center my-6 cursor-pointer">
      <RoundImage height={150} width={150} src={image?.largeURL} />
      <div className="text-center capitalize mt-4">
        <h6 className="text-tsri text-xl font-bold">{name}</h6>
        {jobTitle && <p>{jobTitle}</p>}
      </div>
    </div>
  </Link>
)

export default function TeamPage(props: any) {
  const {navs, team} = props

  return (
    <>
      <Head>
        <meta name="description" content="Das Tsüri.ch Team" />
      </Head>
      <App navs={navs}>
        <div className="lg:container lg:mx-auto mb-20">
          <div>
            <h1 className="text-tsri lg:text-5xl pt-20 mb-14">Tsüri-Team</h1>
            <div className="bg-gray-400	my-4 mr-2 h-0.5 w-full" />
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-x-10">
              {team?.map(({id, slug, image, name, jobTitle}) => (
                <TeamMemberCard
                  key={id}
                  image={image}
                  name={name}
                  jobTitle={jobTitle}
                  id={id}
                  slug={slug}
                />
              ))}
            </div>
          </div>
        </div>
      </App>
    </>
  )
}
