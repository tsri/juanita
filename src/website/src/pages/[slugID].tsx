import React from 'react'
import {articleAdapter} from 'components/route/articleAdapter'
import {ssr} from '../@types/codegen/page'
import {BlockRenderer} from 'components/blocks/blockRenderer'
import Head from 'next/head'
import App from 'components/App'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import {TSRI_PAGE_TITLE} from '../helpers'
import {GetServerSideProps} from 'next'

export interface PageProps {
  slug: string
  navs: any
  page: any
}

export const getServerSideProps: GetServerSideProps = async ({params, locale}) => {
  if (!params || !params.slugID || typeof params.slugID !== 'string') {
    return {
      notFound: true
    }
  }

  const res = await ssr.getServerPage({
    variables: {
      slug: params.slugID
    }
  })

  const {data} = res.props
  if (!data.page) {
    return {
      notFound: true
    }
  }

  // TODO: implement some kind of analytics
  const redirect = data.page.properties.find(property => property.key === 'redirect')
  if (redirect && redirect.value.startsWith('http')) {
    return {
      redirect: {
        permanent: false,
        destination: redirect.value
      }
    }
  }

  return {
    props: {
      page: data.page,
      ...(await serverSideTranslations(locale, ['common']))
    }
  }
}

export function Page(props: PageProps) {
  const {page, navs} = props

  const pageData = articleAdapter(page)

  const {
    title,
    lead,
    image,
    tags,
    publishedAt,
    updatedAt,
    blocks,
    socialMediaTitle,
    socialMediaDescription,
    socialMediaImage,
    canonicalUrl,
    url
  } = pageData

  return (
    <App navs={navs}>
      <Head>
        <title>{`${title} - ${TSRI_PAGE_TITLE}`}</title>
        {lead && <meta name="description" content={lead} />}
        <link rel="canonical" href={url} />
        <meta property="og:title" content={socialMediaTitle || title} />
        <meta property="og:type" content="page" />
        <meta property="og:url" content={url} />
        {socialMediaDescription && (
          <meta property="og:description" content={socialMediaDescription} />
        )}
        {(image || socialMediaImage) && (
          <meta property="og:image" content={socialMediaImage?.ogURL ?? image?.ogURL ?? ''} />
        )}

        <meta property="page:published_time" content={publishedAt.toISOString()} />
        <meta property="page:modified_time" content={updatedAt.toISOString()} />
        {tags.map(tag => (
          <meta key={tag} property="page:tag" content={tag} />
        ))}

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content={socialMediaTitle || title} />
        <meta name="twitter:site" content="@Tsueri_ch" />
        <meta name="twitter:description" content={socialMediaDescription || lead} />
        <meta name="twitter:image" content={socialMediaImage?.ogURL ?? image?.ogURL ?? ''} />
      </Head>

      <div className="xl:pr-52">
        <BlockRenderer
          articleShareUrl={canonicalUrl}
          publishedAt={publishedAt}
          updatedAt={updatedAt}
          isArticle={false}
          blocks={blocks}
          tags={tags}
        />
      </div>
    </App>
  )
}

export default Page
