import React from 'react'
import {ssrArticle, ssrArticleList} from '../../../@types/codegen/page'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import {GetStaticPaths} from 'next'
import {Article} from '../../../@types/codegen/graphql'
import ArticleComponent from 'components/ArticleComponent'

export interface ArticleTemplateContainerProps {
  articleId: string
  isPreview: boolean
  navs: any
  article: Article
}

export const getStaticPaths: GetStaticPaths = async () => {
  const articles = await ssrArticleList.getServerPage({
    variables: {
      take: 50
    }
  })

  const paths = articles.props.data.articles.nodes.map(article => {
    return {
      params: {
        id: article.id,
        slug: article.slug
      }
    }
  })

  return {
    paths,
    fallback: true
  }
}

export async function getStaticProps({params, query, locale}) {
  const {id, slug} = params

  const articleRes = await ssrArticle.getServerPage({
    variables: {
      id,
      slug
    }
  })
  const {data} = articleRes.props

  if (!data.article) {
    return {
      notFound: true,
      revalidate: 10
    }
  }

  const tags = data.article.tags.map(tag => tag.replace('category:', ''))

  return {
    props: {
      articleID: id,
      isPreview: false,
      article: {...data.article, tags},
      ...(await serverSideTranslations(locale, ['common']))
    },
    revalidate: 60
  }
}

export function ArticleContainer(props: ArticleTemplateContainerProps) {
  return <ArticleComponent {...props} />
}

export default ArticleContainer
