import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import {ssrArticleList} from '../../../@types/codegen/page'
import OldTeaserGrid from '../../../components/OldTeaserGrid'
import ArticleTeaser from '../../../components/atoms/deprecatedArticleTeaser'
import React from 'react'
import {GetServerSideProps} from 'next'
import {ArticleConnection} from '../../../@types/codegen/graphql'
import {useRouter} from 'next/router'

export interface TagListProps {
  articleList: ArticleConnection
  tag?: string
  navs: any
}

const TAGS_TAKE = 32

export const getServerSideProps: GetServerSideProps = async ({params, query, locale}) => {
  const tag = params && params.tag && typeof params.tag === 'string' ? params.tag : undefined
  const skip: number = query.skip && typeof query.skip === 'string' ? parseInt(query.skip) : 0
  const articleListResponse = await ssrArticleList.getServerPage({
    variables: {
      ...(tag ? {filter: {tags: [tag]}} : {}),
      take: TAGS_TAKE,
      skip
    }
  })

  const {data: articleListData} = articleListResponse.props
  return {
    props: {
      articleList: articleListData?.articles,
      tag,
      ...(await serverSideTranslations(locale, ['common']))
    }
  }
}

export default function TagList({articleList, tag, navs}: TagListProps) {
  const baseLink = `/zh/rubrik/${tag}`
  const {query} = useRouter()
  const currentSkip: number =
    query.skip && typeof query.skip === 'string' ? parseInt(query.skip) : 0

  const newerArticlesLink =
    currentSkip > 0 ? `${baseLink}?skip=${currentSkip - TAGS_TAKE}` : undefined
  const olderArticlesLink = articleList.pageInfo.hasNextPage
    ? `${baseLink}?skip=${currentSkip + TAGS_TAKE}`
    : undefined

  return (
    <OldTeaserGrid
      title={tag}
      navs={navs}
      newerArticlesLink={newerArticlesLink}
      olderArticlesLink={olderArticlesLink}>
      {articleList.nodes.map(article => (
        <div className="mb-10" key={article.id}>
          <ArticleTeaser
            key={article.id}
            authors={article.authors}
            date={new Date(article.publishedAt)}
            title={article.title}
            image={article?.image}
            tags={article.tags}
            href={`/a/${article.id}/${article.slug}`}
          />
        </div>
      ))}
    </OldTeaserGrid>
  )
}
