import React from 'react'

/**
 * This is a redirect only. Earlier article urls were in the form zh/articleSlug.articleId.
 * We redirect to We.Publish standard format which is a/articleId/articleSlug
 * @constructor
 */
const ZhRedirect = () => {
  // This component doesn't render anything
  return null
}
export async function getServerSideProps({res, params}) {
  const {slugID = ''} = params
  const [slug, id] = (slugID as string).split('.')

  return {
    redirect: {
      destination: `/a/${id}/${slug}`,
      permanent: true
    }
  }
}

export default ZhRedirect
