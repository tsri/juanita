import React from 'react'
import {NavsProps} from '../_app'
import LoginView from '../../components/auth/LoginView'

export default function Login({navs}: {navs: Partial<NavsProps>}) {
  return <LoginView navs={navs} />
}
