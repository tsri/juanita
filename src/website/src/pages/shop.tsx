const ShopRedirect = () => {
  // This component doesn't render anything
  return null
}

export async function getServerSideProps({res}) {
  // Perform server-side redirect
  res.writeHead(302, {Location: 'https://shop.tsri.ch'})
  res.end()

  return {props: {}}
}

export default ShopRedirect
