import Link from 'next/link'

export default function Custom500() {
  return (
    <div className="mx:auto">
      <h1 className="text-center mt-10">Sorry. Etwas ist schiefgelaufen.</h1>
      <img className="mx-auto mt-4" alt="error gif" src="/error500.gif" />
      <p className="text-center mt-10">
        Unser{' '}
        <a href="mailto:info@tsri.ch?subject=Fehler" className="underline">
          Computerflüsterer
        </a>{' '}
        hat bereits sein Lieblingswerkzeug und kümmert sich um die Reparatur.{' '}
        <Link href="/" className="underline">
          Hier klicken
        </Link>{' '}
        um wieder auf die Startseite zu kommen.
      </p>
    </div>
  )
}
