/**
 * DEPRECATED - DO NOT USE ANYMORE!
 */

export enum Breakpoint {
  Mobile = 0,
  Tablet = 600,
  Desktop = 1000
}

export enum ZIndex {
  Default = 0,
  Overlay = 1,
  Header = 2,
  Navigation = 3,
  FullscreenOverlay = 4
}

export const tabletMediaQuery = `screen and (min-width: ${
  Breakpoint.Tablet
}px) and (max-width: ${Breakpoint.Desktop - 1}px)`

export function whenTablet(styles: any) {
  // prettier-ignore
  return {
    [`@media ${tabletMediaQuery}`]: styles
  }
}

export const desktopMediaQuery = `screen and (min-width: ${Breakpoint.Desktop}px)`

export function whenDesktop(styles: any) {
  // prettier-ignore
  return {
    [`@media ${desktopMediaQuery}`]: styles
  }
}
