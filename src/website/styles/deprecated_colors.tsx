/**
 * DEPRECATED - REMOVE
 */

export enum Color {
  White = '#FFFFFF',
  Black = '#000000',

  LightGray = '#B0B0B0',

  Neutral = '#CDCDCD',
  NeutralDark = '#202020',
  NeutralLight = '#F8F8F8',

  Primary = '#3AA1F0',
  PrimaryDark = '#3AA1F0',
  PrimaryLight = '#3AA1F0',

  Secondary = '#6fda8c',
  SecondaryDark = '#39a85e',
  SecondaryLight = '#a3ffbd',

  Highlight = '#FF0D63',

  HighlightSecondary = '#F5FF64',
  HighlightSecondaryLight = '#FBFFC0',

  Error = '#FFBBBB'
}
