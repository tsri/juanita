# Tsüri

## Development

### Prerequisites

- [Node v12.x.x][node-download-url]
- [Yarn v1.17.x][yarn-download-url]

### Setup for development

1. Setup `yarn install && yarn setup && yarn installAll`
2. Start env (db/media) `yarn env:start` (to reset database/media run `yarn env:reset`)
3. Start in watch mode `yarn watch`

**Login Editor:**  
Username: admin@tsri.ch  
Password: 123

The following servers will be available:

- **Website:** [http://localhost:5000](http://localhost:5000)

## Error Handling

Use the handleError() function from the helpers.ts file.

## Styling

There are two **deprecated** style systems: custom style system with ...tablet()/...desktop()/...mobile() functions
and the _tailwind_ setup.

These style systems are continuously replaced with the **MUI** framework. The mui theme is defined in `_app.tsx` in the
`createTheme()` function.

## Form Validation

For the form validation we use [https://react-hook-form.com/get-started](https://react-hook-form.com/get-started).
For an example implementation see `LoginForm.tsx`

## Authentication

- The authentication logic and data live in `AuthContextProvider.tsx`. Within that
  context you'll find a user object (if you are logged in) or authentication methods.
- In case you have a view which requires an authenticated user, wrap that component
  with `AuthenticationRequired.tsx` component. For an example see `profile.tsx`

## Related Repositories

- [karma.run-packages][karmarun-packages-repo-url]
- [wepublish][wepublish-packages-repo-url]

[node-download-url]: https://nodejs.org/en/download/current/
[yarn-download-url]: https://yarnpkg.com/en/docs/install
[karmarun-packages-repo-url]: https://github.com/karmarun/karma.run-packages/
[wepublish-packages-repo-url]: https://github.com/wepublish/wepublish/
